<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrganizationType extends Model
{
    protected $table = 'organizationstypes';

    public function organization() {
        return $this->hasMany('App\Organization');
    }
}
