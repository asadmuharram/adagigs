<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Organization extends Model
{

	protected $table = 'organizations';

    protected $fillable = [
        'organization_name','organization_type_id','url_organization_name','organization_website','user_id','organization_name','image_file_path'
    ];

    public function organizationtype()
    {
        return $this->belongsTo('App\OrganizationType' , 'organization_type_id');
    }

    public function organizationaddress()
    {
        return $this->hasOne('App\OrganizationAddress');
    }
}
