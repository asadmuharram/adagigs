<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GigBand extends Model
{
    protected $table = 'gigsband';

    public function gig()
    {
        return $this->belongsTo('App\Gig' , 'gig_id');
    }

    public function band()
    {
        return $this->belongsTo('App\Band' , 'band_id');
    }

    public function getBandCount($bandId)
    {
    	$Gigbands = Gigband::where([
                ['band_id',$bandId],
                ['status','accept'],
        ])->count()->get();

        $this->band_gig_count = $Gigbands;
        return $this;
    }
}
