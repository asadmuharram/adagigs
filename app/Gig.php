<?php

namespace App;

use Sofa\Eloquence\Eloquence;
use Illuminate\Database\Eloquent\Model;

class Gig extends Model
{
    use Eloquence;

    protected $table = 'gigs';

    protected $dates = ['gig_band_opening_date', 'gig_band_closing_date', 'gig_date','created_at','updated_at'];

    protected $fillable = [
        'gig_name','gigtype_id','gig_description','gig_band_requirement','gig_band_opening_date','gig_band_closing_date','genre_id',
        'gig_date','gig_total_band','user_id','organization_id'
    ];

    protected $searchableColumns = ['gig_name','gigtype_id'];

    public function genre()
    {
        return $this->belongsTo('App\Genre','genre_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }

    public function organization()
    {
        return $this->belongsTo('App\Organization','organization_id');
    }

    public function giggenre()
    {
        return $this->hasMany('App\GigGenre');
    }

    public function city() {
        return $this->belongsTo('App\City','city_id');
    }

    public function gigtype()
    {
        return $this->belongsTo('App\GigType' , 'gigtype_id');
    }

    public function gigband()
    {
        return $this->hasMany('App\GigBand');
    }

    public function giglocation()
    {
        return $this->hasMany('App\GigLocation');
    }

    public function notification()
    {
        return $this->hasMany('App\Notification');
    }
}
