<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Carbon\Carbon;
use App\Gig;


class checkGigStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'gig:checkStatus';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check Gig Status every hours 24/7';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $Time = Carbon::now();
        $Gigs = Gig::where('gig_band_closing_date', '<', $Time)->get();
        foreach ($Gigs as $Gig) {
            $Gig = Gig::findOrFail($Gig->id);
            $Gig->status = 'close';
            $Gig->save();
        }
        $this->info('Gig status has changed.');
        }
    }
