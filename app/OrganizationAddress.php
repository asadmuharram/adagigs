<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrganizationAddress extends Model
{

	protected $table = 'organizationaddresses';

	protected $fillable = [
        'organization_id','city_id','address','postcode','phonenumber',
    ];

    public function organization()
    {
        return $this->belongsTo('App\Organization' , 'organization_id');
    }

    public function city() {
        return $this->belongsTo('App\City','city_id');
    }
}
