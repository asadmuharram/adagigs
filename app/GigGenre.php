<?php

namespace App;

use Sofa\Eloquence\Eloquence;
use Illuminate\Database\Eloquent\Model;

class GigGenre extends Model
{
    use Eloquence;

    protected $table = 'giggenre';

    protected $fillable = [
        'genre_id'
    ];

    protected $searchableColumns = ['genre_id'];

    public function gig() {
        return $this->belongsTo('App\Gig','gig_id');
    }

    public function genre()
    {
        return $this->belongsTo('App\Genre','genre_id');
    }
}
