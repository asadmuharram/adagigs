<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Nicolaslopezj\Searchable\SearchableTrait;
use App\Resource;
use App\Category;

use Carbon\Carbon;
use Mail;
use App\User;
use App\Gig;
use App\City;
use App\Organization;
use App\Genre;
use App\GigType;
use App\GigGenre;
use App\GigLocation;
use App\GigBand;
use App\Band;
use App\BandType;
use App\BandLocation;
use App\BandGenre;
use App\OrganizationType;
use App\OrganizationAddress;
use App\Notification;

use Auth;
use Session;

class FrontpageController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    protected function userId()
    {
        $userId = Auth::user()->id;
        return $userId;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Gigs = Gig::where('status','open')->with('giglocation.city.province')->orderBy('gig_date','asc')->take(6)->get();
        $Organizations = Organization::all();
        $GenreLists = Genre::all();
        $GigTypes = GigType::all();
        $GigGenres = GigGenre::with('genre')->get();
        $nowTime = Carbon::now();

        $Bands  = Band::with('bandtype')->get();
        $BandGenres = BandGenre::with('genre')->get();
        $BandLocations = BandLocation::with('city.province')->get();

        return view('frontpage.frontpageIndex', [
                'Gigs' => $Gigs,
                'Organizations' => $Organizations,
                'GenreLists' => $GenreLists,
                'GigTypes' => $GigTypes,
                'GigGenres' => $GigGenres,
                'nowTime' => $nowTime,
                'Bands' => $Bands,
                'BandGenres' => $BandGenres,
                'BandLocations' => $BandLocations
            ]);
    }

    public function setTime($time)
    {
        Carbon::setLocale('id');
        $timeSet = new Carbon($time , 'Asia/Jakarta');
        return $timeSet;

    }

    public function gigs()
    {
        $Gigs = Gig::where([
            ['status','open'],
        ])->with('giglocation.city.province')->paginate(10);

        $GigLocation = GigLocation::with('city.province')->get();
        $Organizations = Organization::all();
        $Genres = Genre::pluck('genre_name', 'id');
        $GenreLists = Genre::all();
        $GigTypes = GigType::all();
        $GigGenres = GigGenre::with('genre')->get();

        if (Auth::check()) {
        if (Auth::user()->type == 'band') {
        $Band        =   Band::where('user_id',Auth::user()->id)->first();
        $GigBands    =   GigBand::where('band_id',$Band->id)->with('gig')->get();

        $nowTime = Carbon::now();

        return view('frontpage.frontpageGigs', [
                'Gigs' => $Gigs,
                'Genres' => $Genres,
                'nowTime' => $nowTime,
                'GigLocation' => $GigLocation,
                'GigGenres' => $GigGenres,
                'Organizations' => $Organizations,
                'GenreLists' => $GenreLists,
                'GigTypes' => $GigTypes,
                'GigBands' => $GigBands,
                'Band' => $Band
            ]);
        } else {
        $nowTime = Carbon::now();
        return view('frontpage.frontpageGigs', [
                'Gigs' => $Gigs,
                'Genres' => $Genres,
                'nowTime' => $nowTime,
                'GigLocation' => $GigLocation,
                'GigGenres' => $GigGenres,
                'Organizations' => $Organizations,
                'GenreLists' => $GenreLists,
                'GigTypes' => $GigTypes,
            ]);
        }

        } else {

            $nowTime = Carbon::now();
        return view('frontpage.frontpageGigs', [
                'Gigs' => $Gigs,
                'Genres' => $Genres,
                'nowTime' => $nowTime,
                'GigLocation' => $GigLocation,
                'GigGenres' => $GigGenres,
                'Organizations' => $Organizations,
                'GenreLists' => $GenreLists,
                'GigTypes' => $GigTypes,
            ]);
        }
   
    }

    public function bands()
    {
        $Bands  = Band::with('bandtype')->get();
        $BandGenres = BandGenre::with('genre')->get();
        $BandLocations = BandLocation::with('city.province')->get();

        $GigTypes = GigType::all();
        $GenreLists = Genre::all();

        return view('frontpage.frontpageBands', [
                'Bands' => $Bands,
                'BandGenres' => $BandGenres,
                'BandLocations' => $BandLocations,
                'GigTypes' => $GigTypes,
                'GenreLists' => $GenreLists,
                'GenreLists' => $GenreLists,
            ]);
    }

    public function GigBandApply(Request $request)
    {
        $validator = $this->validate($request,[
            'gig' => 'required|integer|max:64',
        ]);

        if (Auth::user()->type == 'band') {
            $Band  = Band::where('user_id',Auth::user()->id)->first();
            $gigBandCount = GigBand::where('gig_id',$request->input('gig'))
                                    ->where('band_id', $Band->id)
                                    ->get();

            if (count($gigBandCount) > 0) {

                Session::flash('error_message', 'Maaf, Anda sudah mengajukan permintaan untuk gig ini.');
                return redirect()->back();
            } else {

                $Gig = Gig::findOrFail($request->input('gig'));
                $GigBand = new GigBand;
                $GigBand->gig_id = $request->input('gig');
                $GigBand->band_id = $Band->id;
                $GigBand->status = 'pending';
                $GigBand->counter = 1;
                $GigBand->request_by = 'band';
                $GigBand->save();

                $Organization = Organization::findOrFail($Gig->organization_id);

                $user = User::find(Auth::user()->id);
                $user->newNotification()
                ->withType('bandJoin')
                ->withSubject('Permintaan ikut serta musisi / band')
                ->withBody('<a href="/band/'.$Band->url_band_name.'">'.$Band->band_name.'</a> mengirimkan permintaan ikut serta gig '.$Gig->gig_name)
                ->withGigId($Gig->id)
                ->regarding($Organization)
                ->deliver();

                $userOrg = User::find($Organization->user_id);

                $dataVer = [ 
                            'orgName' => $Organization->organization_name,
                            'gigName' => $Gig->gig_name,
                            'bandName' => $Band->band_name,
                            ];
                Mail::send('emails.bandApply',$dataVer,function($message) use ($userOrg) {
                        $message->from('no-reply@adagigs.com', 'Adagigs');
                        $message->to($userOrg->email, $userOrg->first_name)->subject('Permintaan Ikut Serta Gig');
                });
                Session::flash('flash_message', 'Permintaan ikut serta gig berhasil. Berikut daftar status permintaan anda.');
                return redirect('dashboard/gig');
            }
        } else {
            Session::flash('error_message', 'Maaf, Anda tidak memiliki akses.');
            return redirect()->back();
        }
 
    }

    public function bandPage($url)
    {
        $Band = Band::where('url_band_name',$url)->with('bandtype')->first();
        $BandGenre = BandGenre::where('band_id',$Band->id)->with('genre')->first();
        $BandLocation = BandLocation::where('band_id',$Band->id)->with('city.province')->first();

        return view('frontpage.frontpageBandPage', [
                'Band' => $Band,
                'BandGenre' => $BandGenre,
                'BandLocation' => $BandLocation,
            ]);
    }

    public function bandPageGigs($url)
    {
        $Band = Band::where('url_band_name',$url)->with('bandtype')->first();
        $BandGenre = BandGenre::where('band_id',$Band->id)->with('genre')->first();
        $BandLocation = BandLocation::where('band_id',$Band->id)->with('city.province')->first();
        $Gigbands = Gigband::where([
                ['band_id',$Band->id],
                ['status','accept'],
            ])->with('gig.organization')->get();

        return view('frontpage.frontpageBandPageGigs', [
                'Band' => $Band,
                'BandGenre' => $BandGenre,
                'BandLocation' => $BandLocation,
                'Gigbands' => $Gigbands,
            ]);
    }


    public function organizationPage($url)
    {
        $Organization = Organization::where('url_organization_name',$url)->with('organizationtype')->first();
        $Gigs = Gig::where('organization_id',$Organization->id)->get();
        $OrganizationAddress = OrganizationAddress::where('organization_id',$Organization->id)->with('city.province')->first();

        return view('frontpage.frontpageOrganizationPage', [
                'Gigs' => $Gigs,
                'Organization' => $Organization,
                'OrganizationAddress' => $OrganizationAddress,
            ]);
    }

    public function organizationPageGigs($url)
    {
        $Organization = Organization::where('url_organization_name',$url)->with('organizationtype')->first();
        $Gigs = Gig::where('organization_id',$Organization->id)->get();
        $OrganizationAddress = OrganizationAddress::where('organization_id',$Organization->id)->with('city.province')->first();

        return view('frontpage.frontpageOrganizationPageGigs', [
                'Gigs' => $Gigs,
                'Organization' => $Organization,
                'OrganizationAddress' => $OrganizationAddress,
            ]);
    }

    public function GigPage($url_organization_name,$gig_slug)
    {
        $Organization = Organization::where('url_organization_name',$url_organization_name)->with('organizationtype')->first();
        $Gig = Gig::where([
                ['organization_id',$Organization->id],
                ['slug',$gig_slug],
            ])->first();
        $Gigbands = Gigband::where([
                ['gig_id',$Gig->id],
            ])->with('gig')->get();
        $nowTime = Carbon::now();
        $GigGenres = GigGenre::with('genre')->get();
        $GigType = Gig::where('id',$Gig->id)->with('gigtype')->first();
        $GigLocation = GigLocation::where('gig_id',$Gig->id)->with('city.province')->first();

        if (Auth::check()) {
        if (Auth::user()->type == 'band') {
        $Band        =   Band::where('user_id',Auth::user()->id)->first();
        $BandGigs    =   GigBand::where('band_id',$Band->id)->where('gig_id',$Gig->id)->with('gig')->get();

        $nowTime = Carbon::now();

            return view('frontpage.frontpageGigPage', [
                'Organization' => $Organization,
                'Gig' => $Gig,
                'nowTime' => $nowTime,
                'Gigbands' => $Gigbands,
                'GigLocation' => $GigLocation,
                'GigGenres' => $GigGenres,
                'GigType' => $GigType,
                'BandGigs' => $BandGigs,
                'Band' => $Band
            ]);

        } else {
            return view('frontpage.frontpageGigPage', [
                'Organization' => $Organization,
                'Gig' => $Gig,
                'nowTime' => $nowTime,
                'GigLocation' => $GigLocation,
                'GigGenres' => $GigGenres,
                'GigType' => $GigType,
            ]);
        }

        } else {

            return view('frontpage.frontpageGigPage', [
                'Organization' => $Organization,
                'Gig' => $Gig,
                'nowTime' => $nowTime,
                'GigLocation' => $GigLocation,
                'GigGenres' => $GigGenres,
                'GigType' => $GigType,
            ]);

        }
    }

    public function GigBandPage($url_organization_name,$gig_slug)
    {
        $Organization = Organization::where('url_organization_name',$url_organization_name)->with('organizationtype')->first();
        $Gig = Gig::where([
                ['organization_id',$Organization->id],
                ['slug',$gig_slug],
            ])->first();
        $GigbandsAccept = Gigband::where([
                ['gig_id',$Gig->id],
                ['status','accept'],
            ])->with('gig')->get();
        $GigGenres = GigGenre::with('genre')->get();
        $GigLocation = GigLocation::where('gig_id',$Gig->id)->with('city.province')->first();

        if (Auth::check()) {
        if (Auth::user()->type == 'band') {
        $Band        =   Band::where('user_id',Auth::user()->id)->first();
        $BandGigs    =   GigBand::where('band_id',$Band->id)->where('gig_id',$Gig->id)->with('gig')->get();

        $nowTime = Carbon::now();

        return view('frontpage.frontpageGigBandPage', [
                'Organization' => $Organization,
                'Gig' => $Gig,
                'GigbandsAccept' => $GigbandsAccept,
                'GigLocation' => $GigLocation,
                'GigGenres' => $GigGenres,
                'BandGigs' => $BandGigs,
                'Band' => $Band,
            ]);

        } else {
            return view('frontpage.frontpageGigBandPage', [
                'Organization' => $Organization,
                'Gig' => $Gig,
                'GigbandsAccept' => $GigbandsAccept,
                'GigLocation' => $GigLocation,
                'GigGenres' => $GigGenres,
            ]);
        }

        } else {
            return view('frontpage.frontpageGigBandPage', [
                'Organization' => $Organization,
                'Gig' => $Gig,
                'GigbandsAccept' => $GigbandsAccept,
                'GigLocation' => $GigLocation,
                'GigGenres' => $GigGenres,
            ]);
        }

        
    }

    public function getGigs()
    {
        $Gigs = Gig::where([
            ['status','open'],
            ])->with('giglocation.city.province')->paginate(10);

        return $Gigs;
    }

    public function privacy()
    {
        return view('frontpage.frontpagePrivacy');
    }

    public function terms()
    {
        return view('frontpage.frontpageTOS');
    }

    public function gigSearch(Request $request)
    {

        /*Variable frontpageGigSearch 
        $Gigs = Gig::search($q)->get();
        */

        $q = $request->input('q', null);
        $city = $request->input('city', null);
        $start = new Carbon($request->input('start', null));
        $end = new Carbon($request->input('end', null));
        $genre = $request->input('genre', null);
        $type = $request->input('type', null);
        $param = $request->input('param', null);
        $keyword = $request->input('keyword', null);

        if ($request->has('param')) {
            switch ($param) {
                case 'none':
                    return redirect('/');
                    break;
                case 'genre':
                    $Genres = Genre::where('genre_name',$keyword)->get();
                    $Gigs = Gig::joinRelations('giglocation','giggenre','gigtype')
                                ->where(function($q) use ($Genres){
                                    foreach($Genres as $Genre){
                                        $q->where('giggenre.genre_id', 'LIKE', $Genre->id);
                                    }})
                                ->prefixColumnsForJoin()
                                ->latest('gigs.created_at')
                                ->groupBy('gigs.id')
                                ->paginate(10);
                                $queries = null;
                    break;
                case 'band':
                    return redirect('bands/search?q='.$keyword);
                    break;
                case 'gig':
                    $Gigs = Gig::joinRelations('giglocation','giggenre','gigtype')
                            ->where('gigs.gig_name', 'LIKE', '%'.$keyword.'%')
                            ->prefixColumnsForJoin()
                            ->latest('gigs.created_at')
                            ->groupBy('gigs.id')
                            ->paginate(10);
                            $queries = null;
                    break;
                case 'location':
                    $Cities = City::where('city_name', 'LIKE', '%'.$keyword.'%')->get();
                    foreach ($Cities as $City) {
                        $Gig = Gig::joinRelations('giglocation','giggenre','gigtype')
                            ->where('giglocation.city_id',$City->id)
                            ->prefixColumnsForJoin()
                            ->latest('gigs.created_at')
                            ->groupBy('gigs.id')
                            ->paginate(10);

                        $queries[] = $Gig;
                        $Gigs = null;
                    }
                    break; 
            }
        } else {
            if ($request->has('start') || $request->has('end')) {
                $Gigs = Gig::joinRelations('giglocation','giggenre','gigtype')
                ->whereBetween('gigs.gig_date', array($start, $end))
                ->prefixColumnsForJoin()
                ->latest('gigs.created_at')
                ->groupBy('gigs.id')
                ->paginate(10);
                $queries = null;
            } else {
                $Gigs = Gig::joinRelations('giglocation','giggenre','gigtype')
                ->where('gigs.gig_name', 'like', '%'.$q.'%')
                ->where('giggenre.genre_id', 'like', '%'.$genre.'%')
                ->where('giglocation.city_id', 'like', '%'.$city.'%')
                ->where('gigs.gigtype_id', 'like', '%'.$type.'%')
                ->prefixColumnsForJoin()
                ->latest('gigs.created_at')
                ->groupBy('gigs.id')
                ->paginate(10);
                $queries = null;
            }
        }

        $GigGenres = GigGenre::with('genre')->get();
        $nowTime = Carbon::now();
        $Organizations = Organization::all();

        /*Variable Sidepanel*/
        $GigTypes = GigType::all();
        $GenreLists = Genre::all();

        return view('frontpage.frontpageGigSearch', [
                'Gigs' => $Gigs,
                'queries' => $queries,
                'GigTypes' => $GigTypes,
                'GenreLists' => $GenreLists,
                'GigGenres' => $GigGenres,
                'nowTime' => $nowTime,
                'Organizations' => $Organizations,
        ]);
    }

    public function bandSearch(Request $request)
    {
        $q = $request->input('q', null);
        $city = $request->input('city', null);
        $genre = $request->input('genre', null);

        $Bands = Band::joinRelations('bandlocation','bandgenre','bandtype')
            ->where('bands.band_name', 'like', '%'.$q.'%')
            ->where('bandgenres.genre_id', 'like', '%'.$genre.'%')
            ->where('bandlocation.city_id', 'like', '%'.$city.'%')
            ->prefixColumnsForJoin()
            ->latest('bands.created_at')
            ->groupBy('bands.id')
            ->paginate(10);

        $BandGenres = BandGenre::with('genre')->get();
        $BandLocations = BandLocation::with('city.province')->get();

        $GigTypes = GigType::all();
        $GenreLists = Genre::all();

        return view('frontpage.frontpageBands', [
                'Bands' => $Bands,
                'BandGenres' => $BandGenres,
                'BandLocations' => $BandLocations,
                'GigTypes' => $GigTypes,
                'GenreLists' => $GenreLists,
                'GenreLists' => $GenreLists,
            ]);
    }

    /* AJAX REQUEST */
    public function ajaxCitiesFront(Request $request)
    {
        $term = $request->term ?: '';
        $cities = City::with('province')->where('city_name', 'like', '%'.$term.'%')->get();
        $valid_cities = array();
        foreach ($cities as $id => $city) {
            $valid_cities[] = array('city_id' => $city->id, 'city_name' => $city->city_name.' , '.$city->province->province_name);
        }
        return \Response::json($valid_cities);
    }

}
