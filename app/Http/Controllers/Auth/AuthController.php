<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Carbon\Carbon;
use Laravel\Socialite\Facades\Socialite;
use Auth;
use Illuminate\Http\Request;
use Hash;
use Mail;
use Illuminate\Support\Facades\Input;
use Session;
use App\LoginAttempts;
use App\Http\Requests;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/dashboard';
    protected $autoLogin = false;
    protected $verCode ;
    protected $maxLoginAttempts = 3; // Amount of bad attempts user can make
    protected $lockoutTime = 300; // Time for which user is going to be blocked in seconds

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
        $this->verCode = $this->verCode(1,999999);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */

    /*
        Status Code :
        1 = Active,
        2 = Not Confirm,
        3 = Deactive
    */

    protected function createLoginAttempts($userId,$ipAddress,$status)
    {
        $nowTime = Carbon::now('Asia/Jakarta');
        $LoginAttempts = LoginAttempts::create(
            [
                'user_id' => $userId,
                'login_ip' => $ipAddress,
                'login_time' => $nowTime,
                'login_status' => $status,
            ]);
        return $LoginAttempts;
        
    }

    public function postLogin(Request $request)
    {   

        $email = $request['email'];
        $password = $request['password'];

        $User  = User::where('email',$request->input('email'))->first();

        if (empty($User)) {
            Session::flash('error_message', 'Email atau Password anda salah');
            return redirect('/login');
        } else {
        if ($User->via == 'adareg') {
            if (Auth::attempt(['email' => $email, 'password' => $password, 'status' => '1' , 'via' => 'adareg'])) {
            $User  = User::where('email',$request->input('email'))->first();
            $status = 'success';
            $this->createLoginAttempts($User->id,$request->ip(),$status);
            return redirect()->intended('dashboard');
            } 
            elseif (Auth::attempt(['email' => $email, 'password' => $password,'status' => '2' ])) {
                $User  = User::where('email',$request->input('email'))->first();
                $status = 'failed_email_not_verified';
                $this->createLoginAttempts($User->id,$request->ip(),$status);
                Auth::logout();
                Session::flash('error_message', 'Silahkan verifikasi email anda');
                return redirect('/login');
            } 
            else {
                $User  = User::where('email',$request->input('email'))->first();
                $status = 'failed_wrong_credentials';
                $this->createLoginAttempts($User->id,$request->ip(),$status);
                Auth::logout();
                Session::flash('error_message', 'Email atau Password anda salah.');
                return redirect('/login');
            }
        } else {
            Auth::logout();
            Session::flash('error_message', 'Silahkan masuk dengan akun facebook anda.');
            return redirect('/login');
        }  

        } 

            
    }
    

    protected function verCode($startNum,$endNum)
    {
        $randomNum = rand($startNum,$endNum);
        $verCode = md5($randomNum);
        return $verCode;
    }
    

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'username' => 'required|max:255|unique:users',
            'email' => 'required|email|max:255|unique:users',
            'day_birth' => 'required',
            'month_birth' => 'required',
            'year_birth' => 'required',
            'password' => 'required|min:6|confirmed',
            'password_confirmation' => 'required|min:6',
            'type' => 'required',
        ]);
    }

    public function getConfirm($username,$token)
    {
        $user = User::where('username', $username)->first();
        $verCode = $user->verification_code;
        if ($verCode == $token) {
            $user->status = 1;
            $user->save();
            $dataVer = [ 
                        'firstName' => $user->first_name,
                        'username' => $user->username
                        ];
            Mail::send('emails.registrationSuccess',$dataVer,function($message) use ($user) {
                    $message->from('no-reply@adagigs.com', 'Adagigs');
                    $message->to($user->email, $user->first_name)->subject('Selamat Datang di Adagigs');
            });
            Session::flash('flash_message', 'Email anda telah terverifikasi , silakan login untuk masuk ke akun anda.');
            return redirect('/login');
        } else {
            Session::flash('error_message', 'Maaf data yang anda masukkan salah , silakan klik disini untuk mengirim ulang kode verifikasi anda');
            return redirect('/login');
        }

    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */

    public function showRegistrationForm()
    {
        $type = sha1('adareg');
        return view('auth.register',[
                'type' => $type,
            ]);
    }


    protected function create(array $data)
    {
        /*
        Status Code :
        1 = Active,
        2 = Not Confirm,
        3 = Deactive
        */

        if ($data['type'] == '7016c5e83e301aba3814dfd9fdbd17414aef061e') {
            $via = 'adareg';
            $status ='2';
        } elseif ($data['type'] == 'f95e6a7b767d0e69dd12bc8c83ab171a316832b8') {
            $via = 'fbreg';
            $status ='1';
        } elseif ($data['type'] == '2b338b2292b4473e0961ddfc3c3ee6790a69848a') {
            $via = 'twreg';
            $status ='1';
        } else {
            Session::flash('error_message', 'Invalid verification');
            return redirect('/register');
        }
        
        $dayBirth = $data['day_birth'];
        $monthBirth = $data['month_birth'];
        $yearBirth = 1932 + $data['year_birth'];
        $email = $data['email'];
        $birthDate = Carbon::create($yearBirth, $monthBirth, $dayBirth, 0);
        
            $verCode = $this->verCode;
            $user = User::create(
            [
                'first_name' => $data['first_name'],
                'last_name' => $data['last_name'],
                'email' => $data['email'],
                'username' => $data['username'],
                'password' => Hash::make($data['password']),
                'birth_date' => $birthDate,
                'status' => $status,
                'verification_code' => $verCode,
                'via' => $via,
                'type' => 'unknown',
            ]);
        return $user;
        
    }

     public function postRegister(Request $request)
     {
        $validator = $this->validator($request->all());
        if ($validator->fails()) {
            $this->throwValidationException(
                $request, $validator
            );
        }

        $dayBirth = $request['day_birth'];
        $monthBirth = $request['month_birth'];
        $yearBirth = 1932 + $request['year_birth'];
        $birthDate = Carbon::create($yearBirth, $monthBirth, $dayBirth, 0);

        $nowTime = Carbon::now('Asia/Jakarta');
        $nowYear = date('Y' , strtotime($nowTime));
        $birthDateYear = date('Y' , strtotime($birthDate));

        $age = $nowYear - $birthDateYear;

        if ($age < 18) {
            Session::flash('error_message', 'Maaf usia anda dibawah 18 tahun. Adagigs mensyaratkan anda berumur min. 18 tahun');
            return redirect('/register');
        } else {

        $regisUser = $this->create($request->all());

        $User  = User::where('id',$regisUser->id)->first();
        $User->register_ip = $request->ip();
        $User->save();

        if ($regisUser) {
            if ($request->input('type') == '7016c5e83e301aba3814dfd9fdbd17414aef061e') {
                $verCode = $this->verCode;
                $dataVer = [
                    'verCode' => $verCode , 
                    'firstName' => Input::get('first_name'),
                    'username' => Input::get('username')
                    ];
                Mail::send('emails.registration',$dataVer,function($message) {
                    $message->from('no-reply@adagigs.com', 'Adagigs');
                    $message->to(Input::get('email'), Input::get('first_name'))->subject('Aktifkan akun Adagigs Anda');
                });
                Session::flash('flash_message', 'Registrasi berhasil ! , Silahkan cek email anda untuk verifikasi akun.');
            } else {
                $dataVer = [ 
                    'firstName' => Input::get('first_name'),
                    'username' => Input::get('username')
                    ];
                Mail::send('emails.registrationSuccess',$dataVer,function($message) {
                    $message->from('no-reply@adagigs.com', 'Adagigs');
                    $message->to(Input::get('email'), Input::get('first_name'))->subject('Selamat Datang di Adagigs');
                });
                Session::flash('flash_message', 'Registrasi berhasil! , Selamat datang di adagigs.');
                Auth::attempt(['email' => $request['email'], 'password' => $request['password'], 'status' => '1']); 
                return redirect('/dashboard');
                
            }
        
        }
        return redirect('login');
        }

     }   

    public function redirectToProvider(Request $request,$provider)
    {


        /*return Socialite::driver($provider)->redirect();*/

        Session::flash('ipaddress',$request->ip());

        return Socialite::with($provider)->fields([
        'first_name', 'last_name', 'email', 'gender', 'birthday' , 'age_range' , 'id'
        ])->scopes([
        'email' , 'user_birthday'
        ])->redirect();
    }

    public function handleProviderCallback($provider)
    {
        try {
        $userSocial = Socialite::driver($provider)->user();
        $emailSocmed = $userSocial->email;
        if(empty($userSocial->email)) {
            Session::flash('error_message', 'Maaf, kami membutuhkan izin akses untuk mendaftar dengan akun '.$provider.' anda.');
            return redirect('register#');
        }
        } catch (\Exception $e) {
            Session::flash('error_message', 'Maaf, kami membutuhkan izin akses untuk mendaftar dengan akun '.$provider.' anda.');
            return redirect('register#');
        }

        $userCheck = User::where([
                ['email', '=', $userSocial->email],
                ['facebook_id', '=', $userSocial->id],
                ['via', '=', 'fbreg'],
                ])->first();

        if(!empty($userCheck))
        {

            Auth::login($userCheck, true);
            $status = 'success_from_fb';
            $this->createLoginAttempts($userCheck->id,Session::get('ipaddress'),$status);
            return redirect('/dashboard#');

        } else {

            $name = explode(' ', $userSocial->name);
            $email = $userSocial->email;
            $first_name = $name[0];
            if (!empty($name[1])) {
                $name[1] = $name[1];
            } else {
                $name[1] = '-';
            }
            $last_name = $name[1];

            if($userSocial->user['verified']){

            /*$nowTime = Carbon::now();
            $nowYear = date('Y' , strtotime($nowTime));
            $birthDateYear = date('Y' , strtotime($userSocial->user['birthday']));*/

            /*$age = $nowYear - $birthDateYear;*/

            if ($userSocial->user['age_range']['min'] < 18) {
            Session::flash('error_message', 'Maaf usia anda dibawah 18 tahun. Adagigs mensyaratkan anda berumur min. 18 tahun');
            return redirect('/register#');
            } else {

                $emailCheck = User::where([
                    ['email', '=', $userSocial->email],
                    ['username', '=', $name[0]],
                ])->first();

                if(!empty($emailCheck))
                {
                    Session::flash('error_message', 'Maaf email yang anda gunakan telah terdaftar.');
                    return redirect('/register#');

                } else {

                $user = User::create(
                [
                    'first_name' => $name[0],
                    'last_name' => $name[1],
                    'email' => $userSocial->email,
                    'username' => $name[0],
                    'status' => '1',
                    'verification_code' => $this->verCode,
                    'via' => 'fbreg',
                    'type' => 'unknown',
                    'register_ip' => Session::get('ipaddress'),
                    'facebook_id' => $userSocial->user['id'],
                ]);

                /*
                $user                           = new User;
                $user->email                    = $userSocial->email;
                $user->first_name               = $name[0];
                $user->last_name                = $name[1];
                $user->username                 = $name[0];
                $user->facebook_id              = $userSocial->id;
                $user->register_ip              = Session::get('ipaddress');
                $user->status                   = '1';
                $user->verification_code        = $this->verCode;
                $user->via                      = 'fbreg';
                $user->type                     = 'unknown';
                $user->birth_date               = date('Y-m-d' , strtotime($userSocial->user['birthday']));
                $user->save();
                */

                if ($user) {
                $verCode = $this->verCode;
                $dataVer = [ 
                        'firstName' => $user->first_name,
                        'username' => $user->username
                        ];
                Mail::send('emails.registrationSuccess',$dataVer,function($message) use ($user) {
                    $message->from('no-reply@adagigs.com', 'Adagigs');
                    $message->to($user->email, $user->first_name)->subject('Selamat Datang di Adagigs');
                });

                Session::flash('flash_message', 'Registrasi berhasil! , Selamat datang di adagigs.');
                Auth::login($user, true); 
                return redirect('/dashboard/welcome');
                }
                }

            }

            } else {
                Session::flash('error_message', 'Maaf, kami membutuhkan izin akses untuk mendaftar dengan akun '.$provider.' anda.');
                return redirect('register#');
            }
            
            }

    }

}