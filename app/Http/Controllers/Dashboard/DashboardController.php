<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Requests;
use Illuminate\Http\Request;
use Auth;
use Hash;

use App\User;
use App\Band;
use App\Organization;
use App\Notification;

use Session;
use App\Http\Controllers\Controller;
use App\Http\Composers\navMenuComposer;


class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

        $this->middleware('auth');
        $this->authenticate();
        
    }

    public function authenticate()
    {   
        if(Auth::check() && Auth::user()->status == '1'){
            return redirect()->intended('dashboard');
        }
        elseif(Auth::check() && Auth::user()->status == '2'){

            Auth::logout();
            Session::flash('error_message', 'Silahkan verifikasi email anda');
            return redirect('/login');
        }
        else {
            Auth::logout();
            return redirect('/login');
        }
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $userId = Auth::user()->id;
        $User = User::where('id',$userId)->first();

        if ($User->type == 'unknown') {
            return redirect('dashboard/welcome/');
        } else {

            if ($User->type == 'organization') {
                    $Organization = Organization::where('user_id',$userId)->first();
                    $unreadNotifications = Notification::where([
                        ['object_type','App\Organization'],
                        ['object_id',$Organization->id],
                    ])->with('gig')->orderBy('created_at','desc')->simplePaginate(5);

                    $notificationCount = Notification::where([
                        ['object_type','App\Organization'],
                        ['object_id',$Organization->id],
                    ])->unread()->count();

                    return view('dashboard.dashboardIndex',[
                        'User' => $User,
                        'unreadNotifications' => $unreadNotifications,
                    ]);

            } else {
                    $Band = Band::where('user_id',$userId)->first();
                    $unreadNotifications = Notification::where([
                        ['object_type','App\Band'],
                        ['object_id',$Band->id],
                    ])->with('gig')->orderBy('created_at','desc')->simplePaginate(5);

                    $notificationCount = Notification::where([
                        ['object_type','App\Band'],
                        ['object_id',$Band->id],
                    ])->unread()->count();

                    return view('dashboard.dashboardIndex',[
                        'User' => $User,
                        'unreadNotifications' => $unreadNotifications,
                    ]);
            }
            
        }
    }

    public function readNotif($id)
    {
        $Notification = Notification::where([
                            ['id',$id],
                        ])->with('gig')->first();
        $Notification->is_read = $Notification->is_read + 1;
        $Notification->save();

        return redirect('dashboard/gig/'.$Notification->gig->id.'-'.$Notification->gig->slug);

    }

    public function welcome()
    {
        $userId = Auth::user()->id;
        $User = User::where('id',$userId)->first();

        if ($User->type == 'unknown') {
            return view('dashboard.dashboardWelcome',[
                'User' => $User,
            ]);
        } else {
            return redirect('/dashboard');
        }

        
    }

    public function settings(Request $request)
    {
        $userId = Auth::user()->id;
        $User = User::where('id',$userId)->first();


        return view('dashboard.dashboardSettings',[
                'username' => $User->username,
                'first_name' => $User->first_name,
                'last_name' => $User->last_name,
                'tabName' => 'emailSet',
                'User' => $User,
            ]);
    }

    public function settingsPassword(Request $request)
    {
         $validator = $this->validate($request,[
            'old_password' => 'required|min:4',
            'password' => 'required|min:4|confirmed',
            'password_confirmation' => 'required|min:4',
        ]);

        $userId = Auth::user()->id;
        $User = User::where('id',$userId)->first();

        if (Hash::check($request['old_password'], $User->password)) {
            $User->password =  Hash::make($request['password']);
            $User->save();
            Session::flash('flash_message', 'Password berhasil diubah');
            return redirect('dashboard/settings/');
        } else {
            Session::flash('error_message', 'Password salah');
            return view('dashboard.dashboardSettings',[
                'username' => $User->username,
                'email' => $User->email,
                'first_name' => $User->first_name,
                'last_name' => $User->last_name,
                'tabName' => 'passRes'
            ]);
        }
    }

    public function settingsAccount(Request $request)
    {
        $validator = $this->validate($request,[
            'first_name' => 'required|max:64',
            'last_name' => 'required|max:64',
            'password_check' => 'required|max:64',
        ]);

        $userId = Auth::user()->id;
        $User = User::where('id',$userId)->first();

        if (Hash::check($request['password_check'], $User->password)) {

        if ($User->username == $request->input('username')) {
            $validator = $this->validate($request,[
                'username' => 'required|max:64',
            ]);
        } else {
            $validator = $this->validate($request,[
                'username' => 'required|max:64|unique:users',
            ]);
        }

        $User->username =  $request['username'];
        $User->first_name =  $request['first_name'];
        $User->last_name =  $request['last_name'];
        $User->save();

        Session::flash('flash_message', 'Akun berhasil diubah');
        return redirect('dashboard/settings/');

        } else {
            Session::flash('error_message', 'Password salah');
            return view('dashboard.dashboardSettings',[
                'username' => $User->username,
                'first_name' => $User->first_name,
                'last_name' => $User->last_name,
                'tabName' => 'emailSet'
            ]);
        }
    }
}
