<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Intervention\Image\Filters\FilterInterface;
use Image;
use Illuminate\Support\Facades\Input;

use App\User;
use App\Organization;
use App\OrganizationType;
use App\OrganizationAddress;

use Auth;
use Session;


class OrganizationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('checkaccountorg');
    }

    protected function userId()
    {
        $userId = Auth::user()->id;
        return $userId;
    }

    protected function getUser()
    {

        $User = User::where('id',$this->userId())->first();
        return $User;
    }

    /* CRUD REQUEST */

    public function index()
    {

    	$Organization = Organization::where('user_id',$this->userId())->first();
        return view('dashboard.organization.organizationIndex',[
                'Organization' => $Organization,
                'User' => $this->getUser()
            ]);
    }

    public function create()
    {

        $OrganizationCount = Organization::where('user_id',Auth::user()->id)->count();

        if($OrganizationCount == 0) {
    	$Organization = Organization::where('user_id',$this->userId())->first();
    	$OrganizationTypes = OrganizationType::pluck('organization_type_name', 'id');
        return view('dashboard.organization.organizationCreate',[
                'Organization' => $Organization,
                'OrganizationTypes' => $OrganizationTypes,
                'User' => $this->getUser()
            ]);
        } else {
            return redirect('dashboard/organization');
        }

    	
    }

    public function store(Request $request)
    {

    	$validator = $this->validate($request,[
            'organization_name' => 'required|max:64',
            'organization_type' => 'required',
            'address' => 'required',
            'city' => 'required',
            'postcode' => 'required',
            'phonenumber' => 'required',
            'profile_img' => 'required|mimes:jpeg,png',
            'url_organization_name' => 'required|max:64|unique:organizations',
            'facebook_url' => 'unique:organizations|max:64',
            'twitter_url' => 'unique:organizations|max:64',
            'instagram_url' => 'unique:organizations|max:64',
            'website_url' => 'max:64',
        ]);



        if ($request->file('profile_img')->isValid()) {

        $image = Input::file('profile_img');
        $img = Image::make($image);
        $imgSize = $img->filesize();

                if ($imgSize >= 1000000) {
                    Session::flash('error_message', 'Ukuran gambar > 1MB');
                    return redirect()->back()->withInput();
                
                } else {

                $filename = date('Y-m-d')."-".$image->getClientOriginalName();
                $destinationPath = 'uploads/organizations/';
                Input::file('profile_img')->move($destinationPath, $filename);
                $OrganizationSlug = str_slug($request->input('organization_name'), "-");

                $Organization = new Organization();
                $Organization->organization_name            = $request->input('organization_name');
                $Organization->organization_type_id         = $request->input('organization_type');
                $Organization->url_organization_name        = $request->input('url_organization_name');
                $Organization->organization_website         = $request->input('website_url');
                $Organization->user_id                      = $this->userId();
                $Organization->image_file_path              = $filename;
                $Organization->twitter_url                  = $request->input('twitter_url');
                $Organization->facebook_url                 = $request->input('facebook_url');
                $Organization->instagram_url                = $request->input('instagram_url');
                $Organization->slug                         = $OrganizationSlug;
                $Organization->save();

                $OrganizationAddress = new OrganizationAddress();
                $OrganizationAddress->organization_id       = $Organization->id;
                $OrganizationAddress->city_id               = $request->input('city');
                $OrganizationAddress->address               = $request->input('address');
                $OrganizationAddress->postcode              = $request->input('postcode');
                $OrganizationAddress->phonenumber           = $request->input('phonenumber');
                $OrganizationAddress->save();

                $User = User::where('id',$this->userId())->first();
                $User->type = 'organization';
                $User->save();

                Session::flash('flash_message', 'Akun berhasil dibuat');
                return redirect('dashboard/organization');

                }

        } else {
            Session::flash('error_message', 'Gambar tidak valid');
            return redirect()->back();
        }

    }

    public function view($id)
    {

        $Organization = Organization::findOrFail($id);
        if ($this->userId() == $Organization->user_id){
        $Organization = Organization::where('id',$id)->with('organizationtype')->first();
        $OrganizationAddress = OrganizationAddress::where('organization_id',$id)->with('city.province')->first();

        return view('dashboard.organization.organizationView',[
                'Organization' => $Organization,
                'OrganizationAddress' => $OrganizationAddress,
                'User' => $this->getUser()
            ]);
        } else {
            return redirect('dashboard/organization');
        }
    
    	
    }

    public function edit($id)
    {

        $Organization = Organization::findOrFail($id);
        if ($this->userId() == $Organization->user_id){
        $Organization = Organization::where('id',$id)->with('organizationtype')->first();
        $OrganizationAddress = OrganizationAddress::where('organization_id',$id)->with('city.province')->first();
        $OrganizationTypes = OrganizationType::all();
        return view('dashboard.organization.organizationEdit',[
            'OrganizationTypes' => $OrganizationTypes,
            'Organization' => $Organization,
            'OrganizationAddress' => $OrganizationAddress,
            'User' => $this->getUser()
            ]);
        } else {
            return redirect('dashboard/organization');
        }


    	
    }

    public function update($id,Request $request)
    {

        $Organization = Organization::findOrFail($id);
        if ($this->userId() == $Organization->user_id){
        $validator = $this->validate($request,[
            'organization_name' => 'required|max:64',
            'organization_type' => 'required',
            'address' => 'required',
            'city' => 'required',
            'postcode' => 'required',
            'phonenumber' => 'required',
            'website_url' => 'max:64',
        ]);

        if ($Organization->url_organization_name == $request->input('url_organization_name')) {
            $validator = $this->validate($request,[
                'url_organization_name' => 'required|max:64',
            ]);
        } else {
            $validator = $this->validate($request,[
                'url_organization_name' => 'required|max:64|unique:organizations',
            ]);
        }

        if ($Organization->facebook_url == $request->input('facebook_url')) {
            $validator = $this->validate($request,[
                'facebook_url' => 'max:64',
            ]);
        } else {
            $validator = $this->validate($request,[
                'facebook_url' => 'max:64|unique:organizations',
            ]);
        }

        if ($Organization->twitter_url == $request->input('twitter_url')) {
            $validator = $this->validate($request,[
                'twitter_url' => 'max:64',
            ]);
        } else {
            $validator = $this->validate($request,[
                'twitter_url' => 'max:64|unique:organizations',
            ]);
        }

        if ($Organization->instagram_url == $request->input('instagram_url')) {
            $validator = $this->validate($request,[
                'instagram_url' => 'max:64',
            ]);
        } else {
            $validator = $this->validate($request,[
                'instagram_url' => 'max:64|unique:organizations',
            ]);
        }

                if ($request->hasFile('profile_img')) {

                    if ($request->file('profile_img')->isValid()) {

                        $validator = $this->validate($request,[
                            'profile_img' => 'required|mimes:jpeg,png',
                        ]);

                        $image = Input::file('profile_img');
                        $img = Image::make($image);
                        $imgSize = $img->filesize();

                        if ($imgSize >= 1000000) {
                        Session::flash('error_message', 'Ukuran gambar > 1MB');
                        return redirect()->back()->withInput();
                
                        } else {

                        $filename = date('Y-m-d')."-".$image->getClientOriginalName();
                        $destinationPath = 'uploads/organizations/';
                        Input::file('profile_img')->move($destinationPath, $filename);
                        $OrganizationSlug = str_slug($request->input('organization_name'), "-");

                        $OrganizationSlug = str_slug($request->input('organization_name'), "-");
                        $Organization = Organization::findOrFail($id);
                        $Organization->organization_name            = $request->input('organization_name');
                        $Organization->organization_type_id         = $request->input('organization_type');
                        $Organization->url_organization_name        = $request->input('url_organization_name');
                        $Organization->organization_website         = $request->input('website_url');
                        $Organization->user_id                      = $this->userId();
                        $Organization->twitter_url                  = $request->input('twitter_url');
                        $Organization->facebook_url                 = $request->input('facebook_url');
                        $Organization->instagram_url                = $request->input('instagram_url');
                        $Organization->image_file_path              = $filename;
                        $Organization->slug                         = $OrganizationSlug;
                        $Organization->save();

                        $OrganizationAddress = OrganizationAddress::where('organization_id',$id)->first();
                        $OrganizationAddress->organization_id       = $Organization->id;
                        $OrganizationAddress->city_id               = $request->input('city');
                        $OrganizationAddress->address               = $request->input('address');
                        $OrganizationAddress->postcode              = $request->input('postcode');
                        $OrganizationAddress->phonenumber           = $request->input('phonenumber');
                        $OrganizationAddress->save();

                        Session::flash('flash_message', 'Akun berhasil dibuat');
                        return redirect('dashboard/organization/'.$Organization->id.'-'.$Organization->slug);

                    }

                    } else {

                        // sending back with error message.
                        Session::flash('error', 'uploaded file is not valid');
                        return Redirect::to('upload');
                    }

                } else {

                $OrganizationSlug = str_slug($request->input('organization_name'), "-");
                $Organization = Organization::findOrFail($id);
                $Organization->organization_name            = $request->input('organization_name');
                $Organization->organization_type_id         = $request->input('organization_type');
                $Organization->url_organization_name        = $request->input('url_organization_name');
                $Organization->organization_website         = $request->input('website_url');
                $Organization->user_id                      = $this->userId();
                $Organization->twitter_url                  = $request->input('twitter_url');
                $Organization->facebook_url                 = $request->input('facebook_url');
                $Organization->instagram_url                = $request->input('instagram_url');
                $Organization->slug                         = $OrganizationSlug;
                $Organization->save();

                $OrganizationAddress = OrganizationAddress::where('organization_id',$id)->first();
                $OrganizationAddress->organization_id       = $Organization->id;
                $OrganizationAddress->city_id               = $request->input('city');
                $OrganizationAddress->address               = $request->input('address');
                $OrganizationAddress->postcode              = $request->input('postcode');
                $OrganizationAddress->phonenumber           = $request->input('phonenumber');
                $OrganizationAddress->save();

                Session::flash('flash_message', 'Akun berhasil dibuat');
                return redirect('dashboard/organization/'.$Organization->id.'-'.$Organization->slug);

                }

        } else {
            return redirect('dashboard/organization');
        }
    }

    public function destroy($id){

        $Organization = Organization::findOrFail($id);
        if ($this->userId() == $Organization->user_id){
        $Organization = Organization::findOrFail($id);
        $Organization->delete();

        $OrganizationAddress = OrganizationAddress::where('organization_id',$id)->first();
        $OrganizationAddress->delete();

        Session::flash('flash_message', 'Akun berhasil dihapus');
        return redirect('dashboard/organization');
        } else {
            return redirect('dashboard/organization');
        }
    }
}
