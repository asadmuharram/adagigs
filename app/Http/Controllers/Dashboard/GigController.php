<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

use Carbon\Carbon;
use Mail;
use App\User;
use App\Gig;
use App\Organization;
use App\Genre;
use App\GigType;
use App\GigGenre;
use App\GigLocation;
use App\GigBand;
use App\Band;
use App\Notification;

use Auth;
use Session;

class GigController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    protected function userId()
    {
        $userId = Auth::user()->id;
        return $userId;
    }

    protected function getUser()
    {

        $User = User::where('id',$this->userId())->first();
        return $User;
    }

    /* CRUD REQUEST */

    public function index()
    {
    	$userId = Auth::user()->id;
        if(Auth::user()->type == 'organization') {
            $Gigs = Gig::where('user_id',$userId)->get();
            return view('dashboard.gig.gigIndex',[
                'Gigs' => $Gigs,
                'User' => $this->getUser()
            ]);
        } elseif(Auth::user()->type == 'band') {
            $Band        =   Band::where('user_id',Auth::user()->id)->first();
            $GigBands    =   GigBand::where('band_id',$Band->id)->with('gig.organization')->get();
            return view('dashboard.gig.gigIndex',[
                'Band'  => $Band,
                'GigBands'   => $GigBands,
                'User'  => $this->getUser()
            ]);
        } else {
            return redirect('dashboard/welcome/');
        }
    	
    }

    public function create()
    {
    	$userId = Auth::user()->id;
        if (Auth::user()->type == 'organization') {
            $Organization = Organization::where('user_id',$userId)->first();
            $GigTypes = GigType::pluck('gig_type_name', 'id');
            $Genres = Genre::pluck('genre_name', 'id');
            return view('dashboard.gig.gigCreate',[
                    'Organization' => $Organization,
                    'Genres' => $Genres,
                    'GigTypes' => $GigTypes,
                    'User' => $this->getUser()
            ]);
        } else {
            return redirect('dashboard/gig');
        }
    	
    }

    public function store(Request $request)
    {
    	$validator = $this->validate($request,[
            'gig_name' => 'required|max:64',
            'gig_img' => 'required',
            'gig_description' => 'required|max:1000',
            'gig_description_requirement' => 'required|max:1000',
            'gig_type' => 'required',
            'genre' => 'required',
            'start_opening_date' => 'required',
            'start_closing_date' => 'required',
            'time_opening_date' => 'required',
            'time_closing_date' => 'required',
            'gig_date' => 'required',
            'total_band' => 'required',
            'venue_name' => 'required',
            'city' => 'required',
        ]);

        $timeOpening = $request->input('time_opening_date').':00:00';
        $timeClosing = $request->input('time_closing_date').':00:00';

        $dateTimeOpening = $request->input('start_opening_date').' '.$timeOpening;
        $dateTimeClosing = $request->input('start_closing_date').' '.$timeClosing;

        $start_opening_date = new Carbon($dateTimeOpening);
        $start_closing_date = new Carbon($dateTimeClosing);
        $gig_date = new Carbon($request->input('gig_date'));

        if ($request->file('gig_img')->isValid()) {
        			$image = Input::file('gig_img');
			    	$filename = date('Y-m-d')."-".$image->getClientOriginalName();
			    	$destinationPath = 'uploads/gigs/';
			 		Input::file('gig_img')->move($destinationPath, $filename);

        $userId = Auth::user()->id;
        $Organization = Organization::where('user_id',$userId)->first();

        $gigSlug = str_slug($request->input('gig_name'), "-");

        $genres = Input::get('genre');

        if (count($genres) > 3) {
            Session::flash('error_message', 'Genre yang dipilih maks 3.');
            return redirect()->back()->withInput();
        } else {

        $Gig = new Gig();
        $Gig->gig_name    				= $request->input('gig_name');
		$Gig->gigtype_id    			= $request->input('gig_type');
		$Gig->gig_description    		= $request->input('gig_description');
        $Gig->gig_band_requirement      = $request->input('gig_description_requirement');
		$Gig->gig_band_opening_date    	= $start_opening_date;
		$Gig->gig_band_closing_date    	= $start_closing_date;
		$Gig->gig_date    				= $gig_date;
		$Gig->gig_total_band    		= $request->input('total_band');
		$Gig->user_id    				= $userId;
		$Gig->organization_id    		= $Organization->id;
		$Gig->status    				= 'open';
		$Gig->slug    					= $gigSlug;
		$Gig->gig_image_file_path    	= $filename;
		$Gig->save();

        foreach (Input::get('genre') as $genre) {
            $GigGenre = new GigGenre();
            $GigGenre->gig_id       = $Gig->id;
            $GigGenre->genre_id     = $genre;
            $GigGenre->save();
        }

        $GigLocation = new GigLocation();
        $GigLocation->gig_id            = $Gig->id;
        $GigLocation->city_id           = $request->input('city');
        $GigLocation->venue_name        = $request->input('venue_name');
        $GigLocation->save();

        Session::flash('flash_message', 'Gig berhasil dibuat');
        return redirect('dashboard/gig/');
        
        }
        

        } else {
        			Session::flash('error_message', 'Gambar tidak valid');
        			return redirect()->back();
        		}

    }

    public function setTime($time)
    {
        Carbon::setLocale('id');
        $timeSet = new Carbon($time , 'Asia/Jakarta');
        return $timeSet;

    }

    public function view($id)
    {

        $Gig = Gig::findOrFail($id);
        if (Auth::user()->id == $Gig->user_id){
        $Gig = Gig::where('id',$id)->first();
        $GigLocation = GigLocation::where('gig_id',$id)->with('city.province')->first();
        $GigGenres = GigGenre::where('gig_id',$id)->with('genre')->get();
        $GigBands = GigBand::where('gig_id',$id)->with('band')->get();

        $openingDate    = $this->setTime($Gig->gig_band_opening_date);
        $closingDate    = $this->setTime($Gig->gig_band_closing_date);
        $gigDate        = $this->setTime($Gig->gig_date);

        $nowTime = Carbon::now();

        return view('dashboard.gig.gigView',[
                'Gig' => $Gig,
                'openingDate' => $openingDate,
                'closingDate' => $closingDate,
                'gigDate' => $gigDate,
                'GigLocation' => $GigLocation,
                'GigGenres' => $GigGenres,
                'nowTime' => $nowTime,
                'GigBands' => $GigBands,
                'User' => $this->getUser()
            ]);
        } else {
            return redirect('dashboard/gig');
        }
    
        
    }

    public function edit($id)
    {

        $Gig = Gig::findOrFail($id);
        if (Auth::user()->id == $Gig->user_id){
        $Gig = Gig::where('id',$id)->first();
        $GigLocation = GigLocation::where('gig_id',$id)->with('city.province')->first();
        $GigGenres = GigGenre::where('gig_id',$id)->lists('genre_id')->all();
        $GigBands = GigBand::where('gig_id',$id)->with('band')->get();
        $GigTypes = GigType::pluck('gig_type_name', 'id');
        $Genres = Genre::pluck('genre_name', 'id');

        $openingDate    = $this->setTime($Gig->gig_band_opening_date);
        $closingDate    = $this->setTime($Gig->gig_band_closing_date);
        $gigDate        = $this->setTime($Gig->gig_date);

        $openingDate = date('m/d/Y', strtotime($openingDate));
        $openingTime = date('H', strtotime($Gig->gig_band_opening_date));
        $closingDate = date('m/d/Y', strtotime($closingDate));
        $closingTime = date('H', strtotime($Gig->gig_band_closing_date));
        $gigDate = date('m/d/Y', strtotime($gigDate));


        $nowTime = Carbon::now();

        return view('dashboard.gig.gigEdit',[
                'Gig' => $Gig,
                'openingDate' => $openingDate,
                'closingDate' => $closingDate,
                'gigDate' => $gigDate,
                'GigLocation' => $GigLocation,
                'GigGenres' => $GigGenres,
                'nowTime' => $nowTime,
                'GigBands' => $GigBands,
                'GigTypes' => $GigTypes,
                'Genres' => $Genres,
                'openingTime' => $openingTime,
                'closingTime' => $closingTime,
                'User' => $this->getUser()

            ]);
        } else {
            return redirect('dashboard/gig');
        }
    
        
    }

    public function update(Request $request,$id)
    {
        $validator = $this->validate($request,[
            'gig_name' => 'required|max:64',
            'gig_description' => 'required|max:1000',
            'gig_description_requirement' => 'required|max:1000',
            'gig_type' => 'required',
            'genre' => 'required',
            'start_opening_date' => 'required',
            'start_closing_date' => 'required',
            'time_opening_date' => 'required',
            'time_closing_date' => 'required',
            'gig_date' => 'required',
            'total_band' => 'required',
            'venue_name' => 'required',
            'city' => 'required',
        ]);

        $timeOpening = $request->input('time_opening_date').':00:00';
        $timeClosing = $request->input('time_closing_date').':00:00';

        $dateTimeOpening = $request->input('start_opening_date').' '.$timeOpening;
        $dateTimeClosing = $request->input('start_closing_date').' '.$timeClosing;

        $start_opening_date = new Carbon($dateTimeOpening);
        $start_closing_date = new Carbon($dateTimeClosing);
        $gig_date = new Carbon($request->input('gig_date'));

        if ($request->hasFile('gig_img')) {

                if ($request->file('gig_img')->isValid()) {
                            $image = Input::file('gig_img');
                            $filename = date('Y-m-d')."-".$image->getClientOriginalName();
                            $destinationPath = 'uploads/gigs/';
                            Input::file('gig_img')->move($destinationPath, $filename);

                $userId = Auth::user()->id;
                $Organization = Organization::where('user_id',$userId)->first();

                $gigSlug = str_slug($request->input('gig_name'), "-");

                $genres = Input::get('genre');

                if (count($genres) > 3) {
                    Session::flash('error_message', 'Genre yang dipilih maks 3.');
                    return redirect()->back()->withInput();
                } else {

                $Gig = Gig::findOrFail($id);
                $Gig->gig_name                  = $request->input('gig_name');
                $Gig->gigtype_id                = $request->input('gig_type');
                $Gig->gig_description           = $request->input('gig_description');
                $Gig->gig_band_requirement      = $request->input('gig_description_requirement');
                $Gig->gig_band_opening_date     = $start_opening_date;
                $Gig->gig_band_closing_date     = $start_closing_date;
                $Gig->gig_date                  = $gig_date;
                $Gig->gig_total_band            = $request->input('total_band');
                $Gig->user_id                   = $userId;
                $Gig->organization_id           = $Organization->id;
                $Gig->status                    = 'open';
                $Gig->slug                      = $gigSlug;
                $Gig->gig_image_file_path       = $filename;
                $Gig->save();

                GigGenre::where('gig_id',$id)->delete();

                foreach (Input::get('genre') as $genre) {
                    $GigGenre = new GigGenre();
                    $GigGenre->gig_id       = $Gig->id;
                    $GigGenre->genre_id     = $genre;
                    $GigGenre->save();
                }

                $GigLocation                    = GigLocation::where('gig_id',$id)->first();
                $GigLocation->gig_id            = $Gig->id;
                $GigLocation->city_id           = $request->input('city');
                $GigLocation->venue_name        = $request->input('venue_name');
                $GigLocation->save();

                Session::flash('flash_message', 'Gig berhasil diubah');
                return redirect('dashboard/gig/'.$Gig->id.'-'.$Gig->slug);

                }

                } else {
                            Session::flash('error_message', 'Gambar tidak valid');
                            return redirect()->back();
                        }
        } else {

                $userId = Auth::user()->id;
                $Organization = Organization::where('user_id',$userId)->first();

                $gigSlug = str_slug($request->input('gig_name'), "-");

                $genres = Input::get('genre');

                if (count($genres) > 3) {
                    Session::flash('error_message', 'Genre yang dipilih maks 3.');
                    return redirect()->back()->withInput();
                } else {

                $Gig = Gig::findOrFail($id);
                $Gig->gig_name                  = $request->input('gig_name');
                $Gig->gigtype_id                = $request->input('gig_type');
                $Gig->gig_description           = $request->input('gig_description');
                $Gig->gig_band_requirement      = $request->input('gig_description_requirement');
                $Gig->gig_band_opening_date     = $start_opening_date;
                $Gig->gig_band_closing_date     = $start_closing_date;
                $Gig->gig_date                  = $gig_date;
                $Gig->gig_total_band            = $request->input('total_band');
                $Gig->user_id                   = $userId;
                $Gig->organization_id           = $Organization->id;
                $Gig->status                    = 'open';
                $Gig->slug                      = $gigSlug;
                $Gig->save();

                GigGenre::where('gig_id',$id)->delete();
            
                foreach (Input::get('genre') as $genre) {
                    $GigGenre = new GigGenre();
                    $GigGenre->gig_id       = $Gig->id;
                    $GigGenre->genre_id     = $genre;
                    $GigGenre->save();
                }

                $GigLocation                    = GigLocation::where('gig_id',$id)->first();
                $GigLocation->gig_id            = $Gig->id;
                $GigLocation->city_id           = $request->input('city');
                $GigLocation->venue_name        = $request->input('venue_name');
                $GigLocation->save();

                Session::flash('flash_message', 'Gig berhasil diubah');
                return redirect('dashboard/gig/'.$Gig->id.'-'.$Gig->slug);

                }
        }

    }

    public function gigBand($id)
    {   
        $Gig = Gig::findOrFail($id);

        $nowTime = Carbon::now();

        if ($nowTime > $Gig->gig_band_closing_date) {

            Session::flash('error_message', 'Maaf, Anda tidak dapat mengundang musisi / band karena waktu registrasi telah selesai.');
            return redirect()->back();
            
        } else {

            if (Auth::user()->id == $Gig->user_id){
            $Gig = Gig::where('id',$id)->first();
            return view('dashboard.gig.gigBandAdd',[
                    'Gig' => $Gig,
                    'User' => $this->getUser()
                ]);
            } else {
                return redirect('dashboard/gig');
            }
        }
    
    }

    public function gigBandAdd(Request $request,$id)
    {   

        $validator = $this->validate($request,[
            'band' => 'required|max:64',
        ]);

        $gigBandCount = GigBand::where('gig_id',$id)
                                ->where('band_id', $request->input('band'))
                                ->get();

        if (count($gigBandCount) > 0) {

            Session::flash('error_message', 'Maaf, Musisi / Band sudah pernah diundang dalam gig ini, silahkan undang musisi / band lain');
            return redirect()->back();
        } else {

            $Gig = Gig::findOrFail($id);
            $GigBand = new GigBand;
            $GigBand->gig_id = $id;
            $GigBand->band_id = $request->input('band');
            $GigBand->status = 'pending';
            $GigBand->counter = 1;
            $GigBand->request_by = 'organization';
            $GigBand->save();

            $Band = Band::findOrFail($GigBand->band_id);
            $Organization = Organization::findOrFail($Gig->organization_id);

            $user = User::find(Auth::user()->id);
            $user->newNotification()
                ->withType('inviteBand')
                ->withSubject('Undangan Ikut Serta Gig')
                ->withBody('<a href="organization/'.$Organization->url_organization_name.'">'.$Organization->organization_name.'</a> mengundang anda untuk ikut serta dalam gig '.$Gig->gig_name)
                ->regarding($Band)
                ->withGigId($Gig->id)
                ->deliver();

            $userBand = User::find($Band->user_id);    

            $dataVer = [ 
                'orgName' => $Organization->organization_name,
                'gigName' => $Gig->gig_name,
                'bandName' => $Band->band_name,
            ];
            Mail::send('emails.bandInvite',$dataVer,function($message) use ($userBand) {
                        $message->from('no-reply@adagigs.com', 'Adagigs');
                        $message->to($userBand->email, $userBand->first_name)->subject('Permintaan Ikut Serta Gig');
            });
            Session::flash('flash_message', 'Musisi / Band berhasil diundang');
            return redirect('dashboard/gig/'.$Gig->id.'-'.$Gig->slug);
        }   
        
    }

    public function changeStatus($id,Request $request)
    {
        $validator = $this->validate($request,[
            'status' => 'required',
        ]);
    
        $Gig = Gig::findOrFail($id);
        $Gig->status      =     $request->input('status');
        $Gig->save();

        Session::flash('flash_message', 'Status Gig berhasil diubah');
        return redirect()->back();
    }

    public function changeBandStatus(Request $request,$id,$bandid)
    {
        $validator = $this->validate($request,[
            'apply_status' => 'required',
        ]);

        $status = $request->input('apply_status');

        $Gig = Gig::findOrFail($id);
        $gigBandCount = GigBand::where([
                                        ['gig_id',$id],
                                        ['status','accept'],
                                    ])->count();       
    
        $GigBand                = GigBand::where([
                                        ['gig_id',$id],
                                        ['band_id',$bandid],
                                    ])->first();

        if ($request->input('apply_status') == 'accept') {

            if ($gigBandCount >= $Gig->gig_total_band) {
                Session::flash('error_message', 'Jumlah Band Sudah memenuhi kuota');
                return redirect()->back();
        }
        else {
            if ($GigBand->counter < 2) {

                $GigBand->status        = $status;
                $GigBand->counter       = $GigBand->counter + 1;

                $GigBand->save();

                if ($GigBand->request_by == 'organization') {

                    $Band = Band::findOrFail($GigBand->band_id);
                    $Organization = Organization::findOrFail($Gig->organization_id);
                    $user = User::find(Auth::user()->id);

                    $notificationType = 'inviteStatus';
                    $notificationSubject = 'Status undangan band';
                    $notificationBody = 'Selamat '.$Band->band_name.' menerima undangan anda.';

                    $user->newNotification()
                        ->withType($notificationType)
                        ->withSubject($notificationSubject)
                        ->withBody($notificationBody)
                        ->withGigId($Gig->id)
                        ->regarding($Organization)
                        ->deliver();

                $userOrg = User::find($Organization->user_id);   

                $dataVer = [ 
                    'orgName' => $Organization->organization_name,
                    'gigName' => $Gig->gig_name,
                    'bandName' => $Band->band_name,
                ];
                Mail::send('emails.bandAccept',$dataVer,function($message) use ($userOrg,$Band) {
                    $message->from('no-reply@adagigs.com', 'Adagigs');
                    $message->to($userOrg->email, $userOrg->first_name)->subject($Band->band_name.' Menerima undangan anda');
                });

                } else {

                    $Band = Band::findOrFail($GigBand->band_id);
                    $Organization = Organization::findOrFail($Gig->organization_id);
                    $user = User::find(Auth::user()->id);

                    $notificationType = 'inviteStatus';
                    $notificationSubject = 'Status permintaan ikut serta gig';
                    $notificationBody = 'Selamat <a href="/organization/'.$Organization->url_organization_name.'">'.$Organization->organization_name.'</a> menerima permintaan anda untuk ikut serta dalam gig '.$Gig->gig_name;

                    $user->newNotification()
                        ->withType($notificationType)
                        ->withSubject($notificationSubject)
                        ->withBody($notificationBody)
                        ->withGigId($Gig->id)
                        ->regarding($Band)
                        ->deliver();

                $userBand = User::find($Band->user_id);    

                $dataVer = [ 
                    'orgName' => $Organization->organization_name,
                    'gigName' => $Gig->gig_name,
                    'bandName' => $Band->band_name,
                ];
                Mail::send('emails.gigAccept',$dataVer,function($message) use ($userBand,$Organization) {
                    $message->from('no-reply@adagigs.com', 'Adagigs');
                    $message->to($userBand->email, $userBand->first_name)->subject($Organization->organization_name.' Menerima undangan anda');
                });
                    
                }

                Session::flash('flash_message', 'Status Band berhasil diubah');
                return redirect()->back();

            } else {

                Session::flash('error_message', 'Maaf anda tidak dapat mengubah permintaan ini.');
                return redirect()->back();
            }
        }

        } else {

        if ($GigBand->counter < 2) {

                $GigBand->status        = $request->input('apply_status');
                $GigBand->counter       = $GigBand->counter + 1;
                $GigBand->save();

                if ($GigBand->request_by == 'organization') {

                    $Band = Band::findOrFail($GigBand->band_id);
                    $Organization = Organization::findOrFail($Gig->organization_id);
                    $user = User::find(Auth::user()->id);

                    $notificationType = 'inviteStatus';
                    $notificationSubject = 'Status Undangan Band';
                    $notificationBody = $Band->band_name.' menolak permintaan anda.';

                    $user->newNotification()
                        ->withType($notificationType)
                        ->withSubject($notificationSubject)
                        ->withBody($notificationBody)
                        ->withGigId($Gig->id)
                        ->regarding($Organization)
                        ->deliver();

                    $userOrg = User::find($Organization->user_id);   

                    $dataVer = [ 
                        'orgName' => $Organization->organization_name,
                        'gigName' => $Gig->gig_name,
                        'bandName' => $Band->band_name,
                    ];
                    Mail::send('emails.bandReject',$dataVer,function($message) use ($userOrg,$Band) {
                        $message->from('no-reply@adagigs.com', 'Adagigs');
                        $message->to($userOrg->email, $userOrg->first_name)->subject($Band->band_name.' Menolak undangan anda');
                    });

                } else {

                    $Band = Band::findOrFail($GigBand->band_id);
                    $Organization = Organization::findOrFail($Gig->organization_id);
                    $user = User::find(Auth::user()->id);

                    $notificationType = 'inviteStatus';
                    $notificationSubject = 'Status Permintaan Ikut Serta Gig';
                    $notificationBody = '<a href="/organization/'.$Organization->url_organization_name.'">'.$Organization->organization_name.'</a> menolak permintaan anda.';

                    $user->newNotification()
                        ->withType($notificationType)
                        ->withSubject($notificationSubject)
                        ->withBody($notificationBody)
                        ->withGigId($Gig->id)
                        ->regarding($Band)
                        ->deliver();

                    $userBand = User::find($Band->user_id);    

                    $dataVer = [ 
                    'orgName' => $Organization->organization_name,
                    'gigName' => $Gig->gig_name,
                    'bandName' => $Band->band_name,
                    ];
                    Mail::send('emails.gigReject',$dataVer,function($message) use ($userBand,$Organization) {
                    $message->from('no-reply@adagigs.com', 'Adagigs');
                    $message->to($userBand->email, $userBand->first_name)->subject($Organization->organization_name.' Menolak undangan anda');
                    });
                    
                }

                Session::flash('flash_message', 'Status Band berhasil diubah');
                return redirect()->back();

            } else {

                Session::flash('error_message', 'Maaf anda tidak dapat mengubah permintaan ini.');
                return redirect()->back();
            }

        }

    }

    /* AJAX REQUEST */

    public function ajaxBands(Request $request)
    {
        $term = $request->term ?: '';
        $Bands = Band::where('band_name', 'like', '%'.$term.'%')->get();
        $valid_bands = array();
        foreach ($Bands as $id => $Band) {
            $valid_bands[] = array('band_id' => $Band->id, 'band_name' => $Band->band_name);
        }
        return \Response::json($valid_bands);
    }
}
