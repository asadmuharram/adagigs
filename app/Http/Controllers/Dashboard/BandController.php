<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Requests;
use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Genre;
use App\Province;
use App\City;
use App\Band;
use App\BandType;
use App\BandLocation;
use App\BandGenre;
use App\SocmedAccountBands;
use Session;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Response;
use Image;
use Illuminate\Support\Facades\Input;

class BandController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('checkaccountband');
    }

    protected function userId()
    {
        $userId = Auth::user()->id;
        return $userId;
    }

    protected function getUser()
    {

        $User = User::where('id',$this->userId())->first();
        return $User;
    }

    /* CRUD REQUEST */

    public function index()
    {
    	$userId = Auth::user()->id;
    	$Bands 	= Band::where('user_id',$userId)->get();

    	return view('dashboard.band.bandIndex',[
    			'Bands' => $Bands,
                'User' => $this->getUser()
    		]);
    }

    public function view($id)
    {
        $Band = Band::findOrFail($id);
        if (Auth::user()->id == $Band->user_id){

            $Band = Band::where('id',$id)->with('bandtype')->first();
            $BandGenre = BandGenre::where('band_id',$id)->with('genre')->first();
            $BandLocation = BandLocation::where('band_id',$id)->with('city.province')->first();

            return view('dashboard.band.bandView',[
                'Band' => $Band,
                'BandGenre' => $BandGenre,
                'BandLocation' => $BandLocation,
                'User' => $this->getUser()
            ]);
        } else {
            return redirect('dashboard/band');
        }

    }

    public function create()
    {
        $BandCount = Band::where('user_id',Auth::user()->id)->count();

        if ($BandCount == 0) {

            $users = City::get();
            foreach ($users as $key => $value) {
                $list[$key][$value->id] = $value->city_name; 
            }
            $citylist = $list; 
            $genres = Genre::pluck('genre_name', 'id');
            $bandtypes = BandType::pluck('band_type_name', 'id');
            return view('dashboard.band.bandCreate',[
                    'genres' => $genres,
                    'cityList' => $citylist, 
                    'bandtypes' => $bandtypes,
                    'User' => $this->getUser()
            ]);

        } else {
            return redirect('dashboard/band');
        }
    	


    }

    public function store(Request $request)
    {
    	$validator = $this->validate($request,[
            'band_name' => 'required|max:64',
            'description' => 'required',
            'band_type' => 'required',
            'genre' => 'required',
            'profile_img' => 'required|mimes:jpeg,png',
            'city' => 'required',
            'url_band_name' => 'required|max:64|unique:bands',
            'website_url' => 'unique:bands|max:64',
            'facebook_url' => 'unique:bands|max:64',
            'twitter_url' => 'unique:bands|max:64',
            'instagram_url' => 'unique:bands|max:64',
            'soundcloud_url' => 'unique:bands|max:64',
            'youtube_url' => 'unique:bands|max:64',

        ]);

        	if ($request->file('profile_img')->isValid()) {

                $image = Input::file('profile_img');
                $img = Image::make($image);
                $imgSize = $img->filesize();

                if ($imgSize >= 1000000) {
                    Session::flash('error_message', 'Ukuran gambar > 1MB');
                    return redirect()->back()->withInput();
                
                } else {

        			$image = Input::file('profile_img');
			    	$filename = date('Y-m-d')."-".$image->getClientOriginalName();
			    	$destinationPath = 'uploads/bands/';
			 		Input::file('profile_img')->move($destinationPath, $filename);
			 		$bandNameSlug = str_slug($request->input('band_name'), "-");

					$Band = new Band();
					$Band->band_name    				= $request->input('band_name');
					$Band->description    				= $request->input('description');
					$Band->url_band_name    			= $request->input('url_band_name');
					$Band->image_band_profile_path		= $filename;
					$Band->user_id    					= $this->userId();
					$Band->band_type    				= $request->input('band_type');
					$Band->website_url    				= $request->input('website_url');
					$Band->facebook_url    				= $request->input('facebook_url');
					$Band->twitter_url    				= $request->input('twitter_url');
					$Band->instagram_url    			= $request->input('instagram_url');
					$Band->soundcloud_url    			= $request->input('soundcloud_url');
					$Band->youtube_url    				= $request->input('youtube_url');
					$Band->slug    						= $bandNameSlug;
					$Band->save();

					$BandGenre = new BandGenre();
					$BandGenre->band_id				= $Band->id;
					$BandGenre->genre_id			= $request->input('genre');
					$BandGenre->save();

					$BandLocation = new BandLocation();
					$BandLocation->band_id			= $Band->id;
					$BandLocation->city_id			= $request->input('city');
					$BandLocation->save();

                    $User = User::where('id',$this->userId())->first();
                    $User->type          = 'band';
                    $User->save();

			       	Session::flash('flash_message', 'Akun berhasil dibuat');
                	return redirect('dashboard/band');
                    }
        		} else {
        			Session::flash('error_message', 'Gambar tidak valid');
        			return redirect()->back();
        		}
    	
		/*$band->mime = $file->getClientMimeType();
		$band->original_filename = $file->getClientOriginalName();
		$band->filename = $file->getFilename().'.'.$extension;
		$band->save();*/
    }

    public function edit($id)
    {

        $Band = Band::findOrFail($id);
        if (Auth::user()->id == $Band->user_id){
        $Bands = Band::where('id',$id)->with('bandtype')->get();
        $BandGenres = BandGenre::where('band_id',$id)->with('genre')->get();
        $BandLocations = BandLocation::where('band_id',$id)->with('city.province')->get();
        $BandTypes = BandType::all();
        return view('dashboard.band.bandEdit',[
            'BandTypes' => $BandTypes,
            'Bands' => $Bands,
            'BandGenres' => $BandGenres,
            'BandLocations' => $BandLocations,
            'User' => $this->getUser()
            ]);
        } else {
            return redirect('dashboard/band');
        }



    }

    public function update($id,Request $request)
    {

        $Band = Band::findOrFail($id);
        if (Auth::user()->id == $Band->user_id){
            $validator = $this->validate($request,[
            'band_name' => 'required|max:64',
            'description' => 'required',
            'band_type' => 'required',
            'genre' => 'required',
            'city' => 'required',
        ]);

        if ($Band->url_band_name == $request->input('url_band_name')) {
            $validator = $this->validate($request,[
                'url_band_name' => 'required|max:64',
            ]);
        } else {
            $validator = $this->validate($request,[
                'url_band_name' => 'required|max:64|unique:bands',
            ]);
        }

        if ($Band->website_url == $request->input('website_url')) {
            $validator = $this->validate($request,[
                'website_url' => 'max:64',
            ]);
        } else {
            $validator = $this->validate($request,[
                'website_url' => 'max:64|unique:bands',
            ]);
        }

        if ($Band->facebook_url == $request->input('facebook_url')) {
            $validator = $this->validate($request,[
                'facebook_url' => 'max:64',
            ]);
        } else {
            $validator = $this->validate($request,[
                'facebook_url' => 'max:64|unique:bands',
            ]);
        }

        if ($Band->twitter_url == $request->input('twitter_url')) {
            $validator = $this->validate($request,[
                'twitter_url' => 'max:64',
            ]);
        } else {
            $validator = $this->validate($request,[
                'twitter_url' => 'max:64|unique:bands',
            ]);
        }
        
        if ($Band->instagram_url == $request->input('instagram_url')) {
            $validator = $this->validate($request,[
                'instagram_url' => 'max:64',
            ]);
        } else {
            $validator = $this->validate($request,[
                'instagram_url' => 'max:64|unique:bands',
            ]);
        }

        if ($Band->soundcloud_url == $request->input('soundcloud_url')) {
            $validator = $this->validate($request,[
                'soundcloud_url' => 'max:64',
            ]);
        } else {
            $validator = $this->validate($request,[
                'soundcloud_url' => 'max:64|unique:bands',
            ]);
        }

        if ($Band->youtube_url == $request->input('youtube_url')) {
            $validator = $this->validate($request,[
                'youtube_url' => 'max:64',
            ]);
        } else {
            $validator = $this->validate($request,[
                'youtube_url' => 'max:64|unique:bands',
            ]);
        }

        $imagepathDB = $Band->image_band_profile_path;

        if ($request->hasFile('profile_img')) {

            if ($request->file('profile_img')->isValid()) {

                $image = Input::file('profile_img');
                $filename = date('Y-m-d')."-".$image->getClientOriginalName();
                $destinationPath = 'uploads/bands/';
                Input::file('profile_img')->move($destinationPath, $filename);
                $bandNameSlug = str_slug($request->input('band_name'), "-");

                $Band = Band::findOrFail($id);
                $Band->band_name                    = $request->input('band_name');
                $Band->description                  = $request->input('description');
                $Band->url_band_name                = $request->input('url_band_name');
                $Band->user_id                      = $this->userId();
                $Band->band_type                    = $request->input('band_type');
                $Band->website_url                  = $request->input('website_url');
                $Band->facebook_url                 = $request->input('facebook_url');
                $Band->twitter_url                  = $request->input('twitter_url');
                $Band->instagram_url                = $request->input('instagram_url');
                $Band->soundcloud_url               = $request->input('soundcloud_url');
                $Band->youtube_url                  = $request->input('youtube_url');   
                $Band->slug                         = $bandNameSlug;
                $Band->image_band_profile_path      = $filename;
                $Band->save();

                $BandGenre = BandGenre::where('band_id',$id)->first();
                $BandGenre->genre_id            = $request->input('genre');
                $BandGenre->save();

                $BandLocation = BandLocation::where('band_id',$id)->first();
                $BandLocation->city_id          = $request->input('city');
                $BandLocation->save();

                // sending back with success message.
                Session::flash('flash_message', 'Akun berhasil diubah');
                return redirect()->back();

            }

            else {
                // sending back with error message.
                Session::flash('error', 'uploaded file is not valid');
                return Redirect::to('upload');
            }

        }

        else {

                $bandNameSlug = str_slug($request->input('band_name'), "-");
                $Band = Band::findOrFail($id);
                $Band->band_name                = $request->input('band_name');
                $Band->description              = $request->input('description');
                $Band->url_band_name            = $request->input('url_band_name');
                $Band->user_id                  = $this->userId();
                $Band->band_type                = $request->input('band_type');
                $Band->website_url              = $request->input('website_url');
                $Band->facebook_url             = $request->input('facebook_url');
                $Band->twitter_url              = $request->input('twitter_url');
                $Band->instagram_url            = $request->input('instagram_url');
                $Band->soundcloud_url           = $request->input('soundcloud_url');
                $Band->youtube_url              = $request->input('youtube_url');
                $Band->slug                     = $bandNameSlug;
                $Band->save();

                $BandGenre = BandGenre::where('band_id',$id)->first();
                $BandGenre->genre_id            = $request->input('genre');
                $BandGenre->save();

                $BandLocation = BandLocation::where('band_id',$id)->first();
                $BandLocation->city_id          = $request->input('city');
                $BandLocation->save();

                // sending back with success message.
                Session::flash('flash_message', 'Akun berhasil diubah');
                return redirect()->back();
        }

        } else {
            return redirect('dashboard/band');
        }

		/*$band->mime = $file->getClientMimeType();
		$band->original_filename = $file->getClientOriginalName();
		$band->filename = $file->getFilename().'.'.$extension;
		$band->save();*/
    }

    public function destroy($id){


        $Band = Band::findOrFail($id);
        if (Auth::user()->id == $Band->user_id){

        $filename = 'uploads/'.$Band->image_band_profile_path;
        if (File::exists($filename)){
            File::delete($filename);
        }
        $Band->delete();

        $BandGenre = BandGenre::where('band_id',$id)->first();
        $BandGenre->delete();

        $BandLocation = BandLocation::where('band_id',$id)->first();
        $BandLocation->delete();

        Session::flash('flash_message', 'Akun band berhasil dihapus');
        return redirect('dashboard/band');
        } else {
            return redirect('dashboard/band');
        }
        
    }


    /* AJAX REQUEST */

    public function ajaxCities(Request $request)
    {
    	$term = $request->term ?: '';
        $cities = City::with('province')->where('city_name', 'like', '%'.$term.'%')->get();
        $valid_cities = array();
        foreach ($cities as $id => $city) {
        	$valid_cities[] = array('city_id' => $city->id, 'city_name' => $city->city_name.' , '.$city->province->province_name);
        }
        return \Response::json($valid_cities);
    }

    public function ajaxGenres(Request $request)
    {
    	$term = $request->term ?: '';
        $genres = Genre::where('genre_name', 'like', '%'.$term.'%')->lists('genre_name', 'id');
        $valid_genres = array();
        foreach ($genres as $id => $genre) {
            $valid_genres[] = array('genre_id' => $id, 'genre_name' => $genre);
        }
        return \Response::json($valid_genres);
    }

    public function ajaxBandtypes(Request $request)
    {
    	$term = $request->term ?: '';
        $bandtypes = BandType::where('band_type_name', 'like', '%'.$term.'%')->lists('band_type_name', 'id');
        $valid_bandtypes = array();
        foreach ($bandtypes as $id => $bandtype) {
            $valid_bandtypes[] = array('type_id' => $id, 'type_name' => $bandtype);
        }
        return \Response::json($valid_bandtypes);
    }
    public function ajaxUserBand(Request $request)
    {
    	$term = $request->term ?: '';
        $users = User::where('first_name', 'like', '%'.$term.'%')->get();
        $valid_users = [];
        foreach ($users as $id => $user) {
            $valid_users[] = ['id' => $user->id, 'text' => $user->first_name.' '.$user->last_name];
        }
        return \Response::json($valid_users);
    }
}

