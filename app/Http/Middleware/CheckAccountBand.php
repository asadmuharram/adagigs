<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class CheckAccountBand
{
    public function handle($request, Closure $next)
    {
        $user = Auth::user();
    	if($user->type === 'band' || $user->type === 'unknown') {
        	return $next($request);
    	}

    	return redirect('/dashboard');
    }
}