<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class CheckAccountOrg
{
    public function handle($request, Closure $next)
    {
        $user = Auth::user();
    	if($user->type === 'organization' || $user->type === 'unknown') {
        	return $next($request);
    	}

    	return redirect('/dashboard');
    }
}