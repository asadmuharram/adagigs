<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/*
TEST EMAIL REGISTRATION
Route::get('/', function () {
    return view('emails.registration',[
            'first_name' =>  'Muharram',
            'username' =>  'muaramasad',
            'verCode' =>  '$2y$10$bs2zUbGE9F5O97UJp84gSejfuHyIFmqsAI.opk7F5XyCuF4YwpBCy',
            ]);
});*/

Route::group(['middlewareGroups' => ['web']], function () {

	/* Auth Routes */
	Route::auth();
	Route::post('/register', ['as' => 'register', 'uses' => 'Auth\AuthController@postRegister']);
	Route::post('/login', ['as' => 'login', 'uses' => 'Auth\AuthController@postLogin']);
	Route::post('/register/success', 'Auth\AuthController@registerSuccess');
	Route::get('/activation/{username}/{token}', 'Auth\AuthController@getConfirm');

	/* Auth SocialLite */
	Route::post('/connect/redirect/{provider}', 'Auth\AuthController@redirectToProvider');
	Route::get('/connect/{provider}', 'Auth\AuthController@handleProviderCallback');

	/* Dashboard Routes */
	Route::get('/dashboard', 'Dashboard\DashboardController@index');
	Route::get('/dashboard/welcome', 'Dashboard\DashboardController@welcome');
	Route::get('/dashboard/settings', 'Dashboard\DashboardController@settings');
	Route::put('/dashboard/settings/password', 'Dashboard\DashboardController@settingsPassword');
	Route::put('/dashboard/settings/account', 'Dashboard\DashboardController@settingsAccount');

	/* Dashboard Band Routes */
	Route::get('/dashboard/band/', 'Dashboard\BandController@index');
	Route::get('/dashboard/band/create', 'Dashboard\BandController@create');
	Route::post('/dashboard/band/store', 'Dashboard\BandController@store');
	Route::get('/dashboard/band/{id}-{slug}', 'Dashboard\BandController@view');
	Route::get('/dashboard/band/{id}-{slug}/edit', 'Dashboard\BandController@edit');
	Route::put('/dashboard/band/{id}-{slug}/update', 'Dashboard\BandController@update');
	Route::delete('/dashboard/band/{id}-{slug}/delete', 'Dashboard\BandController@destroy');

	/* Dashboard Organization Routes */
	Route::get('/dashboard/organization/', 'Dashboard\OrganizationController@index');
	Route::get('/dashboard/organization/create', 'Dashboard\OrganizationController@create');
	Route::post('/dashboard/organization/store', 'Dashboard\OrganizationController@store');
	Route::get('/dashboard/organization/{id}-{slug}', 'Dashboard\OrganizationController@view');
	Route::get('/dashboard/organization/{id}-{slug}/edit', 'Dashboard\OrganizationController@edit');
	Route::put('/dashboard/organization/{id}-{slug}/update', 'Dashboard\OrganizationController@update');
	Route::delete('/dashboard/organization/{id}-{slug}/delete', 'Dashboard\OrganizationController@destroy');

	/* Dashboard Gig Routes */
	Route::get('/dashboard/gig/', 'Dashboard\GigController@index');
	Route::get('/dashboard/gig/create', 'Dashboard\GigController@create');
	Route::post('/dashboard/gig/create', 'Dashboard\GigController@store');
	Route::get('/dashboard/gig/{id}-{slug}', 'Dashboard\GigController@view');
	Route::post('/dashboard/gig/{id}-{slug}/status', 'Dashboard\GigController@changeStatus');
	Route::post('/dashboard/gig/{id}/band/{bandid}/status', 'Dashboard\GigController@changeBandStatus');
	Route::get('/dashboard/gig/{id}/band', 'Dashboard\GigController@gigBand');
	Route::post('/dashboard/gig/{id}/band', 'Dashboard\GigController@gigBandAdd');
	Route::get('/dashboard/gig/{id}-{slug}/edit', 'Dashboard\GigController@edit');
	Route::put('/dashboard/gig/{id}-{slug}/update', 'Dashboard\GigController@update');
	Route::delete('/dashboard/gig/{id}-{slug}/delete', 'Dashboard\GigController@destroy');

	/* Notification Ajax */
	Route::get('/notification/read/{id}', 'Dashboard\DashboardController@readNotif');

	/* Dashboard Ajax */
	Route::get('/dashboard/getcities', 'Dashboard\BandController@ajaxCities');
	Route::get('/dashboard/getgenres', 'Dashboard\BandController@ajaxGenres');
	Route::get('/dashboard/getbandtypes', 'Dashboard\BandController@ajaxBandtypes');
	Route::get('/dashboard/getusers', 'Dashboard\BandController@ajaxUserBand');
	Route::get('/dashboard/getbands', 'Dashboard\GigController@ajaxBands');

	/* Front Ajax */
	Route::get('/getcities', 'FrontpageController@ajaxCitiesFront');

	/* =================== Frontpage Routes ========================= */

	/* Homepage Routes */
	Route::get('/', 'FrontpageController@index');

	/* Homepage Privasi Routes */
	Route::get('/privacy', 'FrontpageController@privacy');
	Route::get('/termsofservice', 'FrontpageController@terms');

	/* Gig Routes */
	Route::get('/gigs', 'FrontpageController@gigs');
	Route::get('/bands', 'FrontpageController@bands');
	Route::get('/gigs/search', 'FrontpageController@gigSearch');
	Route::get('/bands/search', 'FrontpageController@bandSearch');
	Route::get('/{url_organization_name}/gig/{gig_slug}', ['middleware' => 'auth','uses' => 'FrontpageController@GigPage']);
	Route::post('/{url_organization_name}/gig/{gig_slug}/apply', 'FrontpageController@GigBandApply');
	Route::get('/{url_organization_name}/gig/{gig_slug}/band', ['middleware' => 'auth','uses' => 'FrontpageController@GigBandPage']);

	/* Band Page Routes */
	Route::get('/band/{url_band_name}', 'FrontpageController@bandPage');
	Route::get('/band/{url_band_name}/gig', 'FrontpageController@bandPageGigs');

	/* Organization Page Routes */
	Route::get('/organization/{url_organization_name}', 'FrontpageController@organizationPage');
	Route::get('/organization/{url_organization_name}/gig', 'FrontpageController@organizationPageGigs');

});

