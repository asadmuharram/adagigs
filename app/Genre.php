<?php

namespace App;

use Sofa\Eloquence\Eloquence;
use Illuminate\Database\Eloquent\Model;

class Genre extends Model
{
    use Eloquence;

	protected $fillable = [
        'genre_name'
    ];

    protected $searchableColumns = ['genre_name'];

    public function bandgenre()
    {
        return $this->hasMany('App\BandGenre');
    }

    public function giggenre()
    {
        return $this->hasMany('App\GigGenre');
    }

}