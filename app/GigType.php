<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GigType extends Model
{
    protected $table = 'gigtype';

    public function gig() {
        return $this->hasMany('App\Gig');
    }
}
