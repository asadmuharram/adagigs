<?php

namespace App;

use Sofa\Eloquence\Eloquence;
use Illuminate\Database\Eloquent\Model;

class BandLocation extends Model
{
    use Eloquence;

    protected $table = 'bandlocation';

    protected $fillable = [
        'band_id', 'city_id'
    ];

    protected $searchableColumns = ['city_id'];

    public function band() {
        return $this->belongsTo('App\Band');
    }

    public function city() {
        return $this->belongsTo('App\City','city_id');
    }
}
