<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SocmedList extends Model
{
    protected $table = 'socmedlists';

    public function socmedaccountband()
    {
        return $this->hasMany('App\SocmedAccountBand');
    }
}
