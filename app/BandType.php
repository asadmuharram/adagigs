<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BandType extends Model
{
	protected $table = 'bandtype';

    public function band() {
        return $this->hasMany('App\Band');
    }
}
