<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use Auth;
use App\User;
use App\Band;
use App\Organization;
use App\Notification;

class NavMenuServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('layouts.dashboard.auth', function($view)
        {
            if (Auth::check()) {
            $userId = Auth::user()->id;
            $User = User::where('id',$userId)->first();

            if($User->type == 'organization') {

                $Organization = Organization::where('user_id',$userId)->first();
                $notificationCount = Notification::where([
                        ['object_type','App\Organization'],
                        ['object_id',$Organization->id],
                ])->unread()->count();

            } elseif($User->type == 'band') {

                    $Band = Band::where('user_id',$userId)->first();
                    $notificationCount = Notification::where([
                            ['object_type','App\Band'],
                            ['object_id',$Band->id],
                    ])->unread()->count();
            } else {
                $notificationCount = null;
            }

            } else{
                $notificationCount = null;
            }
            $view->with('notificationCount', $notificationCount);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
