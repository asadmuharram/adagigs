<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'email', 'username', 'password', 'birth_date','verification_code','status','via','type','facebook_id' , 'register_ip'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function bands()
    {
        return $this->belongsToMany('App\Band');
    }

    public function gig()
    {
        return $this->hasMany('App\Gig');
    }

    public function loginattempt()
    {
        return $this->hasMany('App\LoginAttempts');
    }

    public function notifications()
    {
        return $this->hasMany('App\Notification');
    }

    public function newNotification()
    {
        $notification = new Notification;
        $notification->user()->associate($this);

        return $notification;
    }
}
