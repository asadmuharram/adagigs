<?php

namespace App;

use Sofa\Eloquence\Eloquence;
use Illuminate\Database\Eloquent\Model;

class City extends Model
{
     use Eloquence;

    public function province() {
        return $this->belongsTo('App\Province','province_id');
    }

    protected $searchableColumns = ['city_name'];

    public function bandlocation()
    {
        return $this->hasMany('App\BandLocation');
    }

    public function giglocation()
    {
        return $this->hasMany('App\GigLocation');
    }

    public function gig()
    {
        return $this->hasMany('App\Gig');
    }

    public function organizationaddress()
    {
        return $this->hasMany('App\OrganizationAddress');
    }
}


