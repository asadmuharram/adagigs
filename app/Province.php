<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Province extends Model
{
    public function cities()
    {
        return $this->hasManyThrough('App\City', 'App\BandLocation', 'province_id', 'city_id');;
    }
}
