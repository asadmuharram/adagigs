<?php

namespace App\Filters;

use Intervention\Image\Filters\FilterInterface;

class AdagigsFilterGig implements FilterInterface
{
	public function applyFilter(\Intervention\Image\Image $image)
    {
        return $image->fit(720, null, null, 'top');
    }
}