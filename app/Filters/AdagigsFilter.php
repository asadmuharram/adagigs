<?php

namespace App\Filters;

use Intervention\Image\Filters\FilterInterface;

class AdagigsFilter implements FilterInterface
{
	public function applyFilter(\Intervention\Image\Image $image)
    {
        return $image->fit(120, 160);
    }
}