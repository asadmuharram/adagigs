<?php

namespace App;

use Sofa\Eloquence\Eloquence;
use Illuminate\Database\Eloquent\Model;

class GigLocation extends Model
{
    use Eloquence;

    protected $table = 'giglocation';

    protected $fillable = [
        'gig_id', 'city_id' , 'venue_name'
    ];

    protected $searchableColumns = ['city_id'];

    public function gig() {
        return $this->belongsTo('App\Gig','gig_id');
    }

    public function city() {
        return $this->belongsTo('App\City','city_id');
    }
}
