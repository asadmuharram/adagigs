<?php

namespace App;

use Sofa\Eloquence\Eloquence;
use Illuminate\Database\Eloquent\Model;

class Band extends Model
{
    use Eloquence;

	protected $fillable = [
        'band_name', 'description', 'url_band_name', 'image_band_profile_path', 'user_id', 'band_type','website_url','slug'
    ];

    protected $searchableColumns = ['band_name'];

    public function users()
    {
        return $this->belongsTo('App\User','user_id');
    }

    public function bandtype()
    {
        return $this->belongsTo('App\BandType' , 'band_type');
    }

    public function socmedaccountband()
    {
        return $this->hasMany('App\SocmedAccountBand');
    }

    public function gigband()
    {
        return $this->hasMany('App\GigBand');
    }

    public function bandlocation()
    {
        return $this->hasMany('App\BandLocation');
    }

    public function bandgenre()
    {
        return $this->hasMany('App\BandGenre');
    }

}
