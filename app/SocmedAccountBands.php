<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SocmedAccountBands extends Model
{
    protected $table = 'socmedaccountbands';

    public function band()
    {
        return $this->belongsTo('App\Band' , 'band_id');
    }

    public function socmedlist()
    {
        return $this->belongsTo('App\SocmedList' , 'socmed_id');
    }
}
