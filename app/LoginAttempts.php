<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LoginAttempts extends Model
{
    protected $table = 'loginattempts';

    protected $fillable = [
        'user_id', 'login_ip', 'login_time' , 'login_status'
    ];

    public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }
}
