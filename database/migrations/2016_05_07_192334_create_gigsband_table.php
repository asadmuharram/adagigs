<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateGigsbandTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('gigsband', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('gig_id');
			$table->string('band_id');
			$table->string('status');
			$table->integer('counter');
			$table->string('request_by');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('gigsband');
	}

}
