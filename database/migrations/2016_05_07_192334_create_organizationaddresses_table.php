<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOrganizationaddressesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('organizationaddresses', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('organization_id');
			$table->string('city_id');
			$table->text('address', 65535);
			$table->string('postcode');
			$table->string('phonenumber');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('organizationaddresses');
	}

}
