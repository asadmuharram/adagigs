<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOrganizationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('organizations', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('organization_name');
			$table->string('organization_type_id');
			$table->string('url_organization_name')->unique();
			$table->string('organization_website');
			$table->string('user_id');
			$table->string('image_file_path');
			$table->timestamps();
			$table->string('twitter_url')->nullable();
			$table->string('facebook_url')->nullable();
			$table->string('instagram_url')->nullable();
			$table->string('slug')->unique();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('organizations');
	}

}
