<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateGigsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('gigs', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('gig_name');
			$table->string('gigtype_id');
			$table->text('gig_description', 65535);
			$table->text('gig_band_requirement', 65535);
			$table->datetime('gig_band_opening_date');
			$table->datetime('gig_band_closing_date');
			$table->date('gig_date');
			$table->integer('gig_total_band');
			$table->string('user_id');
			$table->string('giglocation_id');
			$table->string('organization_id');
			$table->timestamps();
			$table->string('status');
			$table->string('slug')->unique();
			$table->string('gig_image_file_path');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('gigs');
	}

}
