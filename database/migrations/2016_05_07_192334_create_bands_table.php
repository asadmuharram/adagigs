<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBandsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('bands', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('band_name');
			$table->text('description', 65535);
			$table->string('url_band_name')->unique();
			$table->string('band_type');
			$table->string('image_band_profile_path');
			$table->string('user_id');
			$table->timestamps();
			$table->string('website_url');
			$table->string('slug')->unique();
			$table->string('facebook_url')->nullable();
			$table->string('twitter_url')->nullable();
			$table->string('instagram_url')->nullable();
			$table->string('soundcloud_url')->nullable();
			$table->string('youtube_url')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('bands');
	}

}
