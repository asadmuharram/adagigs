<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterLoginattemptsTableAddStatusLogin extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('loginattempts', function(Blueprint $table)
        {
            $table->string('login_status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('loginattempts', function(Blueprint $table){
            $table->dropColumn('login_status');
        });
    }
}
