@extends('layouts.dashboard.auth')
@section('title', 'Dashboard')
@section('content')
@include('common.errors')
@include('common.notifications')
<div class="container">
    @include('layouts.dashboard.sidepanel')
    <div class="col-md-9">
        <div class="row">
            <div class="col-md-12">
                <div class="panelGigs bgCard">
                    <div class="panelTitle">
                        <h4 class="mainTitle">PENGATURAN</h4>
                        <h5 class="subTitle"></h5>
                    </div>
                    <div class="panelBody">
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation"
                                @if($tabName == 'emailSet')
                                class="active"
                                @else
                                class=""
                                @endif
                                ><a href="#emailSet" aria-controls="emailSet" role="tab" data-toggle="tab">Akun</a></li>
                                <li role="presentation"
                                    @if($tabName == 'passRes')
                                    class="active"
                                    @else
                                    class=""
                                    @endif
                                    ><a href="#passRes" aria-controls="passRes" role="tab" data-toggle="tab">Password</a></li>
                                </ul>
                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane
                                        @if($tabName == 'emailSet')
                                        active
                                        @else
                                        class=""
                                        @endif
                                        " id="emailSet">
                                        <h3>Akun</h3>
                                        <br/>
                                        <form action="{{ url('/dashboard/settings/account') }}" method="post" enctype="multipart/form-data">
                                            {!! csrf_field() !!}
                                            {{ method_field('PUT') }}
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
                                                        <label>Nama Depan</label>
                                                        <input type="text" class="form-control" name="first_name" placeholder="Masukkan nama depan" value="{{$first_name}}"/>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                                                        <label>Nama Belakang</label>
                                                        <input type="text" class="form-control" name="last_name" placeholder="Masukkan nama belakang" value="{{$last_name}}"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                                                        <label>Username</label>
                                                        <input type="text" class="form-control" name="username" placeholder="Masukkan username" value="{{$username}}"/>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group{{ $errors->has('password_check') ? ' has-error' : '' }}">
                                                        <label>Password</label>
                                                        <input type="password" class="form-control" name="password_check" placeholder="Masukkan password untuk merubah akun anda" value=""/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="bottomBtn">
                                                <div class="row">
                                                    <div class="col-md-3">
                                                        <button class="btn btn-md btn-block btn-orange-round" type="submit"><i class="fa fa-pencil" aria-hidden="true"></i> Simpan</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <div role="tabpanel" class="tab-pane
                                        @if($tabName == 'passRes')
                                        active
                                        @else
                                        class=""
                                        @endif
                                        " id="passRes">
                                        <h3>Password</h3>
                                        <br/>
                                        <form class="formDashboard" role="form" method="POST" action="{{ url('/dashboard/settings/password') }}" enctype="multipart/form-data">
                                            {!! csrf_field() !!}
                                            {{ method_field('PUT') }}
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group{{ $errors->has('old_password') ? ' has-error' : '' }}">
                                                        <label>Password Saat Ini</label>
                                                        <input type="password" class="form-control" name="old_password" placeholder="Masukkan password saat ini" value=""/>
                                                    </div>
                                                </div>
                                            </div>
                                            <hr/>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                                        <label>Password Baru</label>
                                                        <input type="password" class="form-control" name="password" placeholder="Masukkan password baru" value=""/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                                        <label>Konfirmasi Password</label>
                                                        <input type="password" class="form-control" name="password_confirmation" placeholder="Masukkan password konfirmasi" value=""/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="bottomBtn">
                                                <div class="row">
                                                    <div class="col-md-3">
                                                        <button class="btn btn-md btn-block btn-orange-round" type="submit"><i class="fa fa-pencil" aria-hidden="true"></i> Simpan</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endsection