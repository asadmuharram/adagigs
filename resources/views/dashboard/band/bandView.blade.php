@extends('layouts.dashboard.auth')
@section('title', 'Create Musician / Band')
@section('content')
@include('common.errors')
@include('common.notifications')
<div class="container">
    @include('layouts.dashboard.sidepanel')
    <div class="col-md-9">
        <div class="row">
            <div class="col-md-12">
                <div class="panelGigs bgCard">
                    <div class="panelBody">
                        
                            <div class="row">
                                <div class="media">
                                    <div class="media-left">
                                        <img src="{{ url('/images/profile/'.$Band->image_band_profile_path) }}" alt="" class="img-circle thumbSqure">
                                    </div>
                                    <div class="media-body">
                                        <h4 class="mainTitleBand">{{$Band->band_name}} <span class="label label-default">{{$BandGenre->genre->genre_name}}</span></h4>
                                        <p>{{$Band->bandtype->band_type_name}}</p>
                                    </div>
                                </div>
                            </div>
                        
                        <div class="ProfileBand">
                            <div class="row">
                                <div class="col-md-12">
                                    <h4 class="mainTitle">Biography</h4>
                                    <p class="descBand">{{$Band->description}}</p>
                                </div>
                            </div>
                        </div>
                        <div class="ProfileBand">
                            <div class="row">
                                <div class="col-md-6">
                                    <h4 class="mainTitle">Lokasi</h4>
                                    <p class="descBand">
                                        {{$BandLocation->city->city_name}} , {{$BandLocation->city->province->province_name}}
                                    </p>
                                </div>
                                <div class="col-md-6">
                                    <h4 class="mainTitle">URL</h4>
                                    <p class="descBand"><a href="{{ url('/band/'.$Band->url_band_name)}}" target="blank">adagigs.com/band/{{$Band->url_band_name}}</a></p>
                                </div>
                            </div>
                        </div>
                        <div class="ProfileBand">
                            <div class="row">
                                <div class="col-md-6">
                                    <h4 class="mainTitle">Sosial Media</h4>
                                    <?php
                                    if(!empty($Band->facebook_url)){
                                    ?>
                                    <div class="col-md-2">
                                        <a href="http://facebook.com/{{$Band->facebook_url}}" target="blank"><i class="fa fa-facebook-square fa-2x" aria-hidden="true"></i></a>
                                    </div>
                                    <?php
                                    }
                                    if(!empty($Band->twitter_url)){
                                    ?>
                                    <div class="col-md-2">
                                        <a href="http://twitter.com/{{$Band->twitter_url}}" target="blank"><i class="fa fa-twitter fa-2x" aria-hidden="true"></i></a>
                                    </div>
                                    <?php
                                    }
                                    if(!empty($Band->instagram_url)){
                                    ?>
                                    <div class="col-md-2">
                                        <a href="http://instagram.com/{{$Band->instagram_url}}" target="blank"><i class="fa fa-instagram fa-2x" aria-hidden="true"></i></a>
                                    </div>
                                    <?php
                                    }
                                    if(!empty($Band->soundcloud_url)){
                                    ?>
                                    <div class="col-md-2">
                                        <a href="http://soundcloud.com/{{$Band->soundcloud_url}}" target="blank"><i class="fa fa-soundcloud fa-2x" aria-hidden="true"></i></a>
                                    </div>
                                    <?php
                                    }
                                    if(!empty($Band->youtube_url)){
                                    ?>
                                    <div class="col-md-2">
                                        <a href="http://youtube.com/channel/{{$Band->youtube_url}}" target="blank"><i class="fa fa-youtube-play fa-2x" aria-hidden="true"></i></a>
                                    </div>
                                    <?php
                                    }
                                    ?>
                                </div>
                                <div class="col-md-6">
                                    <h4 class="mainTitle">Website</h4>
                                    <p class="descBand"><a href="http://{{$Band->website_url}}" target="blank">{{$Band->website_url}}</a></p>
                                </div>
                            </div>
                        </div>
                        <div class="bottomBtn">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="col-md-6">
                                        <a class="btn btn-md btn-block btn-orange" href="{{ url('/dashboard/band/'.$Band->id.'-'.$Band->slug.'/edit/') }}"><i class="fa fa-pencil" aria-hidden="true"></i> Ubah Akun</a>
                                    </div>
                                    <div class="col-md-6">
                                        <a class="btn btn-md btn-block btn-gray-round" href="{{ URL::previous() }}">Kembali</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection