@extends('layouts.dashboard.auth')
@section('title', 'Create Musician / Band')
@section('content')
@include('common.errors')
@include('common.notifications')
<div class="container">
    @include('layouts.dashboard.sidepanel')
    <div class="col-md-9">
        <!--<div class="row">
            <div class="col-md-12">
                <div class="panelGigs bgCard">
                    <div class="panelTitle">
                        <h4 class="mainTitle">DAFTAR SEBAGAI ANGGOTA BAND</h4>
                        <h5 class="subTitle">Cari Band tempat kamu bermusik dibawah ini.</h5>
                    </div>
                    <div class="panelBody">
                        <form class="formDashboard" role="form" method="POST" action="{{ url('/dashboard/band/apply') }}">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group{{ $errors->has('band_name_apply') ? ' has-error' : '' }}">
                                        <label>Nama Band</label>
                                        <input type="text" class="form-control" name="band_name_apply" placeholder="Masukkan nama musisi atau band" value="{{ old('band_name_apply') }}"/>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 col-md-offset-3">
                                    <div class="col-md-6">
                                        <button class="btn btn-lg btn-block btn-orange-round" type="submit">Daftar</button>
                                    </div>
                                    <div class="col-md-6">
                                        <a class="btn btn-lg btn-block btn-gray-round" href="{{ url('/dashboard/band') }}">Batal</a>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>-->
        <div class="row">
            <div class="col-md-12">
                <div class="panelGigs bgCard">
                    <div class="panelTitle">
                        <h4 class="mainTitle">BUAT AKUN MUSISI / BAND</h4>
                        <h5 class="subTitle">Kamu dapat mendaftar sebagai musisi solo, grup musik atau band.</h5>
                    </div>
                    <div class="panelBody">
                        
                        <form class="formDashboard" role="form" method="POST" action="{{ url('/dashboard/band/store') }}" enctype="multipart/form-data">
                            {!! csrf_field() !!}
                            <input type="hidden" class="form-control" value="{{ Auth::user()->id }}" name="user_id">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group{{ $errors->has('band_name') ? ' has-error' : '' }}">
                                        <label>Nama Musisi / Band</label>
                                        <input type="text" class="form-control" name="band_name" placeholder="Masukkan nama musisi , grup musik atau band" value="{{ old('band_name') }}"/>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                                        <label>Biography</label>
                                        <textarea class="form-control" rows="3" name="description" placeholder="Jelaskan secara singkat mengenai biography">{{ old('description') }}</textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group{{ $errors->has('band_type') ? ' has-error' : '' }}">
                                        <label>Tipe</label>
                                        {{ Form::select('band_type', $bandtypes, null, ['placeholder' => 'Pilih Tipe','class' => 'form-control selectpicker','data-live-search' => 'true']) }}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group{{ $errors->has('genre') ? ' has-error' : '' }}">
                                        <label>Genre</label>
                                        {{ Form::select('genre', $genres, null, ['placeholder' => 'Pilih Genre','class' => 'form-control selectpicker','data-live-search' => 'true']) }}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group{{ $errors->has('profile_img') ? ' has-error' : '' }}">
                                        <label for="exampleInputFile">Gambar Profil</label>
                                        <input type="file" id="exampleInputFile" name="profile_img" value="{{ old('profile_img') }}">
                                        <p class="help-block">Pastikan format gambar .jpg / .png dengan ukuran max 1MB.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group{{ $errors->has('city') ? ' has-error' : '' }}">
                                        <label>Lokasi</label>
                                        <input id="city_id" type="text" class="form-control" name="city_id" placeholder="Ketik lokasi kota" value="{{ old('city_id') }}"/>
                                        <input id="city" type="hidden" class="form-control" name="city" placeholder="Ketik lokasi kota" value="{{ old('city') }}"/>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="basic-url">Nama URL</label>
                                        <div class="input-group{{ $errors->has('url_band_name') ? ' has-error' : '' }}">
                                            <span class="input-group-addon" id="basic-addon3">www.adagigs.com/band/</span>
                                            <input type="text" class="form-control" id="basic-url" aria-describedby="basic-addon3" name="url_band_name" placeholder="URL" value="{{ old('url_band_name') }}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <label for="basic-url">Akun Social Media</label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="input-group{{ $errors->has('facebook_url') ? ' has-error' : '' }}">
                                                    <span class="input-group-addon" id="basic-addon3"><i class="fa fa-facebook-square" aria-hidden="true"></i> http://facebook.com/</span>
                                                    <input type="text" class="form-control" id="basic-url" aria-describedby="basic-addon3" name="facebook_url" placeholder="URL" value="{{ old('facebook_url') }}">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="input-group{{ $errors->has('twitter_url') ? ' has-error' : '' }}">
                                                    <span class="input-group-addon" id="basic-addon3"><i class="fa fa-twitter" aria-hidden="true"></i></span>
                                                    <input type="text" class="form-control" id="basic-url" aria-describedby="basic-addon3" name="twitter_url" placeholder="Masukkan username twitter" value="{{ old('twitter_url') }}">
                                                </div>
                                            </div>
                                        </div>
                                        <br/>
                                        <div class="row">
                                        <div class="col-md-6">
                                                <div class="input-group{{ $errors->has('soundcloud_url') ? ' has-error' : '' }}">
                                                    <span class="input-group-addon" id="basic-addon3"><i class="fa fa-soundcloud" aria-hidden="true"></i> http://soundcloud.com/</span>
                                                    <input type="text" class="form-control" id="basic-url" aria-describedby="basic-addon3" name="soundcloud_url" placeholder="URL" value="{{ old('soundcloud_url') }}">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="input-group{{ $errors->has('instagram_url') ? ' has-error' : '' }}">
                                                    <span class="input-group-addon" id="basic-addon3"><i class="fa fa-instagram" aria-hidden="true"></i></span>
                                                    <input type="text" class="form-control" id="basic-url" aria-describedby="basic-addon3" name="instagram_url" placeholder="Masukkan username instagram" value="{{ old('instagram_url') }}">
                                                </div>
                                            </div>
                                            
                                        </div>
                                        <br/>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="input-group{{ $errors->has('youtube_url') ? ' has-error' : '' }}">
                                                    <span class="input-group-addon" id="basic-addon3"><i class="fa fa-youtube-play" aria-hidden="true"></i> http://youtube.com/channel/</span>
                                                    <input type="text" class="form-control" id="basic-url" aria-describedby="basic-addon3" name="youtube_url" placeholder="URL" value="{{ old('youtube_url') }}">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="input-group">
                                                    <span class="input-group-addon" id="basic-addon3"><i class="fa fa-link" aria-hidden="true"></i></span>
                                                    <input type="text" class="form-control" id="basic-url" aria-describedby="basic-addon3" name="website_url" placeholder="Masukkan url website" value="{{ old('website_url') }}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="bottomBtn">
                            <div class="row">
                                <div class="col-md-6 col-md-offset-3">
                                    <div class="col-md-6">
                                        <button class="btn btn-md btn-block btn-orange-round" type="submit"><i class="fa fa-floppy-o" aria-hidden="true"></i> Buat Akun</button>
                                    </div>
                                    <div class="col-md-6">
                                        <a class="btn btn-md btn-block btn-gray-round" href="{{ URL::previous() }}">Batal</a>
                                    </div>
                                </div>
                            </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection