@extends('layouts.dashboard.auth')
@section('title', 'Dashboard - Band')
@section('content')
@include('common.errors')
@include('common.notifications')
<div class="container">
    @include('layouts.dashboard.sidepanel')
    <div class="col-md-9">
        <div class="row">
            <div class="col-md-12">
                <div class="panelBand bgCard">
                    <div class="panelTitle">
                        <div class="row nopadding">
                            <div class="col-md-12">
                                <h4 class="mainTitle">BAND</h4>
                                <h5 class="subTitle">Berikut akun band anda.</h5>
                            </div>
                        </div>
                    </div>
                    @if(count($Bands) > 0)
                    <div class="panelBandList">
                        @foreach($Bands as $band)
                        <div class="row">
                        <div class="col-md-12">
                            <div class="media">
                                <div class="media-left">
                                    <img class="thumb img-circle" src="{{ url('/images/profile/'.$band->image_band_profile_path) }}" alt="{{$band->band_name}}" >
                                </div>
                                <div class="media-body">
                                    <h4 class="titleBand">{{$band->band_name}}</h4>
                                </div>
                                <div class="media-right">
                                    <a class="btn btn-sm btn-block btn-orange-round vertical-align" href="{{ url('dashboard/band/'.$band->id.'-'.$band->slug) }}">Lihat</a>
                                </div>
                            </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                    @else
                    <div class="createBand">
                        <h5 class="subTitle">Kamu belum terdaftar sebagai musisi maupun anggota band.</h5>
                        <a href="{{ URL::to('/dashboard/band/create') }}"><button class="btn btn-md btn-block btn-orange-round" type="submit">Daftar Musisi / Band</button></a>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection