@extends('layouts.dashboard.auth')
@section('title', 'Edit Musician / Band')
@section('content')
@include('common.errors')
@include('common.notifications')
<div class="container">
    @include('layouts.dashboard.sidepanel')
    <div class="col-md-9">
        <div class="row">
            <div class="col-md-12">
                @foreach($Bands as $Band)
                <div class="panelGigs bgCard">
                    <div class="panelTitle">
                        <h4 class="mainTitle">UBAH AKUN MUSISI / BAND</h4>
                        <h5 class="subTitle">Anda dapat mengubah informasi mengenai akun band disini.</h5>
                    </div>
                    <div class="panelBody">
                        <form class="formDashboard" role="form" method="POST" action="{{ url('/dashboard/band/'.$Band->id.'-'.$Band->slug.'/update/') }}" enctype="multipart/form-data">
                            {!! csrf_field() !!}
                            {{ method_field('PUT') }}
                            <input type="hidden" class="form-control" value="{{ Auth::user()->id }}" name="user_id">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group{{ $errors->has('band_name') ? ' has-error' : '' }}">
                                        <label>Nama Musisi / Band</label>
                                        <input type="text" class="form-control" name="band_name" placeholder="Masukkan nama musisi , grup musik atau band" value="{{$Band->band_name}}"/>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                                        <label>Biography</label>
                                        <textarea class="form-control" rows="3" name="description" placeholder="Jelaskan secara singkat mengenai biography">{{$Band->description}}</textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group{{ $errors->has('band_type') ? ' has-error' : '' }}">
                                        <label>Tipe</label>
                                        <select class="form-control selectpicker" name="band_type" data-live-search="true">
                                            <option value="">Please Select Category</option>
                                            @foreach ($BandTypes as $BandType)
                                            @if($Band->band_type == $BandType->id)
                                            <option selected="selected" value="{{$BandType->id}}">{{$BandType->band_type_name}}</option>
                                            @else
                                            <option value="{{$BandType->id}}">{{$BandType->band_type_name}}</option>
                                            @endif
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group{{ $errors->has('genre') ? ' has-error' : '' }}">
                                        <label>Genre</label>
                                        @foreach($BandGenres as $BandGenre)
                                        <input id="genre_id" type="text" class="form-control" name="genre_id" placeholder="Ketik Genre Musik" value="{{$BandGenre->genre->genre_name}}"/>
                                        <input id="genre" type="hidden" class="form-control" name="genre" placeholder="Ketik lokasi kota" value="{{$BandGenre->genre_id}}"/>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group{{ $errors->has('profile_img') ? ' has-error' : '' }}">
                                        <label for="exampleInputFile">Gambar Profil</label>
                                        <img src="{{ url('/images/profile/'.$Band->image_band_profile_path) }}" alt="" class="img-responsive" width="20%" height="auto">
                                        <br/>
                                        <input type="file" id="exampleInputFile" name="profile_img" value="{{ old('profile_img') }}">
                                        <p class="help-block">Pastikan format gambar .jpg / .png dengan ukuran max 1MB.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group{{ $errors->has('city') ? ' has-error' : '' }}">
                                        <label>Lokasi</label>
                                        @foreach($BandLocations as $BandLocation)
                                        <input id="city_id" type="text" class="form-control" name="city_id" placeholder="Ketik lokasi kota" value="{{$BandLocation->city->city_name}} , {{$BandLocation->city->province->province_name}}"/>
                                        <input id="city" type="hidden" class="form-control" name="city" placeholder="Ketik lokasi kota" value="{{$BandLocation->city_id}}"/>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="basic-url">Nama URL</label>
                                        <div class="input-group{{ $errors->has('url_band_name') ? ' has-error' : '' }}">
                                            <span class="input-group-addon" id="basic-addon3">www.adagigs.com/pages/</span>
                                            <input type="text" class="form-control" id="basic-url" aria-describedby="basic-addon3" name="url_band_name" placeholder="Masukkan nama url" value="{{$Band->url_band_name}}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <label for="basic-url">Akun Social Media</label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="input-group{{ $errors->has('facebook_url') ? ' has-error' : '' }}">
                                                    <span class="input-group-addon" id="basic-addon3"><i class="fa fa-facebook-square" aria-hidden="true"></i> http://facebook.com/</span>
                                                    <input type="text" class="form-control" id="basic-url" aria-describedby="basic-addon3" name="facebook_url" placeholder="Masukkan url facebook" value="{{$Band->facebook_url}}">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="input-group{{ $errors->has('twitter_url') ? ' has-error' : '' }}">
                                                    <span class="input-group-addon" id="basic-addon3"><i class="fa fa-twitter" aria-hidden="true"></i></span>
                                                    <input type="text" class="form-control" id="basic-url" aria-describedby="basic-addon3" name="twitter_url" placeholder="Masukkan url twitter" value="{{$Band->twitter_url}}">
                                                </div>
                                            </div>
                                        </div>
                                        <br/>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="input-group{{ $errors->has('soundcloud_url') ? ' has-error' : '' }}">
                                                    <span class="input-group-addon" id="basic-addon3"><i class="fa fa-soundcloud" aria-hidden="true"></i> http://soundcloud.com/</span>
                                                    <input type="text" class="form-control" id="basic-url" aria-describedby="basic-addon3" name="soundcloud_url" placeholder="Masukkan url soundcloud" value="{{$Band->soundcloud_url}}">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="input-group{{ $errors->has('instagram_url') ? ' has-error' : '' }}">
                                                    <span class="input-group-addon" id="basic-addon3"><i class="fa fa-instagram" aria-hidden="true"></i></span>
                                                    <input type="text" class="form-control" id="basic-url" aria-describedby="basic-addon3" name="instagram_url" placeholder="Masukkan url instagram" value="{{$Band->instagram_url}}">
                                                </div>
                                            </div>
                                        </div>
                                        <br/>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="input-group{{ $errors->has('youtube_url') ? ' has-error' : '' }}">
                                                    <span class="input-group-addon" id="basic-addon3"><i class="fa fa-youtube-play" aria-hidden="true"></i> http://youtube.com/</span>
                                                    <input type="text" class="form-control" id="basic-url" aria-describedby="basic-addon3" name="youtube_url" placeholder="Masukkan url youtube" value="{{$Band->youtube_url}}">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="input-group">
                                                    <span class="input-group-addon" id="basic-addon3"><i class="fa fa-link" aria-hidden="true"></i></span>
                                                    <input type="text" class="form-control" id="basic-url" aria-describedby="basic-addon3" name="website_url" placeholder="Masukkan url website" value="{{$Band->website_url}}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="bottomBtn">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="col-md-6">
                                            <button class="btn btn-md btn-block btn-orange" type="submit"><i class="fa fa-pencil" aria-hidden="true"></i> Ubah Akun</button>
                                        </div>
                                        <div class="col-md-6">
                                            <a class="btn btn-md btn-block btn-gray-round" href="{{ URL::previous() }}">Kembali</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
@endsection