@extends('layouts.dashboard.auth')
@section('title', 'Add Musician / Band')
@section('content')
@include('common.errors')
@include('common.notifications')
<div class="container">
    @include('layouts.dashboard.sidepanel')
    <div class="col-md-9">
        <div class="row">
            <div class="col-md-12">
                <div class="panelGigs bgCard">
                    <div class="panelTitle">
                        <h4 class="mainTitle">Undang Musisi / Band</h4>
                        <h5 class="subTitle"><b>Perhatian,</b> Sebelum mengundang musisi / band untuk bergabung dalam gig ini. Diharapkan menghubungi terlebih dahulu musisi / band yang bersangkutan karena anda hanya dapat mengundang musisi / band 1 kali dalam gig ini.</h5>
                    </div>
                    <div class="panelBody">
                        <form class="formDashboard" role="form" method="POST" action="{{ url('/dashboard/gig/'.$Gig->id.'/band') }}" enctype="multipart/form-data">
                            {!! csrf_field() !!}
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group{{ $errors->has('band') ? ' has-error' : '' }}">
                                        <label>Nama Band</label>
                                        <input id="band_id" type="text" class="form-control" name="band_id" placeholder="Ketik Nama Band" value="{{ old('band_id') }}"/>
                                        <input id="band" type="hidden" class="form-control" name="band" value="{{ old('band') }}"/>
                                    </div>
                                </div>
                            </div>
                            <div class="bottomBtn">
                            <div class="row">
                                <div class="col-md-8 col-md-offset-2">
                                    <div class="col-md-6">
                                        <button class="btn btn-md btn-block btn-orange" type="submit"><i class="fa fa-paper-plane" aria-hidden="true"></i> Undang Musisi / Band</button>
                                    </div>
                                    <div class="col-md-6">
                                        <a class="btn btn-md btn-block btn-gray-round" href="{{ url('dashboard/gig/'.$Gig->id.'-'.$Gig->slug) }}">Batal</a>
                                    </div>
                                </div>
                            </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection