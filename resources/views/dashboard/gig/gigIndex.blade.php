@extends('layouts.dashboard.auth')
@section('title', 'Dashboard - Gig')
@section('content')
@include('common.errors')
@include('common.notifications')
<div class="container">
    @include('layouts.dashboard.sidepanel')
    <div class="col-md-9">
        @if($User->type == 'organization')
        <div class="row">
            <div class="col-md-12">
                <div class="panelBand bgCard">
                    <div class="panelTitle">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="col-md-10">
                                    <h4 class="mainTitle">GIG</h4>
                                    <h5 class="subTitle">Berikut daftar gig yang anda buat.</h5>
                                </div>
                                <div class="col-md-2">
                                    <a class="btn btn-md btn-block btn-orange btn-top" href="{{ url('dashboard/gig/create') }}"><i class="fa fa-plus" aria-hidden="true"></i> Buat Gig</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    @if(count($Gigs) > 0)
                    <div class="panelBandList">
                        @foreach($Gigs as $Gig)
                        <div class="row">
                            <div class="col-md-12">
                                <div class="media">
                                    <div class="media-left">
                                        <img class="thumb img-circle" src="{{ asset('/') }}./uploads/gigs/{{$Gig->gig_image_file_path}}" alt="{{$Gig->gig_name}}" >
                                    </div>
                                    <div class="media-body">
                                        <h4 class="titleBand">{{$Gig->gig_name}}</h4>
                                    </div>
                                    <div class="media-right">
                                        <a class="btn btn-sm btn-block btn-orange-round btn-top" href="{{ url('dashboard/gig/'.$Gig->id.'-'.$Gig->slug) }}">Lihat</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                        @else
                        <div class="createBand">
                            <h5 class="subTitle">Kamu belum membuat gig</h5>
                            <a href="{{ URL::to('/dashboard/gig/create') }}"><button class="btn btn-md btn-block btn-orange-round " type="submit">Buat Gig</button></a>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        @else
        <div class="row">
            <div class="col-md-12">
                <div class="panelBand bgCard">
                    <div class="panelTitle">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="col-md-12">
                                    <h4 class="mainTitle">GIG</h4>
                                    <h5 class="subTitle">Berikut daftar gig yang anda ikuti.</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    @if(count($GigBands) > 0)
                    <div class="panelBandList">
                        @foreach($GigBands as $GigBand)
                        <div class="row">
                        <div class="col-md-12">
                            <div class="media">
                                <div class="media-left">
                                    <img class="thumb img-circle" src="{{ url('/images/profile/'.$GigBand->gig->gig_image_file_path) }}" alt="{{$GigBand->gig->gig_name}}" >
                                </div>
                                <div class="media-body">
                                    <a href="{{ url('/'.$GigBand->gig->organization->url_organization_name.'/gig/'.$GigBand->gig->slug) }}"><h4 class="titleBand">{{$GigBand->gig->gig_name}}</h4></a>
                                </div>
                                <div class="media-right">
                                    @if($GigBand->request_by == 'band')
                                    @if($GigBand->status == 'accept')
                                    <button id="applyGig" type="button" class="btn btn-md btn-block btn-green vertical-align" disabled="disabled"><i class="fa fa-check" aria-hidden="true"></i> Diterima oleh Penyelenggara</button>
                                    @elseif($GigBand->status == 'pending')
                                    <button id="applyGig" type="button" class="btn btn-md btn-block btn-yellow vertical-align"  disabled="disabled"><i class="fa fa-hourglass-start" aria-hidden="true"></i> Menunggu Konfirmasi Penyelenggara</button>
                                    @else
                                    <button class="btn btn-md btn-red vertical-align" type="button"  disabled="disabled">
                                    <i class="fa fa-times" aria-hidden="true"></i> Ditolak oleh Penyelenggara
                                    @endif
                                    @else
                                    <form action="{{ url('/dashboard/gig/'.$GigBand->gig->id.'/band/'.$GigBand->band_id.'/status') }}" method="POST">
                                        {!! csrf_field() !!}
                                        <div class="dropdown dropdown-submit-input">
                                            <input type="hidden" name="apply_status" />
                                            @if($GigBand->status == 'accept')
                                            <button class="btn btn-md btn-green dropdown-toggle vertical-align" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-check" aria-hidden="true"></i>
                                            Diterima
                                            @elseif($GigBand->status == 'pending')
                                            <button class="btn btn-md btn-yellow dropdown-toggle vertical-align" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fa fa-hourglass-start" aria-hidden="true"></i> Pending
                                            @else
                                            <button class="btn btn-md btn-red dropdown-toggle vertical-align" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Ditolak
                                            @endif
                                            <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                                <li><a href="#" data-value="accept">Terima</a></li>
                                                <li><a href="#" data-value="reject">Tolak</a></li>
                                            </ul>
                                        </div>
                                    </form>
                                    @endif
                                </div>
                            </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                    @else
                    <div class="createBand">
                        <h5 class="subTitle">Belum ada gig yang kamu ikuti.</h5>
                        <a href="{{ URL::to('/gigs') }}"><button class="btn btn-md btn-block btn-orange-round " type="submit">Cari Gig</button></a>
                    </div>
                    @endif
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>
@endsection