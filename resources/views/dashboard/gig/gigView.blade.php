@extends('layouts.dashboard.auth')
@section('title', $Gig->gig_name )
@section('content')
@include('common.errors')
@include('common.notifications')
<div class="container">
    @include('layouts.dashboard.sidepanel')
    <div class="col-xs-12 col-md-9 col-sm-height nopadding">
        <div class="row-height">
            <div class="col-xs-12 col-md-12 col-sm-height">
                <div class="panelGigs bgCard">
                    <div class="panelBody">
                        <div class="row">
                            <div class="media">
                                <div class="media-left">
                                    <img class="thumbBig img-rounded" src="{{ url('/images/profile/'.$Gig->gig_image_file_path) }}" alt="{{$Gig->gig_name}}" >
                                </div>
                                <div class="media-body">
                                    <div class="panelTitleGig">
                                    <h4 class="mainTitleGig"><a class="collapsed" data-toggle="collapse" href="#panel-collapse-gig" aria-expanded="false" aria-controls="collapseExample">{{$Gig->gig_name}}</a></h4>
                                    </div>
                                    <p class="subTitleGig"><i class="fa fa-calendar" aria-hidden="true"></i> Waktu Registrasi :
                                        @if($Gig->gig_band_closing_date <= $nowTime)
                                        <span class="label label-danger">Telah Selesai</span>
                                        @else
                                        <span class="label label-success">{{$closingDate->diffForHumans()}}</span>
                                        @endif
                                    </p>
                                    <p class="subTitleGig"><i class="fa fa-calendar-check-o" aria-hidden="true"></i> Tanggal Acara : {{$gigDate->toFormattedDateString()}}</p>
                                    <p class="subTitleGig"><i class="fa fa-users" aria-hidden="true"></i> Jumlah Musisi / Band : {{$Gig->gig_total_band}}</p>
                                </div>
                                <div class="media-right">
                                    <form action="{{ url('/dashboard/gig/'.$Gig->id.'-'.$Gig->slug.'/status') }}" method="POST">
                                        {!! csrf_field() !!}
                                        <div class="dropdown dropdown-submit-input">
                                            <input type="hidden" name="status" />
                                            @if($closingDate > $nowTime)
                                            @if($Gig->status == 'open')
                                            <button class="btn btn-green btn-md dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            {{$Gig->status}}
                                            @else
                                            <button class="btn btn-red dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            {{$Gig->status}}
                                            @endif
                                            @else
                                            <button class="btn btn-red dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Close
                                            @endif
                                            <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                                <li><a href="#" data-value="open">Open</a></li>
                                                <li><a href="#" data-value="close">Close</a></li>
                                            </ul>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="collapse" id="panel-collapse-gig">
                        <div class="ProfileOrg">
                            <div class="row">
                                <div class="col-md-6">
                                    <h4 class="mainTitle">Tempat</h4>
                                    <p class="descBand">{{$GigLocation->venue_name}} <br/>{{$GigLocation->city->city_name}}, {{$GigLocation->city->province->province_name}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h4 class="mainTitle">Genre</h4>
                                    <p class="descBand">
                                        @foreach($GigGenres as $GigGenre)
                                        {{$GigGenre->genre->genre_name}},
                                        @endforeach
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="ProfileOrg">
                            <div class="row">
                                <div class="col-md-12">
                                    <h4 class="mainTitle">Deskripsi Gig</h4>
                                    <div class="descBand">{!! $Gig->gig_description !!}</div>
                                </div>
                            </div>
                        </div>
                        <div class="ProfileOrg">
                            <div class="row">
                                <div class="col-md-12">
                                    <h4 class="mainTitle">Persyaratan Band</h4>
                                    <div class="descBand">{!! $Gig->gig_band_requirement !!}</div>
                                </div>
                            </div>
                        </div>
                        <div class="bottomBtn">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="col-md-6">
                                        <a href="{{ url('/dashboard/gig/'.$Gig->id.'-'.$Gig->slug.'/edit/') }}"><button class="btn btn-md btn-block btn-orange"><i class="fa fa-pencil" aria-hidden="true"></i> Ubah Gig</button></a>
                                    </div>
                                    <div class="col-md-6">
                                        <a href="{{ url('/dashboard/gig') }}"><button class="btn btn-md btn-block btn-gray-round">Kembali</button></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row-height">
            <div class="col-xs-12 col-md-12 col-sm-height">
                <div class="panelGigs bgCard">
                <div class="row nopadding">
                    <div class="panelTitle">
                        <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-9">
                                <h4 class="mainTitle">MUSISI / BAND</h4>
                                <h5 class="subTitle">Berikut daftar musisi / band yang berpartisipasi di gig ini.</h5>
                            </div>
                            <div class="col-md-3">
                                <a href="{{ url('dashboard/gig/'.$Gig->id.'/band') }}"><button class="btn btn-md btn-block btn-orange btn-top"><i class="fa fa-plus" aria-hidden="true"></i> Undang Musisi / Band</button></a>
                            </div>
                        </div>
                        </div>
                    </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            @if(count($GigBands) > 0)
                            <div class="panelBandList">                                
                                @foreach($GigBands as $GigBand)                               
                                <div class="row">
                                <div class="col-md-12">
                                    <div class="media">
                                        <div class="media-left">
                                            <img class="thumb img-circle" src="{{ url('/images/profile/'.$GigBand->band->image_band_profile_path) }}" alt="{{$GigBand->band->band_name}}" >
                                        </div>
                                        <div class="media-body">
                                            <a href="{{ url('band/'.$GigBand->band->url_band_name) }}"><h4 class="titleBand">{{$GigBand->band->band_name}}</h4></a>
                                        </div>
                                        <div class="media-right">
                                            @if($GigBand->request_by == 'organization')
                                            @if($GigBand->status == 'accept')
                                            <button id="applyGig" type="button" class="btn btn-md btn-block btn-green" disabled="disabled"><i class="fa fa-check" aria-hidden="true"></i> Diterima oleh musisi / band</button>
                                            @elseif($GigBand->status == 'pending')
                                            <button id="applyGig" type="button" class="btn btn-md btn-block btn-yellow"  disabled="disabled"><i class="fa fa-hourglass-start" aria-hidden="true"></i> Menunggu Konfirmasi musisi / band</button>
                                            @else
                                            <button class="btn btn-md btn-red" type="button"  disabled="disabled"><i class="fa fa-times" aria-hidden="true"></i>
                                            Ditolak oleh musisi / band
                                            @endif
                                            @else
                                            <form action="{{ url('/dashboard/gig/'.$Gig->id.'/band/'.$GigBand->band_id.'/status') }}" method="POST">
                                                {!! csrf_field() !!}
                                                <div class="dropdown dropdown-submit-input">
                                                    <input type="hidden" name="apply_status" />
                                                    @if($GigBand->status == 'accept')
                                                    <button class="btn btn-md btn-green dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-check" aria-hidden="true"></i>
                                                    Diterima
                                                    @elseif($GigBand->status == 'pending')
                                                    <button class="btn btn-md btn-yellow dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i class="fa fa-hourglass-start" aria-hidden="true"></i> Pending
                                                    @else
                                                    <button class="btn btn-md btn-red dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-times" aria-hidden="true"></i> 
                                                    Ditolak
                                                    @endif
                                                    <span class="caret"></span>
                                                    </button>
                                                    <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                                        <li><a href="#" data-value="accept">Terima</a></li>
                                                        <li><a href="#" data-value="reject">Tolak</a></li>
                                                    </ul>
                                                </div>
                                            </form>
                                            @endif
                                        </div>
                                    </div>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                            @else
                            <div class="col-md-8 col-md-offset-2">
                                <h5 class="subTitle">Belum ada musisi / band yang berpartisipasi di gig ini.</h5>
                            </div>
                            @endif
                        </div>
                    </div>
                    
                </div>
                
            </div>
        </div>


    </div>
</div>
@endsection