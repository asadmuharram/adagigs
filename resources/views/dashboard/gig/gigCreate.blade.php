@extends('layouts.dashboard.auth')
@section('title', 'Create Gig')
@section('content')
@include('common.errors')
@include('common.notifications')
<div class="container">
    @include('layouts.dashboard.sidepanel')
    <div class="col-md-9">
        <div class="row">
            <div class="col-md-12">
                <div class="panelGigs bgCard">
                    <div class="panelTitle">
                        <h4 class="mainTitle">BUAT GIG</h4>
                        <h5 class="subTitle">Silahkan isi data mengenai gig yang akan anda selenggarakan.</h5>
                    </div>
                    <div class="panelBody">
                        <form class="formDashboard" role="form" method="POST" action="{{ url('/dashboard/gig/create') }}" enctype="multipart/form-data">
                            {!! csrf_field() !!}
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group{{ $errors->has('gig_name') ? ' has-error' : '' }}">
                                        <label>Nama Gig</label>
                                        <input type="text" class="form-control" name="gig_name" placeholder="Masukkan nama gig" value="{{ old('gig_name') }}"/>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group{{ $errors->has('gig_description') ? ' has-error' : '' }}">
                                        <label>Deskripsi Gig</label> <small>( Jelaskan detail mengenai gig yang akan anda selenggarakan. )</small>
                                        <textarea class="form-control textDesc" rows="3" name="gig_description" placeholder="Tuliskan deskripsi singkat gig">{{ old('gig_description') }}</textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group{{ $errors->has('gig_description_requirement') ? ' has-error' : '' }}">
                                        <label>Persyaratan Musisi / Band</label> <small>( Jelaskan mengenai persyaratan bagi musisi / band untuk dapat ikut serta dalam gig ini. )</small>
                                        <textarea class="form-control textDesc" rows="3" name="gig_description_requirement" placeholder="Tuliskan persyaratan musisi / band">{{ old('gig_description_requirement') }}</textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group{{ $errors->has('gig_type') ? ' has-error' : '' }}">
                                        <label>Tipe</label>
                                        {{ Form::select('gig_type', $GigTypes, old('gig_type'), ['placeholder' => 'Pilih Tipe','class' => 'form-control selectpicker','data-live-search' => 'true']) }}
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group{{ $errors->has('genre') ? ' has-error' : '' }}">
                                        <label>Genre</label> <small>(Maks. 3)</small>
                                        {{ Form::select('genre[]', $Genres, null, ['placeholder' => 'Pilih Genre','class' => 'form-control selectpicker','data-live-search' => 'true' , 'multiple']) }}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group{{ $errors->has('gig_img') ? ' has-error' : '' }}">
                                        <label for="exampleInputFile">Gambar Pamflet / Flyer</label>
                                        <input type="file" id="exampleInputFile" name="gig_img" value="{{ old('gig_img') }}">
                                        <p class="help-block">Pastikan format gambar .jpg / .png dengan ukuran max 1MB.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Tgl Buka Registrasi Band</label>
                                    <div class="input-group{{ $errors->has('start_opening_date') ? ' has-error' : '' }}">
                                        <input type="text" class="form-control datePicker" id="openDate" name="start_opening_date" placeholder="Tentukan tanggal awal" value="{{ old('start_opening_date') }}">
                                        <select class="form-control selectpicker" name="time_opening_date">
                                            <option value="">Pilih Jam</option>
                                            @for($i = 1; $i <= 24; $i++)
                                            <option value="{{$i}}">{{$i}}:00</option>
                                            @endfor
                                        </select>
                                        <span class="input-group-addon" id="basic-addon3"><i class="fa fa-clock-o" aria-hidden="true"></i></span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label>Tgl Tutup Registrasi Band</label>
                                    <div class="input-group{{ $errors->has('start_closing_date') ? ' has-error' : '' }}">
                                        <input type="text" class="form-control datePicker" id="closeDate" name="start_closing_date" placeholder="Tentukan tanggal akhir" value="{{ old('start_closing_date') }}">
                                        <select class="form-control selectpicker" name="time_closing_date">
                                            <option value="">Pilih Jam</option>
                                            @for($i = 1; $i <= 24; $i++)
                                            <option value="{{$i}}">{{$i}}:00</option>
                                            @endfor
                                        </select>
                                        <span class="input-group-addon" id="basic-addon3"><i class="fa fa-clock-o" aria-hidden="true"></i></span>
                                    </div>
                                </div>
                            </div>
                            <br/>
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Tanggal Penyelenggaraan</label>
                                    <div class="input-group{{ $errors->has('gig_date') ? ' has-error' : '' }}">
                                        <input type="text" class="form-control datePicker" id="gigDate" name="gig_date" placeholder="Tentukan tanggal penyelenggaraan" value="{{ old('gig_date') }}">
                                        <span class="input-group-addon" id="basic-addon3"><i class="fa fa-calendar-o" aria-hidden="true"></i></span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group{{ $errors->has('total_band') ? ' has-error' : '' }}">
                                        <label>Jumlah Partisipasi Musisi / Band</label>
                                        <select class="form-control selectpicker" name="total_band" data-live-search="true">
                                            <option value="">Pilih Jumlah</option>
                                            @for($i = 1; $i <= 20; $i++)
                                            <option value="{{$i}}">{{$i}}</option>
                                            @endfor
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <br/>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group{{ $errors->has('venue_name') ? ' has-error' : '' }}">
                                        <label>Tempat</label>
                                        <input type="text" class="form-control" name="venue_name" placeholder="Masukkan nama venue, gedung atau alamat" value="{{ old('venue_name') }}"/>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group{{ $errors->has('city') ? ' has-error' : '' }}">
                                        <label>Kota</label>
                                        <input id="city_id" type="text" class="form-control" name="city_id" placeholder="Ketik lokasi kota" value="{{ old('city_id') }}"/>
                                        <input id="city" type="hidden" class="form-control" name="city" placeholder="Ketik lokasi kota" value="{{ old('city') }}"/>
                                    </div>
                                </div>
                            </div>
                            <div class="bottomBtn">
                            <div class="row">
                                <div class="col-md-6 col-md-offset-3">
                                    <div class="col-md-6">
                                        <button class="btn btn-md btn-block btn-orange" type="submit"><i class="fa fa-floppy-o" aria-hidden="true"></i> Buat Gig</button>
                                    </div>
                                    <div class="col-md-6">
                                        <a class="btn btn-md btn-block btn-gray-round" href="{{ url('/dashboard/gig') }}">Batal</a>
                                    </div>
                                </div>
                            </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection