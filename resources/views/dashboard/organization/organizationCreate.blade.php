@extends('layouts.dashboard.auth')
@section('title', 'Create Musician / Band')
@section('content')
@include('common.errors')
@include('common.notifications')
<div class="container">
    @include('layouts.dashboard.sidepanel')
    <div class="col-md-9">
        <div class="row">
            <div class="col-md-12">
                <div class="panelGigs bgCard">
                    <div class="panelTitle">
                        <h4 class="mainTitle">BUAT AKUN ORGANISASI</h4>
                        <h5 class="subTitle">Kamu dapat mendaftar organisasi.</h5>
                    </div>
                    <div class="panelBody">
                        
                        <form class="formDashboard" role="form" method="POST" action="{{ url('/dashboard/organization/store') }}" enctype="multipart/form-data">
                            {!! csrf_field() !!}
                            <input type="hidden" class="form-control" value="{{ Auth::user()->id }}" name="user_id">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group{{ $errors->has('organization_name') ? ' has-error' : '' }}">
                                        <label>Nama Organisasi</label>
                                        <input type="text" class="form-control" name="organization_name" placeholder="Masukkan nama organisasi" value="{{ old('organization_name') }}"/>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group{{ $errors->has('organization_type') ? ' has-error' : '' }}">
                                        <label>Tipe</label>
                                        {{ Form::select('organization_type', $OrganizationTypes, null, ['placeholder' => 'Pilih Tipe','class' => 'form-control selectpicker','data-live-search' => 'true']) }}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                                        <label>Alamat</label>
                                        <textarea class="form-control" rows="3" name="address" placeholder="Masukkan Alamat">{{ old('address') }}</textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group{{ $errors->has('profile_img') ? ' has-error' : '' }}">
                                        <label for="exampleInputFile">Gambar Profil</label>
                                        <input type="file" id="exampleInputFile" name="profile_img" value="{{ old('profile_img') }}">
                                        <p class="help-block">Pastikan format gambar .jpg / .png dengan ukuran max 1MB.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group{{ $errors->has('city') ? ' has-error' : '' }}">
                                        <label>Kota</label>
                                        <input id="city_id" type="text" class="form-control" name="city_id" placeholder="Ketik lokasi kota" value="{{ old('city_id') }}"/>
                                        <input id="city" type="hidden" class="form-control" name="city" placeholder="Ketik lokasi kota" value="{{ old('city') }}"/>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group{{ $errors->has('postcode') ? ' has-error' : '' }}">
                                        <label>Kode Pos</label>
                                        <input type="text" class="form-control" name="postcode" placeholder="Masukkan Kodepos" value="{{ old('postcode') }}"/>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group{{ $errors->has('phonenumber') ? ' has-error' : '' }}">
                                        <label>No. Telepon</label>
                                        <input type="text" class="form-control" name="phonenumber" placeholder="Masukkan no telepon yang valid" value="{{ old('phonenumber') }}"/>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="basic-url">Nama URL</label>
                                        <div class="input-group{{ $errors->has('url_organization_name') ? ' has-error' : '' }}">
                                            <span class="input-group-addon" id="basic-addon3">www.adagigs.com/</span>
                                            <input type="text" class="form-control" id="basic-url" aria-describedby="basic-addon3" name="url_organization_name" placeholder="Masukkan nama url" value="{{ old('url_organization_name') }}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <label for="basic-url">Akun Social Media</label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="input-group{{ $errors->has('facebook_url') ? ' has-error' : '' }}">
                                                    <span class="input-group-addon" id="basic-addon3"><i class="fa fa-facebook-square" aria-hidden="true"></i> http://facebook.com/</span>
                                                    <input type="text" class="form-control" id="basic-url" aria-describedby="basic-addon3" name="facebook_url" placeholder="Nama url" value="{{ old('facebook_url') }}">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="input-group{{ $errors->has('twitter_url') ? ' has-error' : '' }}">
                                                    <span class="input-group-addon" id="basic-addon3"><i class="fa fa-twitter" aria-hidden="true"></i></span>
                                                    <input type="text" class="form-control" id="basic-url" aria-describedby="basic-addon3" name="twitter_url" placeholder="Masukkan username twitter" value="{{ old('twitter_url') }}">
                                                </div>
                                            </div>
                                        </div>
                                        <br/>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="input-group{{ $errors->has('instagram_url') ? ' has-error' : '' }}">
                                                    <span class="input-group-addon" id="basic-addon3"><i class="fa fa-instagram" aria-hidden="true"></i></span>
                                                    <input type="text" class="form-control" id="basic-url" aria-describedby="basic-addon3" name="instagram_url" placeholder="Masukkan username instagram" value="{{ old('instagram_url') }}">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="input-group">
                                                    <span class="input-group-addon" id="basic-addon3"><i class="fa fa-link" aria-hidden="true"></i></span>
                                                    <input type="text" class="form-control" id="basic-url" aria-describedby="basic-addon3" name="website_url" placeholder="Masukkan url website" value="{{ old('website_url') }}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="bottomBtn">
                            <div class="row">
                                <div class="col-md-6 col-md-offset-3">
                                    <div class="col-md-6">
                                        <button class="btn btn-md btn-block btn-orange-round" type="submit"><i class="fa fa-floppy-o" aria-hidden="true"></i> Buat Akun</button>
                                    </div>
                                    <div class="col-md-6">
                                        <a class="btn btn-md btn-block btn-gray-round" href="{{ URL::previous() }}">Batal</a>
                                    </div>
                                </div>
                            </div>
                            </div>
                        </form> 
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection