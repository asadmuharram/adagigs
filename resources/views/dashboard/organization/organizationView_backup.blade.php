@extends('layouts.dashboard.auth')
@section('title', $Organization->organization_name)
@section('content')
@include('common.errors')
@include('common.notifications')
<div class="container">
    @include('layouts.dashboard.sidepanel')
    <div class="col-md-9">
        <div class="row">
            <div class="col-md-12">
                <div class="panelGigs bgCard">
                    <div class="panelBody">
                        <div class="ProfileOrg">
                            <div class="row">
                                <div class="media">
                                <div class="media-left">
                                <img class="img-circle thumbSqure" src="{{ url('/images/profile/'.$Organization->image_file_path) }}" alt="{{$Organization->organization_name}}" >
                                </div>
                                <div class="media-body">
                                <h4 class="mainTitleOrg">{{$Organization->organization_name}} <span class="label label-default">{{$Organization->organizationtype->organization_type_name}}</span></h4>
                                </div>
                                </div>
                            </div>
                        </div>
                        <div class="ProfileOrg">
                            <div class="row">
                                <div class="col-md-6">
                                    <h4 class="mainTitle">Alamat</h4>
                                    <p class="descBand">{{$OrganizationAddress->address}}<br/>{{$OrganizationAddress->city->city_name}} , {{$OrganizationAddress->city->province->province_name}} <br/>{{$OrganizationAddress->postcode}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h4 class="mainTitle">No.Telp</h4>
                                    <p class="descBand">{{$OrganizationAddress->phonenumber}}</p>
                                </div>
                            </div>
                        </div>
                        <div class="ProfileOrg">
                            <div class="row">
                                <div class="col-md-6">
                                    <h4 class="mainTitle">Sosial Media</h4>
                                    <?php
                                    if(!empty($Organization->facebook_url)){
                                    ?>
                                    <div class="col-md-2">
                                        <a href="http://facebook.com/{{$Organization->facebook_url}}" target="blank"><i class="fa fa-facebook-square fa-2x" aria-hidden="true"></i></a>
                                    </div>
                                    <?php
                                    }
                                    if(!empty($Organization->twitter_url)){
                                    ?>
                                    <div class="col-md-2">
                                        <a href="http://twitter.com/{{$Organization->twitter_url}}" target="blank"><i class="fa fa-twitter fa-2x" aria-hidden="true"></i></a>
                                    </div>
                                    <?php
                                    }
                                    if(!empty($Organization->instagram_url)){
                                    ?>
                                    <div class="col-md-2">
                                        <a href="http://instagram.com/{{$Organization->instagram_url}}" target="blank"><i class="fa fa-instagram fa-2x" aria-hidden="true"></i></a>
                                    </div>
                                    <?php
                                    }
                                    ?>
                                </div>
                                <div class="col-md-6">
                                    <h4 class="mainTitle">Website</h4>
                                    <p class="descBand"><a href="http://{{$Organization->organization_website}}" target="blank">{{$Organization->organization_website}}</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="bottomBtn">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="col-md-6">
                                    <a class="btn btn-md btn-block btn-orange-round" href="{{ url('/dashboard/organization/'.$Organization->id.'-'.$Organization->slug.'/edit/') }}"><i class="fa fa-pencil" aria-hidden="true"></i> Ubah Akun</a>
                                </div>
                                <div class="col-md-6">
                                    <a class="btn btn-md btn-block btn-gray-round" href="{{ URL::previous() }}">Kembali</a>
                                </div>
                            </div>
                            <div class="col-md-6 pull-right">
                                <div class="col-md-6 col-md-offset-6">
                                    <button id="delete" type="button" class="btn btn-md btn-block btn-orange-round" data-toggle="modal" data-target=".bs-example-modal-md"><i class="fa fa-trash-o"></i> Hapus Akun</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade bs-example-modal-md" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <form action="{{ url('/dashboard/organization/'.$Organization->id.'-'.$Organization->slug.'/delete/') }}" method="POST" style="display: inline">
                {{ csrf_field() }}
                {{ method_field('DELETE') }}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Kamu yakin untuk menghapus akun ?</h4>
                </div>
                <div class="modal-footer">
                    <div class="col-md-6">
                        <button type="submit" class="btn btn-md btn-block btn-orange-round"> Ya</button>
                        
                    </div>
                    <div class="col-md-6">
                        <button type="button" class="btn btn-md btn-block btn-gray-round" data-dismiss="modal">Tidak</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection