@extends('layouts.dashboard.auth')
@section('title', 'Dashboard - Band')
@section('content')
@include('common.errors')
@include('common.notifications')
<div class="container">
    @include('layouts.dashboard.sidepanel')
    <div class="col-md-9">
        <div class="row">
            <div class="col-md-12">
                <div class="panelBand bgCard">
                    <div class="panelTitle">
                    <div class="row nopadding">
                    <div class="col-md-12">
                        <h4 class="mainTitle">ORGANISASI</h4>
                        <h5 class="subTitle">Anda terdaftar dalam organisasi berikut</h5>
                        </div>
                        </div>
                    </div>
                    @if(count($Organization) > 0)
                    <div class="panelBandList">
                        <div class="row">
                        <div class="col-md-12">
                            <div class="media">
                                <div class="media-left">
                                    <img class="thumb img-circle" src="{{ url('/images/profile/'.$Organization->image_file_path) }}" alt="{{$Organization->organization_name}}" >
                                </div>
                                <div class="media-body">
                                    <h4 class="titleBand">{{$Organization->organization_name}}</h4>
                                </div>
                                <div class="media-right">
                                    <a class="btn btn-sm btn-block btn-orange-round" href="{{ url('dashboard/organization/'.$Organization->id.'-'.$Organization->slug) }}">Lihat</a>
                                </div>
                            </div>
                            </div>
                        </div>
                    </div>
                    @else
                    <div class="createBand">
                        <h5 class="subTitle">Kamu belum mempunyai organisasi</h5>
                        <a href="{{ URL::to('/dashboard/organization/create') }}"><button class="btn btn-md btn-block btn-orange" type="submit">Daftar Organisasi</button></a>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection