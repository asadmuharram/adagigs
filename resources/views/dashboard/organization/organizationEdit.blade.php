@extends('layouts.dashboard.auth')
@section('title', 'Edit Organisasi')
@section('content')
@include('common.errors')
@include('common.notifications')
<div class="container">
    @include('layouts.dashboard.sidepanel')
    <div class="col-md-9">
        <div class="row">
            <div class="col-md-12">
                <div class="panelGigs bgCard">
                    <div class="panelTitle">
                        <h4 class="mainTitle">UBAH AKUN ORGANISASI</h4>
                        <h5 class="subTitle">Anda dapat mengubah informasi mengenai akun organisasi disini.</h5>
                    </div>
                    <div class="panelBody">
                        <form class="formDashboard" role="form" method="POST" action="{{ url('/dashboard/organization/'.$Organization->id.'-'.$Organization->slug.'/update/') }}" enctype="multipart/form-data">
                            {!! csrf_field() !!}
                            {{ method_field('PUT') }}
                            <input type="hidden" class="form-control" value="{{ Auth::user()->id }}" name="user_id">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group{{ $errors->has('organization_name') ? ' has-error' : '' }}">
                                        <label>Nama Musisi / Band</label>
                                        <input type="text" class="form-control" name="organization_name" placeholder="Masukkan nama musisi , grup musik atau band" value="{{$Organization->organization_name}}"/>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group{{ $errors->has('organization_type') ? ' has-error' : '' }}">
                                        <label>Tipe</label>
                                        <select class="form-control selectpicker" name="organization_type" data-live-search="true">
                                            @foreach ($OrganizationTypes as $OrganizationType)
                                            @if($Organization->organization_type_id == $OrganizationType->id)
                                            <option selected="selected" value="{{$OrganizationType->id}}">{{$OrganizationType->organization_type_name}}</option>
                                            @else
                                            <option value="{{$OrganizationType->id}}">{{$OrganizationType->organization_type_name}}</option>
                                            @endif
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                                        <label>Alamat</label>
                                        <textarea class="form-control" rows="3" name="address" placeholder="Masukkan Alamat">{{ $OrganizationAddress->address }}</textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group{{ $errors->has('profile_img') ? ' has-error' : '' }}">
                                        <label for="exampleInputFile">Gambar Profil</label>
                                        <img src="{{ asset('/') }}./uploads/organizations/{{$Organization->image_file_path}}" alt="" class="img-responsive" width="20%" height="auto">
                                        <br/>
                                        <input type="file" id="exampleInputFile" name="profile_img" value="{{ old('profile_img') }}">
                                        <p class="help-block">Pastikan format gambar .jpg /.png dengan ukuran max 1MB.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group{{ $errors->has('city') ? ' has-error' : '' }}">
                                        <label>Lokasi</label>
                                        <input id="city_id" type="text" class="form-control" name="city_id" placeholder="Ketik lokasi kota" value="{{$OrganizationAddress->city->city_name}} , {{$OrganizationAddress->city->province->province_name}}"/>
                                        <input id="city" type="hidden" class="form-control" name="city" placeholder="Ketik lokasi kota" value="{{$OrganizationAddress->city_id}}"/>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group{{ $errors->has('postcode') ? ' has-error' : '' }}">
                                        <label>Kode Pos</label>
                                        <input type="text" class="form-control" name="postcode" placeholder="Masukkan Kodepos" value="{{$OrganizationAddress->postcode}}"/>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group{{ $errors->has('phonenumber') ? ' has-error' : '' }}">
                                        <label>No. Telepon</label>
                                        <input type="text" class="form-control" name="phonenumber" placeholder="Masukkan no telepon yang valid" value="{{$OrganizationAddress->phonenumber}}"/>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="basic-url">Nama URL</label>
                                        <div class="input-group{{ $errors->has('url_organization_name') ? ' has-error' : '' }}">
                                            <span class="input-group-addon" id="basic-addon3">www.adagigs.com/organization/</span>
                                            <input type="text" class="form-control" id="basic-url" aria-describedby="basic-addon3" name="url_organization_name" placeholder="Masukkan nama url" value="{{$Organization->url_organization_name}}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <label for="basic-url">Akun Social Media</label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="input-group{{ $errors->has('facebook_url') ? ' has-error' : '' }}">
                                                    <span class="input-group-addon" id="basic-addon3"><i class="fa fa-facebook-square" aria-hidden="true"></i> http://facebook.com/</span>
                                                    <input type="text" class="form-control" id="basic-url" aria-describedby="basic-addon3" name="facebook_url" placeholder="Masukkan url facebook" value="{{$Organization->facebook_url}}">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="input-group{{ $errors->has('twitter_url') ? ' has-error' : '' }}">
                                                    <span class="input-group-addon" id="basic-addon3"><i class="fa fa-twitter" aria-hidden="true"></i></span>
                                                    <input type="text" class="form-control" id="basic-url" aria-describedby="basic-addon3" name="twitter_url" placeholder="Masukkan url twitter" value="{{$Organization->twitter_url}}">
                                                </div>
                                            </div>
                                        </div>
                                        <br/>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="input-group{{ $errors->has('instagram_url') ? ' has-error' : '' }}">
                                                    <span class="input-group-addon" id="basic-addon3"><i class="fa fa-instagram" aria-hidden="true"></i></span>
                                                    <input type="text" class="form-control" id="basic-url" aria-describedby="basic-addon3" name="instagram_url" placeholder="Masukkan url instagram" value="{{$Organization->instagram_url}}">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="input-group">
                                                    <span class="input-group-addon" id="basic-addon3"><i class="fa fa-link" aria-hidden="true"></i></span>
                                                    <input type="text" class="form-control" id="basic-url" aria-describedby="basic-addon3" name="website_url" placeholder="Masukkan url website" value="{{$Organization->organization_website}}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="bottomBtn">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="col-md-6">
                                            <button class="btn btn-md btn-block btn-orange" type="submit"><i class="fa fa-pencil" aria-hidden="true"></i> Ubah Akun</button>
                                        </div>
                                        <div class="col-md-6">
                                            <a class="btn btn-md btn-block btn-gray-round" href="{{ URL::previous() }}">Kembali</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection