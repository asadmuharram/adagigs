@extends('layouts.dashboard.auth')
@section('title', 'Dashboard')
@section('content')
@include('common.errors')
@include('common.notifications')
<div class="container">
    @include('layouts.dashboard.sidepanel')
    <div class="col-md-9">
        <div class="row">
            <div class="col-md-12">
                <div class="panelGigs bgCard">
                    <div class="panelTitle">
                        <h4 class="mainTitle">NEWS FEED</h4>
                        <h5 class="subTitle">Berikut adalah update dari gigs dan musisi / band yang kamu ikuti.</h5>
                    </div>
                    <div class="panelBody">
                        @foreach($unreadNotifications as $notification)
                        @if($notification->is_read == 0)
                        <div class="media unreadNotif">
                        @else
                        <div class="media">
                        @endif
                            <div class="media-left">
                                <img class="thumb img-rounded" src="{{ url('/images/profile/'.$notification->gig->gig_image_file_path) }}" alt="{{$notification->gig->gig_name}}" >
                            </div>
                            <div class="media-body">
                                <h4 class="media-heading">
                                @if($notification->is_read == 0)
                                <span class="label label-success">New</span>
                                @endif
                                {{ $notification->subject }} - <small>{{ $notification->sent_at->diffForHumans() }}</small></h4>
                                <p class="body">{!! $notification->body !!}.</p>
                            </div>
                            <div class="media-right">
                                <a class="btn btn-sm btn-block btn-orange-round vertical-align" href="/notification/read/{{ $notification->id }}">Lihat</a>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 col-xs-12 col-md-offset-3">
                    {!! $unreadNotifications->render() !!}
                </div>
        </div>
        </div>
    </div>
</div>
@endsection