@extends('layouts.dashboard.auth')
@section('title', 'Dashboard')
@section('content')
@include('common.errors')
@include('common.notifications')
<div class="container">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-12 col-xs-12 col-centered">
                <h2 class="text-center">Selamat Datang di Adagigs.</h2>
                <h4 class="text-center">Silahkan pilih tipe akun anda.</h4>
            </div>
        </div>
        <div class="row">
            <div class="account-section">
                <div class="col-md-6 text-center">
                    <div class="panelGigs bgCard">
                        <div class="panelTitle">
                            <h3 class="mainTitleBig text-center">ORGANISASI</h3>
                            <h5 class="subTitle text-center">Dengan akun organisasi, anda dapat membuat gig dan mengundang musisi atau band untuk berpartisipasi dalam gig yang anda buat.</h5>
                        </div>
                        <div class="panelBody">
                            <img src="{{ URL::asset('assets/images/stage_img.jpg') }}" height="200px" width="auto">
                        </div>
                        <div class="panelFooter">
                            <div class="row">
                                <div class="col-md-4 col-md-offset-4">
                                    <a href="{{ URL::to('/dashboard/organization/create') }}">
                                        <button class="btn btn-md btn-block btn-orange" type="button"> Pilih</button>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 text-center">
                    <div class="panelGigs bgCard">
                        <div class="panelTitle">
                            <h3 class="mainTitleBig text-center">MUSISI / BAND</h3>
                            <h5 class="subTitle text-center">Dengan akun musisi / band, anda dapat mencari gig yang sesuai dengan genre musik anda dan ikut berpartisipasi dalam gig yang ada.</h5>
                        </div>
                        <div class="panelBody">
                            <img src="{{ URL::asset('assets/images/drum_img.jpg') }}" height="200px" width="auto">
                        </div>
                        <div class="panelFooter">
                            <div class="row">
                                <div class="col-md-4 col-md-offset-4">
                                    <a href="{{ URL::to('/dashboard/band/create') }}">
                                        <button class="btn btn-md btn-block btn-orange" type="submit"> Pilih</button>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection