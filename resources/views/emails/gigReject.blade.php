<div bgcolor="#F8F8F8" style="font-family:Arial,sans-serif;width:100%!important;min-height:100%;font-size:18px;line-height:20px;background:#f8f8f8;margin:0;padding:0">
  <table style="font-family:Arial,sans-serif;width:100%;margin:0;padding:0">
    <tbody><tr style="font-family:Arial,sans-serif;margin:0;padding:0">
      <td style="font-family:Arial,sans-serif;margin:0;padding:0"></td>
      <td style="font-family:Arial,sans-serif;display:block!important;max-width:600px!important;clear:both!important;margin:0 auto;padding:0">
        <div style="font-family:Arial,sans-serif;max-width:600px;display:block;margin:30px auto 0;padding:0">
          <table style="font-family:Arial,sans-serif;width:100%;margin:0;padding:0">
            <tbody><tr style="font-family:Arial,sans-serif;margin:0;padding:0">
              <td style="font-family:Arial,sans-serif;margin:0;padding:0"><a href="" style="display:block;font-family:'Open Sans','HelveticaNeue-Light','Helvetica Neue Light','Helvetica Neue',Helvetica,Arial,'Lucida Grande',sans-serif;text-decoration:none;color:#347eb9;font-weight:normal;font-size:15px;line-height:1.6;margin:0 0 0 10px;padding:0" target="_blank" data-saferedirecturl="">
              <img src="http://adagigs.com/logoadagigs.png" height="30px" style="font-family:Arial,sans-serif;max-width:100%;text-decoration:none;margin:0 0 0 10px;padding:0;border:none"></a></td>
            </tr>
          </tbody></table>
        </div>
      </td>
      <td style="font-family:Arial,sans-serif;margin:0;padding:0"></td>
    </tr>
  </tbody></table>
  <table style="font-family:Arial,sans-serif;width:100%;margin:0;padding:0">
    <tbody><tr style="font-family:Arial,sans-serif;margin:0;padding:0">
      <td style="font-family:Arial,sans-serif;margin:0;padding:0"></td>
      <td style="font-family:Arial,sans-serif;display:block!important;max-width:600px!important;clear:both!important;margin:0 auto;padding:0">
        <div style="max-width:545px;border-radius:5px;font-family:Arial,sans-serif;display:block;background:#ffffff;margin:0 auto;padding:0 0 20px">
          <table style="font-family:Arial,sans-serif;width:100%;margin:0;padding:0">
            <tbody><tr style="font-family:Arial,sans-serif;margin:0;padding:0">
              <td style="font-family:Arial,sans-serif;margin:0;padding:0">
                <a href="" style="font-family:'Open Sans','HelveticaNeue-Light','Helvetica Neue Light','Helvetica Neue',Helvetica,Arial,'Lucida Grande',sans-serif;color:#347eb9;font-weight:normal;font-size:15px;line-height:1.6;text-decoration:none;margin:0 0 10px;padding:0" target="_blank" data-saferedirecturl=""><img alt="" src="http://adagigs.com/EmailHeader_MailIcon.jpg" style="font-family:Arial,sans-serif;max-width:100%;text-decoration:none;margin:0 0 0;padding:0;border:none"></a>
                <div style="font-family:Arial,sans-serif;max-width:465px;margin:0;padding:0 40px">
                  <h3 style="font-family:'Open Sans','HelveticaNeue-Light','Helvetica Neue Light','Helvetica Neue',Helvetica,Arial,'Lucida Grande',sans-serif;line-height:1.4;color:#3f526d;font-weight:500;font-size:20px;margin:30px 0 12px;padding:0">Hai {{$bandName}},</h3>
                  <p style="font-family:'Open Sans','HelveticaNeue-Light','Helvetica Neue Light','Helvetica Neue',Helvetica,Arial,'Lucida Grande',sans-serif;font-weight:normal;color:#3f526d;font-size:16px;line-height:1.6;margin:0 0 10px;padding:0;text-align:justify">Mohon maaf, permintaan anda ditolak oleh {{$orgName}}.</p>
                </div>
                <div style="font-family:Arial,sans-serif;max-width:465px;margin:0;padding:0 40px">
                  <p style="font-family:'Open Sans','HelveticaNeue-Light','Helvetica Neue Light','Helvetica Neue',Helvetica,Arial,'Lucida Grande',sans-serif;font-weight:normal;color:#96a6b0;font-size:15px;line-height:1.6;text-align:center;margin:0 0 10px;padding:0" align="center"><a href="{{url('/gigs')}}" style="font-family:'Open Sans','HelveticaNeue-Light','Helvetica Neue Light','Helvetica Neue',Helvetica,Arial,'Lucida Grande',sans-serif;color:#fff;border-radius:50px;text-decoration:none;font-size:14px;font-weight:bold;display:inline-block;width:auto;text-align:center;line-height:1.6;background:#ff5133;margin:20px auto;padding:15px 40px 15px 30px" target="_blank">CARI GIG LAIN</a></p>
                </div>
                <div style="font-family:Arial,sans-serif;max-width:465px;margin:0;padding:0 40px">
                  <div style="font-family:Arial,sans-serif;border-top-width:1px;border-top-color:rgba(0,0,0,0.20);border-top-style:dotted;clear:both;margin:20 0;padding:0"></div>
                  <div style="font-family:Arial,sans-serif;border-top-width:1px;border-top-color:rgba(0,0,0,0.20);border-top-style:dotted;clear:both;margin:20 0;padding:0"></div>
                  <div style="font-family:Arial,sans-serif;display:block;clear:both;margin:0;padding:10px 0">
                    <p style="font-family:'Open Sans','HelveticaNeue-Light','Helvetica Neue Light','Helvetica Neue',Helvetica,Arial,'Lucida Grande',sans-serif;font-weight:normal;color:#3f526d;font-size:16px;line-height:1.6;float:left;width:50.0000%;max-width:600px;margin-bottom:20;padding:0">
                      Salam,<br style="font-family:Arial,sans-serif;margin:0;padding:0">
                      Adagigs
                    </p>
                  </div>
                </div>
              </td>
            </tr>
          </tbody></table>
        </div>
      </td>
      <td style="font-family:Arial,sans-serif;margin:0;padding:0"></td>
    </tr>
  </tbody></table>
  <table style="font-family:Arial,sans-serif;width:100%;clear:both!important;margin:20px 0 0;padding:0">
    <tbody><tr style="font-family:Arial,sans-serif;margin:0;padding:0">
      <td style="font-family:Arial,sans-serif;margin:0;padding:0"></td>
      <td style="font-family:Arial,sans-serif;display:block!important;max-width:600px!important;clear:both!important;margin:0 auto;padding:0">
        
        <div style="font-family:Arial,sans-serif;max-width:600px;display:block;margin:0 auto;padding:0">
          <table style="font-family:Arial,sans-serif;width:100%;margin:0;padding:0">
            <tbody>
          </tbody></table>
        </div>
         <!--<div style="font-family:Arial,sans-serif;text-align:center;margin:0;padding:0" align="center">
          <p style="font-family:'Open Sans','HelveticaNeue-Light','Helvetica Neue Light','Helvetica Neue',Helvetica,Arial,'Lucida Grande',sans-serif;color:#979797;font-size:12px;text-align:center;font-weight:normal;line-height:1.6;margin:0 0 10px;padding:0" align="center">
            <!--<a href="" style="font-family:'Open Sans','HelveticaNeue-Light','Helvetica Neue Light','Helvetica Neue',Helvetica,Arial,'Lucida Grande',sans-serif;color:#d7d7d7;font-weight:normal;font-size:15px;line-height:1.6;text-decoration:none;margin:0 0 10px;padding:0" target="_blank" data-saferedirecturl=""><u></u>Unsubscribe<u></u></a>
            <span style="font-family:Arial,sans-serif;color:#d7d7d7;margin:0;padding:0 10px">|</span>
            <a href="" style="font-family:'Open Sans','HelveticaNeue-Light','Helvetica Neue Light','Helvetica Neue',Helvetica,Arial,'Lucida Grande',sans-serif;color:#d7d7d7;font-weight:normal;font-size:15px;line-height:1.6;text-decoration:none;margin:0 0 10px;padding:0" target="_blank" data-saferedirecturl="">View this email online</a>
          </p>
        </div>-->
        <p style="font-family:'Open Sans','HelveticaNeue-Light','Helvetica Neue Light','Helvetica Neue',Helvetica,Arial,'Lucida Grande',sans-serif;color:#979797;font-size:10px;text-align:center;font-weight:normal;line-height:1.6;margin:0 0 10px;padding:0" align="center">2016 Adagigs | <span style="font-family:Arial,sans-serif;color:#d7d7d7;font-size:10px!important;margin:0;padding:0">www.adagigs.com</span></p>
      </td>
    </tr>
  </tbody></table>
</div></div>