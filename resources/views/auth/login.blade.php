@extends('layouts.dashboard.auth')
@section('title', 'Login')
@section('content')
@include('common.errors')
@include('common.notifications')
<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="bgCard-auth">
            <form class="" role="form" method="POST" action="{{ url('/login') }}">
                {!! csrf_field() !!}
                <h2 class="form-signin-heading">Masuk</h2>
                <hr/>
                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <input type="email" class="form-control" name="email" placeholder="Alamat Email" value="{{ old('email') }}"/>
                </div>
                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <input type="password" class="form-control" name="password" placeholder="Password" />
                </div>
                <div class="row">
                <div class="col-md-6">
                    <label class="checkbox">
                    <input type="checkbox" value="remember-me" id="rememberMe" name="rememberMe"> Biarkan saya tetap masuk
                    </label>
                </div>
                <div class="col-md-6">
                    <label class="checkbox">
                    <a class="btn btn-link rstPas" href="{{ url('/password/reset') }}">Lupa password ?</a>
                    </label>
                </div>
                </div>
                <button class="btn btn-md btn-block btn-orange" type="submit">Masuk</button>
                </form>
                <div class="separator"><span>Atau</span></div>
                <form role="form" method="POST" action="{{url('/connect/redirect', ['facebook'])}}">
                    {!! csrf_field() !!}
                    <button class="btn btn-block btn-social btn-facebook" type="submit"><i class="fa fa-facebook"></i> Masuk dengan Facebook</button>
                </form>
                <hr/>
                <span>Belum punya akun ?<a class="btn btn-link" href="{{ url('/register') }}">Daftar</a></span>
            </div>
        </div>
    </div>
</div>
@endsection