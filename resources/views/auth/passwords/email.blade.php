@extends('layouts.dashboard.auth')
@section('title', 'Reset Password')
<!-- Main Content -->
@section('content')
@if(Session::has('status'))
<div class="alert alert-success alert-dismissible animated fadeInDown" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    {{ Session::get('status') }}
</div>
@endif
@if ($errors->has('email'))
<div class="alert alert-danger alert-dismissible animated fadeInDown" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    Kami tidak menemukan akun dengan alamat email tersebut. Silahkan cek kembali.
</div>
@endif
<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="bgCard-auth">
                <form class="" role="form" method="POST" action="{{ url('/password/email') }}">
                    {!! csrf_field() !!}
                    <h2 class="form-signin-heading">Reset Password</h2>
                    <hr/>
                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <input type="email" class="form-control" name="email" placeholder="Alamat Email" value="{{ old('email') }}"/>
                    </div>
                    <button class="btn btn-md btn-block btn-orange" type="submit">Reset Password</button>
                </form>
                <hr/>
                Sudah punya akun ? <a href="{{ url('/login') }}">Login</a>
            </div>
        </div>
    </div>
</div>
@endsection