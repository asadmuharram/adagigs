@extends('layouts.dashboard.auth')
@section('title', 'Reset Password')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="bgCard-auth">
            <form class="" role="form" method="POST" action="{{ url('/password/reset') }}">      
                        {!! csrf_field() !!}
                        <h2 class="form-signin-heading">Ubah Password</h2>
                        <hr/>
                        <input type="hidden" name="token" value="{{ $token }}">
                        <input type="hidden" name="email" value="{{ $email or old('email') }}">
                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <input type="password" class="form-control" name="password" placeholder="password baru">
                                @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                                @endif
                        </div>
                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                <input type="password" class="form-control" name="password_confirmation" placeholder="konfirmasi password">
                                @if ($errors->has('password_confirmation'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password_confirmation') }}</strong>
                                </span>
                                @endif
                        </div>
                        <button class="btn btn-md btn-block btn-orange" type="submit">Ubah Password</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection