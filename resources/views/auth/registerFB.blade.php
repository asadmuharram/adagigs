@extends('layouts.dashboard.auth')
@section('title', 'Register')
@section('content')
<?php
$days = range(1, 31);
$years = range(1932, 2002);
?>
@include('common.errors')
@include('common.notifications')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <form class="form-signin" role="form" method="POST" action="{{ url('/register') }}">
                {!! csrf_field() !!}
                <input type="hidden" class="form-control" name="type" value="{{ $type }}" />
                <h2 class="form-signin-heading">Daftar Akun</h2>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
                            <input type="first_name" class="form-control" name="first_name" placeholder="Nama Depan" value="{{ $first_name }}" readonly="readonly"/>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                            <input type="last_name" class="form-control" name="last_name" placeholder="Nama Belakang" value="{{ $last_name }}" readonly="readonly"/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <input type="email" class="form-control" name="email" placeholder="Alamat Email" value="{{ $email }}" readonly="readonly"/>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                            <input type="text" class="form-control" name="username" placeholder="Username" value="{{ old('username') }}" />
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <input type="password" class="form-control" name="password" placeholder="Password"/>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <input type="password" class="form-control" name="password_confirmation" placeholder="Konfirmasi Password"/>
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-12">
                        <label class="form-group">Tanggal Lahir <i id="tooltip" class="fa fa-question-circle" data-toggle="tooltip" data-placement="right" title="" data-original-title="Adagigs mensyaratkan umur minimal 18 tahun."></i></label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4 col-md-4">
                        <div class="form-group{{ $errors->has('day_birth') ? ' has-error' : '' }}">
                            {{ Form::select('day_birth', $days, null, ['placeholder' => 'Tanggal','class' => 'form-control']) }}
                        </div>
                    </div>
                    <div class="col-sm-4 col-md-4">
                        <div class="form-group{{ $errors->has('month_birth') ? ' has-error' : '' }}">
                            {{ Form::select('month_birth', [
                            '1' => 'Januari',
                            '2' => 'Februari',
                            '3' => 'Maret',
                            '4' => 'April',
                            '5' => 'Mei',
                            '6' => 'Juni',
                            '7' => 'Juli',
                            '8' => 'Agustus',
                            '9' => 'September',
                            '10' => 'Oktober',
                            '11' => 'November',
                            '12' => 'Desember',] , null, ['placeholder' => 'Bulan','class' => 'form-control']
                            ) }}
                        </div>
                    </div>
                    <div class="col-sm-4 col-md-4">
                        <div class="form-group{{ $errors->has('year_birth') ? ' has-error' : '' }}">
                            {{ Form::select('year_birth', $years, null, ['placeholder' => 'Tahun','class' => 'form-control']) }}
                        </div>
                    </div>
                </div>
                <hr/>
                <div class="row">
                    <div class="col-sm-12 col-md-12">
                        <p>Dengan menekan tombol Daftar, saya mengkonfirmasi telah menyetujui <a href="#">Syarat dan Ketentuan</a> , serta <a href="#">Kebijakan Privasi</a> Adagigs.</p>
                        <button class="btn btn-md btn-block btn-orange" type="submit">Daftar</button>
                    </div>
                </div>
                <hr/>
                <span>Sudah Punya Akun ?<a class="btn btn-link" href="{{ url('/login') }}">Login</a></span>
            </form>
            
            
        </div>
    </div>
</div>
@endsection