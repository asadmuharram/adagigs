@extends('layouts.dashboard.auth')
@section('title', 'Halaman tidak ditemukan')
@section('content')
<div class="container">
<div class="row">
<div class="col-md-12 col-md-12 text-center">
    <img src="{{ URL::asset('assets/images/404_icn.png') }}">
    <h2>Halaman tidak ditemukan.</h2>
</div>
</div>
@endsection