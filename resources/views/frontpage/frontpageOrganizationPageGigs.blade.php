@extends('layouts.dashboard.auth')
@section('title', $Organization->organization_name.' - Gig')
@section('content')
<div class="container">
	<div class="col-md-12 col-sm-12">
		<div class="card hovercard">
			<div class="card-background">
				<img class="card-bkimg" alt="" src="{{ url('/images/profile/'.$Organization->image_file_path) }}">
				<!-- http://lorempixel.com/850/280/people/9/ -->
			</div>
			<div class="useravatar">
				<img alt="" src="{{ url('/images/profile/'.$Organization->image_file_path) }}">
			</div>
			<div class="card-info">
				<span class="card-title">{{$Organization->organization_name}} | <span style="font-family: 'Proxima Nova Rg';">{{$Organization->organizationtype->organization_type_name}}</span></span>
			</div>
			<div class="card-info-detail">
				<span class="card-title-detail"><i class="fa fa-map-marker" aria-hidden="true"></i> {{$OrganizationAddress->address}}, {{$OrganizationAddress->city->city_name}}, {{$OrganizationAddress->city->province->province_name}}</span>
			</div>
		</div>
		<div class="btn-pref btn-group btn-group-justified btn-group-lg" role="group" aria-label="...">
			<div class="btn-group" role="group">
				<a href="/organization/{{$Organization->url_organization_name}}"><button type="button" class="btn btn-white-tab">
					<i class="fa fa-info-circle" aria-hidden="true"></i>
					<div class="hidden-xs">Info</div>
				</button></a>
			</div>
			<div class="btn-group" role="group">
				<button type="button" class="btn btn-white-tab-active" href="#tab2" data-toggle="tab">
				<i class="fa fa-play" aria-hidden="true"></i>
				<div class="hidden-xs">Gig</div>
				</button>
			</div>
		</div>
		<div class="well">
			<div class="tab-content">
				<div class="tab-pane fade in active">
					@if(count($Gigs) > 0)
					<div class="panelBandList">
							@foreach($Gigs as $Gig)
								<div class="row">
								<div class="col-md-12">
									<div class="media">
										<div class="media-left">
											<img class="thumb img-circle" src="{{ asset('/') }}./uploads/gigs/{{$Gig->gig_image_file_path}}" alt="{{$Gig->gig_name}}" >
										</div>
										<div class="media-body">
											<h4 class="titleBand">{{$Gig->gig_name}}</h4>
										</div>
										<div class="media-right">
											<a class="btn btn-md btn-block btn-orange-round vertical-align" href="/{{$Organization->url_organization_name}}/gig/{{$Gig->slug}}">Detail</a>
										</div>
									</div>
									</div>
								</div>
							@endforeach
					</div>
					@else
					<h5 class="subTitle">Belum pernah mengadakan gig.</h5>
					@endif
				</div>
			</div>
		</div>
		
	</div>
</div>
@endsection