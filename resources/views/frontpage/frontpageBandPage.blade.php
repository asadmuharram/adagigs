@extends('layouts.dashboard.auth')
@section('title', $Band->band_name)
@section('content')
<div class="container">
	<div class="col-md-12 col-sm-12">
		<div class="card hovercard-gig">
			<div class="card-background-gig">
				<img class="card-bkimg" alt="" src="{{ url('/images/profile/'.$Band->image_band_profile_path) }}">
				<!-- http://lorempixel.com/850/280/people/9/ -->
			</div>
			<div class="useravatar-gig">
				<img alt="" src="{{ url('/images/profile/'.$Band->image_band_profile_path) }}">
			</div>
			<div class="card-info-genre-gig">
                <span class="label label-transparent"style="font-family: 'Proxima Nova Rg';">{{$BandGenre->genre->genre_name}}</span>
            </div>
			<div class="card-info-gig">
				<span class="card-title-gig">{{$Band->band_name}} | <span style="font-family: 'Proxima Nova Rg';">{{$Band->bandtype->band_type_name}}</span></span>
			</div>
			<div class="card-info-detail-gig">
				</div>
			<div class="card-info-detail-gig">
				<span class="card-title-detail-gig"><i class="fa fa-map-marker" aria-hidden="true"></i> {{$BandLocation->city->city_name}}, {{$BandLocation->city->province->province_name}}</span>
			</div>
		</div>
		<div class="btn-pref btn-group btn-group-justified btn-group-lg" role="group" aria-label="...">
			<div class="btn-group" role="group">
				<button type="button" class="btn btn-white-tab-active">
				<i class="fa fa-info-circle" aria-hidden="true"></i>
				<div class="hidden-xs">Info</div>
				</button>
			</div>
			<div class="btn-group" role="group">
				<a href="/band/{{$Band->url_band_name}}/gig"><button type="button" class="btn btn-white-tab">
				<i class="fa fa-play" aria-hidden="true"></i>
				<div class="hidden-xs">Gig</div>
				</button></a>
			</div>
		</div>
		<div class="well">
			<div class="tab-content">
				<div class="tab-pane fade in active">
						<div class="panelBody">
						<div class="ProfileOrg">
                        	<div class="row">
                                <div class="col-md-12">
                                    <h4 class="mainTitle">Biography</h4>
                                    <p class="descBand">{{$Band->description}}</p>
                                </div>
                            </div>
                            </div>
                        <div class="ProfileOrg">
                            <div class="row">
                                <div class="col-md-6 col-xs-12">
                                    <h4 class="mainTitle">Sosial Media</h4>
                                    <?php
                                    if(!empty($Band->facebook_url)){
                                    ?>
                                    <div class="col-md-2 col-xs-2">
                                        <a href="http://facebook.com/{{$Band->facebook_url}}" target="blank"><i class="fa fa-facebook-square fa-2x" aria-hidden="true"></i></a>
                                    </div>
                                    <?php
                                    }
                                    if(!empty($Band->twitter_url)){
                                    ?>
                                    <div class="col-md-2 col-xs-2">
                                        <a href="http://twitter.com/{{$Band->twitter_url}}" target="blank"><i class="fa fa-twitter fa-2x" aria-hidden="true"></i></a>
                                    </div>
                                    <?php
                                    }
                                    if(!empty($Band->instagram_url)){
                                    ?>
                                    <div class="col-md-2 col-xs-2">
                                        <a href="http://instagram.com/{{$Band->instagram_url}}" target="blank"><i class="fa fa-instagram fa-2x" aria-hidden="true"></i></a>
                                    </div>
                                    <?php
                                    }
                                    if(!empty($Band->soundcloud_url)){
                                    ?>
                                    <div class="col-md-2 col-xs-2">
                                        <a href="http://soundcloud.com/{{$Band->soundcloud_url}}" target="blank"><i class="fa fa-soundcloud fa-2x" aria-hidden="true"></i></a>
                                    </div>
                                    <?php
                                    }
                                    if(!empty($Band->youtube_url)){
                                    ?>
                                    <div class="col-md-2 col-xs-2">
                                        <a href="http://youtube.com/channel/{{$Band->youtube_url}}" target="blank"><i class="fa fa-youtube-play fa-2x" aria-hidden="true"></i></a>
                                    </div>
                                    <?php
                                    }
                                    ?>
                                </div>
                                <div class="col-md-6 col-xs-12">
                                    <h4 class="mainTitle">Website</h4>
                                    <p class="descBand"><a href="http://{{$Band->website_url}}" target="blank">{{$Band->website_url}}</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
				</div>
			</div>
		</div>
		
	</div>
</div>
@endsection