<?php use Carbon\Carbon;
Carbon::setLocale('id');
?>
@extends('layouts.frontpage.app')
@section('title', 'Home')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-xs-12 col-md-12 col-sm-height">
            <div class="col-xs-10 col-md-10">
                <h2 class="headerTitle">Gig terkini</h2>
            </div>
            <div class="col-xs-10 col-md-2">
            </div>
        </div>
    </div>
    <div class="row nopadding">
        <div class="col-xs-12 col-md-12 col-sm-height nopadding">
            @foreach($Gigs->chunk(3) as $items)
            <div class="row nopadding">
                @foreach($items as $Gig)
                <div class="col-xs-12 col-md-4 col-sm-height row-height ">
                    <div class="card-gig">
                        <div class="card-image">
                            <div class="gradient"></div>
                            <img class="img-responsive" src="images/imagegig/{{$Gig->gig_image_file_path}}">
                            <span class="card-info-date">
                                <div class="date">
                                    <div class="day">{{$Gig->gig_date->format('d')}}</div>
                                    <div class="month">{{$Gig->gig_date->format('M')}}</div>
                                </div>
                            </span>
                            <span class="card-info-title">
                                @foreach ($Organizations as $Organization)
                                @if($Gig->organization_id == $Organization->id)
                                <a href="/{{$Organization->url_organization_name}}/gig/{{$Gig->slug}}">{{$Gig->gig_name}}</a>
                                @endif
                                @endforeach
                            </span>
                            <span class="card-info-by">
                                @foreach ($Gig->giglocation as $giglocation)
                                <i class="fa fa-map-marker" aria-hidden="true"></i> {{$giglocation->city->city_name}}, {{$giglocation->city->province->province_name}}
                                @endforeach
                            </span>
                        </div>
                        <div class="card-content">
                            <div class="card-info-regisTime">
                                @if($Gig->gig_band_closing_date <= $nowTime OR $Gig->status == 'close')
                                <h4 class="registTitle"><i class="fa fa-clock-o" aria-hidden="true"></i> Akhir Registrasi : <span class="registInfo">Telah Selesai</span></h4>
                                @else
                                <h4 class="registTitle"><i class="fa fa-clock-o" aria-hidden="true"></i> Akhir Registrasi : <span class="registInfo">{{$Gig->gig_band_closing_date->diffForHumans()}}</span></h4>
                                @endif
                            </div>
                        </div>
                        <div class="card-action">
                            <div class="row">
                                <div class="col-md-12 col-xs-12">
                                    <div class="card-info-by">
                                        @foreach ($Organizations as $Organization)
                                        @if($Gig->organization_id == $Organization->id)
                                        Diselenggarakan Oleh <a href="{{ url('/organization/'.$Organization->url_organization_name) }}">{{$Organization->organization_name}}</a>
                                        @endif
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
            @endforeach
        </div>
    </div>
    <div class="row nopadding">
        <div class="col-xs-12 col-md-12 col-sm-height nopadding">
            <div class="col-xs-12 col-md-4 col-md-offset-4">
                <div class="btn-more-wrap">
                    <a href="/gigs"><button class="btn btn-lg btn-block btn-gray-round"> Lihat Gig Lainnya <i class="fa fa-long-arrow-right" aria-hidden="true"></i></button></a>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-md-12 col-sm-height">
            <div class="col-xs-10 col-md-10">
                <h2 class="headerTitle">Musisi / Band</h2>
            </div>
            <div class="col-xs-10 col-md-2">
            </div>
        </div>
    </div>
    <div class="row nopadding">
        <div class="col-xs-12 col-md-12 col-sm-height nopadding">
            @foreach($Bands->chunk(3) as $items)
            <div class="row nopadding">
                @foreach($items as $Band)
                <div class="col-xs-12 col-md-4 col-sm-height row-height ">
                    <div class="card-gig">
                        <div class="card-image">
                            <div class="gradient"></div>
                            <img class="img-responsive" src="images/imagegig/{{$Band->image_band_profile_path}}">
                            <span class="card-info-title">
                                {{$Band->band_name}}
                            </span>
                            <span class="card-info-by">
                                @foreach ($BandLocations as $BandLocation)
                                @if($BandLocation->band_id == $Band->id)
                                <i class="fa fa-map-marker" aria-hidden="true"></i> {{$BandLocation->city->city_name}}, {{$BandLocation->city->province->province_name}}
                                @endif
                                @endforeach
                            </span>
                        </div>
                        <div class="card-action">
                            <div class="row">
                                <div class="col-md-8 col-xs-8">
                                    <span class="label label-transparent">{{$Band->bandtype->band_type_name}}</span>
                                    @foreach ($BandGenres as $BandGenre)
                                    @if($BandGenre->band_id == $Band->id)
                                    <span class="label label-transparent">{{$BandGenre->genre->genre_name}}</span>
                                    @endif
                                    @endforeach
                                </div>
                                <div class="col-md-4 col-xs-4 text-right">
                                    <a href="/band/{{$Band->url_band_name}}">Lihat Profil <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
            @endforeach
        </div>
    </div>
    <div class="row nopadding">
        <div class="col-xs-12 col-md-12 col-sm-height nopadding">
            <div class="col-xs-12 col-md-4 col-md-offset-4">
                <div class="btn-more-wrap">
                    <a href="/bands"><button class="btn btn-lg btn-block btn-gray-round"> Lihat Musisi / Band Lainnya <i class="fa fa-long-arrow-right" aria-hidden="true"></i></button></a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection