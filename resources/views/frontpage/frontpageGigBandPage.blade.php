<?php use Carbon\Carbon;
Carbon::setLocale('id');
?>
@extends('layouts.dashboard.auth')
@section('title', $Gig->gig_name)
@section('content')
<div class="container">
    <div class="col-md-12 col-sm-12">
        <div class="card hovercard-gig">
            <div class="card-background-gig">
                <img class="card-bkimg" alt="" src="{{ url('/images/profile/'.$Gig->gig_image_file_path) }}">
                <!-- http://lorempixel.com/850/280/people/9/ -->
            </div>
            <div class="useravatar-gig">
                <img alt="" src="{{ url('/images/profile/'.$Gig->gig_image_file_path) }}">
            </div>
            <div class="card-info-apply-gig">
                @if(Auth::check())
                @if(Auth::user()->type == 'band')
                @if(count($BandGigs) > 0)
                @foreach ($BandGigs as $BandGig)
                @if($BandGig->gig_id == $Gig->id)
                @if($BandGig->status == 'accept')
                <div class="card-info-apply-gig">
                    <button id="applyGig" type="button" class="btn btn-md btn-block btn-green"><i class="fa fa-check" aria-hidden="true"></i> Diterima</button>
                </div>
                @elseif($BandGig->status == 'pending')
                <div class="card-info-apply-gig">
                    <button id="applyGig" type="button" class="btn btn-md btn-block btn-yellow"><i class="fa fa-hourglass-start" aria-hidden="true"></i> Menunggu Konfirmasi</button>
                </div>
                @else
                <div class="card-info-apply-gig">
                    <button class="btn btn-md btn-red" type="button"><i class="fa fa-times" aria-hidden="true"></i> Ditolak</button>
                </div>
                @endif
                @endif
                @endforeach
                @else
                <div class="card-info-apply-gig">
                    <button type="button" class="btn btn-md btn-block btn-orange"  data-toggle="modal" data-target=".bs-gig-modal-md" ><i class="fa fa-paper-plane" aria-hidden="true"></i> Ikut Serta</button>
                </div>
                @endif
                @endif
                @endif
            </div>
            <div class="card-info-genre-gig">
                @foreach ($GigGenres as $GigGenre)
                @if($Gig->id == $GigGenre->gig_id)
                <span class="label label-transparent" style="font-family: 'Proxima Nova Rg';">{{$GigGenre->genre->genre_name}}</span>
                @endif
                @endforeach
            </div>
            <div class="card-info-gig">
                <span class="card-title-gig">{{$Gig->gig_name}}</span>
            </div>
            <div class="card-info-detail-gig">
                <span class="card-title-detail-gig"><i class="fa fa-map-marker" aria-hidden="true"></i> {{$GigLocation->venue_name}}, {{$GigLocation->city->city_name}}, {{$GigLocation->city->province->province_name}}</span>
            </div>
        </div>
        <div class="btn-pref btn-group btn-group-justified btn-group-lg" role="group" aria-label="...">
            <div class="btn-group" role="group">
                <a href="/{{$Organization->url_organization_name}}/gig/{{$Gig->slug}}"><button type="button" class="btn btn-white-tab">
                    <i class="fa fa-info-circle" aria-hidden="true"></i>
                    <div class="hidden-xs">Info</div>
                </button></a>
            </div>
            <div class="btn-group" role="group">
                <button type="button" class="btn btn-white-tab-active">
                <i class="fa fa-play" aria-hidden="true"></i>
                <div class="hidden-xs">Musisi / Band</div>
                </button>
            </div>
        </div>
        <div class="well">
            <div class="tab-content">
                <div class="tab-pane fade in active">
                    @if(count($GigbandsAccept) > 0)
                    <div class="panelBandList">                      
                            @foreach($GigbandsAccept as $Band)
                                <div class="row">
                                <div class="col-md-12">
                                    <div class="media">
                                        <div class="media-left">
                                            <img class="thumb img-circle" src="{{ asset('/') }}./uploads/bands/{{$Band->band->image_band_profile_path}}" alt="{{$Band->band->band_name}}" >
                                        </div>
                                        <div class="media-body">
                                            <h4 class="titleBand">{{$Band->band->band_name}}</h4>
                                        </div>
                                        <div class="media-right">
                                            <a class="btn btn-md btn-block btn-orange-round" href="/band/{{$Band->band->url_band_name}}">Detail</a>
                                        </div>
                                    </div>
                                    </div>
                                </div>           
                            @endforeach
                    </div>
                    @else
                    <h5 class="subTitle">Belum ada musisi / band yang berpartisipasi.</h5>
                    @endif
                </div>
            </div>
        </div>
        
    </div>
</div>
@if(Auth::check())
@if(Auth::user()->type == 'band')
<div class="modal fade bs-gig-modal-md" tabindex="-1" role="dialog" aria-labelledby="GigModalLabel">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <form action="{{ url('/'.$Organization->url_organization_name.'/gig/'.$Gig->slug.'/apply') }}" method="POST" style="display: inline">
                {{ csrf_field() }}
                {{ method_field('POST') }}
                <input type="hidden" value="{{$Gig->id}}" name="gig"></input>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Anda hanya dapat mengajukan permintaan ini 1x dalam 1 gig, Anda yakin untuk ikut serta ?</h4>
                </div>
                <div class="modal-footer">
                    <div class="col-md-6">
                        <button type="submit" class="btn btn-md btn-block btn-orange"> Ya</button>
                    </div>
                    <div class="col-md-6">
                        <button type="button" class="btn btn-md btn-block btn-gray-round" data-dismiss="modal">Tidak</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endif
@endif
@endsection