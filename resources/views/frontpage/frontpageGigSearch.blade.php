<?php use Carbon\Carbon;
Carbon::setLocale('id');
?>
@extends('layouts.dashboard.auth')
@section('title', 'Cari Gigs')
@section('content')
@include('common.errors')
@include('common.notifications')
<div class="container">
    @include('layouts.frontpage.sidepanelGig')
    <div class="col-xs-12 col-md-9 col-sm-height nopadding">

    @if($queries != null)
     @foreach($queries as $Gigs)
        @foreach($Gigs->chunk(2) as $items)
        <div class="row-height">
            @foreach($items as $Gig)
            <div class="col-xs-12 col-md-6 col-sm-height">
                <div class="card-gig">
                    <div class="card-image">
                        <div class="gradient"></div>
                        <img class="img-responsive" src="{{url('images/imagegig/'.$Gig->gig_image_file_path)}}">
                        <span class="card-info-date">
                            <div class="date">
                                <div class="day">{{$Gig->gig_date->format('d')}}</div>
                                <div class="month">{{$Gig->gig_date->format('M')}}</div>
                            </div>
                        </span>
                        <span class="card-info-genre">
                            @foreach ($GigGenres as $GigGenre)
                            @if($Gig->id == $GigGenre->gig_id)
                            <span class="label label-transparent">{{$GigGenre->genre->genre_name}}</span>
                            @endif
                            @endforeach
                        </span>
                        <span class="card-info-title">{{$Gig->gig_name}}</span>
                        <span class="card-info-by">
                            @foreach ($Gig->giglocation as $giglocation)
                            <i class="fa fa-map-marker" aria-hidden="true"></i> {{$giglocation->city->city_name}}, {{$giglocation->city->province->province_name}}
                            @endforeach
                        </span>
                    </div>
                    <div class="card-content">
                        <div class="card-info-regisTime">
                            @if($Gig->gig_band_closing_date <= $nowTime OR $Gig->status == 'close')
                            <h4 class="registTitle"><i class="fa fa-clock-o" aria-hidden="true"></i> Akhir Registrasi : <span class="registInfo">Telah Selesai</span></h4>
                            @else
                            <h4 class="registTitle"><i class="fa fa-clock-o" aria-hidden="true"></i> Akhir Registrasi : <span class="registInfo">{{$Gig->gig_band_closing_date->diffForHumans()}}</span></h4>
                            @endif
                        </div>
                    </div>
                    <div class="card-action">
                        <div class="row">
                            <div class="col-md-6 col-xs-6">
                                <div class="card-info-by">
                                    @foreach ($Organizations as $Organization)
                                    @if($Gig->organization_id == $Organization->id)
                                    Diselenggarakan Oleh <br/><a href="{{ url('/organization/'.$Organization->url_organization_name) }}">{{$Organization->organization_name}}</a>
                                    @endif
                                    @endforeach
                                </div>
                            </div>
                            <div class="col-md-6 col-xs-6">
                                <a href="/{{$Organization->url_organization_name}}/gig/{{$Gig->slug}}"><button class="btn btn-md btn-block btn-gray-round"> Detail</button></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
        @endforeach
        @endforeach
@else
        @foreach($Gigs->chunk(2) as $items)
        <div class="row-height">
            @foreach($items as $Gig)
            <div class="col-xs-12 col-md-6 col-sm-height">
                <div class="card-gig">
                    <div class="card-image">
                        <div class="gradient"></div>
                        <img class="img-responsive" src="{{url('images/imagegig/'.$Gig->gig_image_file_path)}}">
                        <span class="card-info-date">
                            <div class="date">
                                <div class="day">{{$Gig->gig_date->format('d')}}</div>
                                <div class="month">{{$Gig->gig_date->format('M')}}</div>
                            </div>
                        </span>
                        <span class="card-info-genre">
                            @foreach ($GigGenres as $GigGenre)
                            @if($Gig->id == $GigGenre->gig_id)
                            <span class="label label-transparent">{{$GigGenre->genre->genre_name}}</span>
                            @endif
                            @endforeach
                        </span>
                        <span class="card-info-title">{{$Gig->gig_name}}</span>
                        <span class="card-info-by">
                            @foreach ($Gig->giglocation as $giglocation)
                            <i class="fa fa-map-marker" aria-hidden="true"></i> {{$giglocation->city->city_name}}, {{$giglocation->city->province->province_name}}
                            @endforeach
                        </span>
                    </div>
                    <div class="card-content">
                        <div class="card-info-regisTime">
                            @if($Gig->gig_band_closing_date <= $nowTime OR $Gig->status == 'close')
                            <h4 class="registTitle"><i class="fa fa-clock-o" aria-hidden="true"></i> Akhir Registrasi : <span class="registInfo">Telah Selesai</span></h4>
                            @else
                            <h4 class="registTitle"><i class="fa fa-clock-o" aria-hidden="true"></i> Akhir Registrasi : <span class="registInfo">{{$Gig->gig_band_closing_date->diffForHumans()}}</span></h4>
                            @endif
                        </div>
                    </div>
                    <div class="card-action">
                        <div class="row">
                            <div class="col-md-6 col-xs-6">
                                <div class="card-info-by">
                                    @foreach ($Organizations as $Organization)
                                    @if($Gig->organization_id == $Organization->id)
                                    Diselenggarakan Oleh <br/><a href="{{ url('/organization/'.$Organization->url_organization_name) }}">{{$Organization->organization_name}}</a>
                                    @endif
                                    @endforeach
                                </div>
                            </div>
                            <div class="col-md-6 col-xs-6">
                                <a href="/{{$Organization->url_organization_name}}/gig/{{$Gig->slug}}"><button class="btn btn-md btn-block btn-gray-round"> Detail</button></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
        @endforeach
        @endif
        <div class="row-height">
        <div class="col-md-12 col-xs-12 text-left">
        {!! $Gigs->render() !!}
        </div>
        </div>
    </div>
</div>
@endsection