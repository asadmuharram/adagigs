<?php use Carbon\Carbon;
Carbon::setLocale('id');
?>
@extends('layouts.dashboard.auth')
@section('title', 'Cari Gigs')
@section('content')
@include('common.errors')
@include('common.notifications')
<div class="container">
    @include('layouts.frontpage.sidepanelBand')
    <div class="col-xs-12 col-md-9 col-sm-height nopadding">
        @foreach($Bands->chunk(2) as $items)
            <div class="row nopadding">
                @foreach($items as $Band)
                <div class="col-xs-12 col-md-6 col-sm-height">
                    <div class="card-gig">
                        <div class="card-image">
                            <div class="gradient"></div>
                            <img class="img-responsive" src="{{url('images/imagegig/'.$Band->image_band_profile_path)}}">
                            <span class="card-info-title">
                                {{$Band->band_name}}
                            </span>
                            <span class="card-info-by">
                                @foreach ($BandLocations as $BandLocation)
                                @if($BandLocation->band_id == $Band->id)
                                <i class="fa fa-map-marker" aria-hidden="true"></i> {{$BandLocation->city->city_name}}, {{$BandLocation->city->province->province_name}}
                                @endif
                                @endforeach
                            </span>
                        </div>
                        <div class="card-action">
                            <div class="row">
                                <div class="col-md-8 col-xs-8">
                                    <span class="label label-transparent">{{$Band->bandtype->band_type_name}}</span>
                                    @foreach ($BandGenres as $BandGenre)
                                    @if($BandGenre->band_id == $Band->id)
                                    <span class="label label-transparent">{{$BandGenre->genre->genre_name}}</span>
                                    @endif
                                    @endforeach
                                </div>
                                <div class="col-md-4 col-xs-4 text-right">
                                    <a href="/band/{{$Band->url_band_name}}">Lihat Profil <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
            @endforeach
    </div>
</div>
@endsection