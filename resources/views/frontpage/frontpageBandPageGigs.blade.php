@extends('layouts.dashboard.auth')
@section('title', $Band->band_name)
@section('content')
<div class="container">
	<div class="col-md-12 col-sm-12">
		<div class="card hovercard-gig">
			<div class="card-background-gig">
				<img class="card-bkimg" alt="" src="{{ url('/images/profile/'.$Band->image_band_profile_path) }}">
				<!-- http://lorempixel.com/850/280/people/9/ -->
			</div>
			<div class="useravatar-gig">
				<img alt="" src="{{ url('/images/profile/'.$Band->image_band_profile_path) }}">
			</div>
			<div class="card-info-genre-gig">
				<span class="label label-transparent"style="font-family: 'Proxima Nova Rg';">{{$BandGenre->genre->genre_name}}</span>
			</div>
			<div class="card-info-gig">
				<span class="card-title-gig">{{$Band->band_name}} | <span style="font-family: 'Proxima Nova Rg';">{{$Band->bandtype->band_type_name}}</span>
			</div>
			<div class="card-info-detail-gig">
				<span class="card-title-detail-gig"><i class="fa fa-map-marker" aria-hidden="true"></i> {{$BandLocation->city->city_name}}, {{$BandLocation->city->province->province_name}}</span>
			</div>
		</div>
		<div class="btn-pref btn-group btn-group-justified btn-group-lg" role="group" aria-label="...">
			<div class="btn-group" role="group">
				<a href="/band/{{$Band->url_band_name}}"><button type="button" class="btn btn-white-tab">
					<i class="fa fa-info-circle" aria-hidden="true"></i>
					<div class="hidden-xs">Info</div>
				</button></a>
			</div>
			<div class="btn-group" role="group">
				<button type="button" class="btn btn-white-tab-active">
				<i class="fa fa-play" aria-hidden="true"></i>
				<div class="hidden-xs">Gig</div>
				</button>
			</div>
		</div>
		<div class="well">
			<div class="tab-content">
				<div class="tab-pane fade in active">
					@if(count($Gigbands) > 0)
					<div class="panelBandList">
						@foreach($Gigbands as $Gig)
						<div class="row">
						<div class="col-md-12">
							<div class="media">
								<div class="media-left">
									<img class="thumb img-circle" src="{{ url('/images/profile/'.$Gig->gig->gig_image_file_path) }}" alt="{{$Gig->gig->gig_name}}" >
								</div>
								<div class="media-body">
									<h4 class="titleBand">{{$Gig->gig->gig_name}}</h4>
								</div>
								<div class="media-right">
									<a class="btn btn-md btn-block btn-orange-round" href="/{{$Gig->gig->organization->url_organization_name}}/gig/{{$Gig->gig->slug}}">Lihat</a>
								</div>
							</div>
							</div>
						</div>
						@endforeach
					</div>
					@else
					<h5 class="subTitle">Belum pernah mengikuti gig.</h5>
					@endif
				</div>
			</div>
		</div>
		
	</div>
</div>
@endsection