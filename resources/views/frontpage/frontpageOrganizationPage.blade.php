@extends('layouts.dashboard.auth')
@section('title', $Organization->organization_name)
@section('content')
<div class="container">
	<div class="col-md-12 col-sm-12">
		<div class="card hovercard">
			<div class="card-background">
				<img class="card-bkimg" alt="" src="{{ url('/images/profile/'.$Organization->image_file_path) }}">
				<!-- http://lorempixel.com/850/280/people/9/ -->
			</div>
			<div class="useravatar">
				<img alt="" src="{{ url('/images/profile/'.$Organization->image_file_path) }}">
			</div>
			<div class="card-info">
				<span class="card-title">{{$Organization->organization_name}} | <span style="font-family: 'Proxima Nova Rg';">{{$Organization->organizationtype->organization_type_name}}</span></span>
			</div>
			<div class="card-info-detail">
				<span class="card-title-detail"><i class="fa fa-map-marker" aria-hidden="true"></i> {{$OrganizationAddress->address}}, {{$OrganizationAddress->city->city_name}}, {{$OrganizationAddress->city->province->province_name}}</span>
			</div>
		</div>
		<div class="btn-pref btn-group btn-group-justified btn-group-lg" role="group" aria-label="...">
			<div class="btn-group" role="group">
				<button type="button" class="btn btn-white-tab-active">
				<i class="fa fa-info-circle" aria-hidden="true"></i>
				<div class="hidden-xs">Info</div>
				</button>
			</div>
			<div class="btn-group" role="group">
				<a href="/organization/{{$Organization->url_organization_name}}/gig"><button type="button" class="btn btn-white-tab">
				<i class="fa fa-play" aria-hidden="true"></i>
				<div class="hidden-xs">Gig</div>
				</button></a>
			</div>
		</div>
		<div class="well">
			<div class="tab-content">
				<div class="tab-pane fade in active">
						<div class="panelBody">
                        <div class="ProfileOrg">
                            <div class="row">
                                <div class="col-md-6 col-xs-12">
                                    <h4 class="mainTitle">Sosial Media</h4>
                                    <?php
                                    if(!empty($Organization->facebook_url)){
                                    ?>
                                    <div class="col-md-2 col-xs-2">
                                        <a href="http://facebook.com/{{$Organization->facebook_url}}" target="blank"><i class="fa fa-facebook-square fa-2x" aria-hidden="true"></i></a>
                                    </div>
                                    <?php
                                    }
                                    if(!empty($Organization->twitter_url)){
                                    ?>
                                    <div class="col-md-2 col-xs-2">
                                        <a href="http://twitter.com/{{$Organization->twitter_url}}" target="blank"><i class="fa fa-twitter fa-2x" aria-hidden="true"></i></a>
                                    </div>
                                    <?php
                                    }
                                    if(!empty($Organization->instagram_url)){
                                    ?>
                                    <div class="col-md-2 col-xs-2">
                                        <a href="http://instagram.com/{{$Organization->instagram_url}}" target="blank"><i class="fa fa-instagram fa-2x" aria-hidden="true"></i></a>
                                    </div>
                                    <?php
                                    }
                                    ?>
                                </div>
                                <div class="col-md-6 col-xs-12">
                                    <h4 class="mainTitle">Website</h4>
                                    <p class="descBand"><a href="http://{{$Organization->organization_website}}" target="blank">{{$Organization->organization_website}}</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
				</div>
			</div>
		</div>
		
	</div>
</div>
@endsection