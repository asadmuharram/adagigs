<div class="col-xs-12 col-md-3 col-sm-height">
    <div class="row-height">
        <div class="inside inside-full-height">
            <div class="panelSidebar bgCard">
                <div class="panelTitle">
                    <h4 class="mainTitle">
                    <a id="toggle-collapse" data-toggle="collapse" href="#panel-collapse-gig" aria-expanded="false" aria-controls="collapseExample">
                        CARI GIG
                    </a></h4>
                </div>
                <div class="panelSidemenu">
                    <ul class="sideMenu-search collapse in" id="panel-collapse-gig">
                        <li>    
                            <form action="{{ url('/gigs/search') }}" method="GET">
                                <label>Nama Gig</label>
                                <input type="text" class="form-control" name="q" placeholder="Ketik nama gig" value="{{ Request::get('q') }}"/>
                                <button  type="submit" style="display:none;"></button>
                            </form>
                        </li>
                        <li>    
                            <form id="searchLocation" action="{{ url('/gigs/search') }}" method="GET">
                                <label>Lokasi</label>
                                <input id="city_id_search" type="text" class="form-control" name="cityname" placeholder="Ketik lokasi kota" value="{{ Request::get('cityname') }}"/>
                                <input id="city_search" type="hidden" class="form-control" name="city" value=""/>
                                <button  type="submit" style="display:none;"></button>
                            </form>
                        </li>
                        <li>
                            <form id="searchGig" action="{{ url('/gigs/search') }}" method="GET">
                                <label>Tanggal</label>
                                <div class="input-group{{ $errors->has('start') ? ' has-error' : '' }}">
                                    <input type="text" class="form-control datePicker" id="openDate" name="start" placeholder="Tentukan tanggal awal" value="{{Request::get('start')}}">
                                    <span class="input-group-addon" id="basic-addon3"><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                </div>
                                <div class="input-group{{ $errors->has('end') ? ' has-error' : '' }}">
                                    <input type="text" class="form-control datePicker" id="closeDate" name="end" placeholder="Tentukan tanggal akhir" value="{{Request::get('end')}}" >
                                    <span class="input-group-addon" id="basic-addon3"><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                </div>
                                <button  type="submit" style="display:none;"></button>
                            </form>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="row-height">
        <div class="inside inside-full-height">
            <div class="panelSidebar bgCard">
                <div class="panelTitle">
                    <h4 class="mainTitle"><a id="toggle-collapse" data-toggle="collapse" href="#panel-collapse-type" aria-expanded="false" aria-controls="collapseExample">TIPE GIG</a></h4>
                </div>
                <div class="panelSidemenu">
                    <ul class="sideMenu-search-overflow collapse in" id="panel-collapse-type">
                            @foreach($GigTypes as $GigType)
                            <li>
                            <form action="{{ url('/gigs/search') }}" method="GET">
                                <label>
                                    @if($GigType->id == Request::get('type') )
                                        <input type="radio" name="type" value="{{$GigType->id}}" onChange="this.form.submit()" checked> {{$GigType->gig_type_name}}
                                    @else
                                        <input type="radio" name="type" value="{{$GigType->id}}" onChange="this.form.submit()"> {{$GigType->gig_type_name}}
                                    @endif
                                </label>
                            </form>
                            </li>
                            @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="row-height">
        <div class="inside inside-full-height">
            <div class="panelSidebar bgCard">
                <div class="panelTitle">
                    <h4 class="mainTitle">
                    <a id="toggle-collapse" data-toggle="collapse" href="#panel-collapse-genre" aria-expanded="false" aria-controls="collapseExample">
                        GENRE GIG
                    </a>
                    </h4>
                </div>
                <div class="panelSidemenu">
                    <ul class="sideMenu-search-overflow collapse in" id="panel-collapse-genre">
                            @foreach($GenreLists as $GenreList)
                            <li>
                            <form action="{{ url('/gigs/search') }}" method="GET">
                                <label>
                                    @if($GenreList->id == Request::get('genre') )
                                        <input type="radio" name="genre" value="{{$GenreList->id}}" onChange="this.form.submit()" checked=""> {{$GenreList->genre_name}}
                                    @else
                                        <input type="radio" name="genre" value="{{$GenreList->id}}" onChange="this.form.submit()"> {{$GenreList->genre_name}}
                                    @endif
                                </label>
                            </form>
                            </li>
                            @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>