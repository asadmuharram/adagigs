@if (ends_with(Route::currentRouteAction(), 'FrontpageController@index'))
<section class="headerTop">
    <nav class="navbar navbar-default navbar-static-top">
        <div class="container">
            <div class="navbar-header">
                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                <span class="sr-only">Toggle Navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                </button>
                <!-- Branding Image -->
                <a class="navbar-brand" href="{{ url('/') }}"></a>
            </div>
            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                <ul class="nav navbar-nav">
                    <li><a href="{{ url('/gigs/') }}">cari gigs</a></li>
                    <li><a href="{{ url('/bands/') }}">cari musisi / band</a></li>
                </ul>
                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                    <!-- Authentication Links -->
                    @if (Auth::guest())
                    <li><a href="{{ url('/login') }}">Masuk</a></li>
                    <li><a href="{{ url('/register') }}">Daftar</a></li>
                    @else
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            {{ Auth::user()->first_name }} <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="{{ url('/dashboard') }}">Dashboard</a></li>
                            <li><a href="{{ url('/logout') }}">Logout</a></li>
                        </ul>
                    </li>
                    @endif
                </ul>
            </div>
        </div>
    </nav>
    <div class="container">
        <div class="headerSearch">
            <div class="col-md-12">
                <h1>Temukan gig, musisi atau band disini.</h1>
                <h2>Jelajahi gig, musisi atau band dan dapatkan kesempatan ikut serta dalam gig dimanapun sesuai genre musikmu.</h2>
                <form action="{{ url('/gigs/search') }}" method="GET">
                    <div class="right-inner-addon input-group searchBox">
                        <div class="input-group-btn search-panel">
                            <button type="button" class="btn btn-filter btn-default dropdown-toggle" data-toggle="dropdown">
                            <span id="search_concept">Pilih Kategori</span> <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="#gig">Gig</a></li>
                                <li><a href="#genre">Genre</a></li>
                                <li><a href="#band">Musisi / Band</a></li>
                                <li><a href="#location">Lokasi Kota</a></li>
                            </ul>
                        </div>
                        <input type="hidden" name="param" value="none" id="search_param">
                        <input type="text" class="form-control" name="keyword" placeholder="Kata Kunci, Nama Gig, Genre Musik, Lokasi kota" value="{{ old('keyword') }}">
                         @if ($errors->has('param')) <p class="help-block">{{ $errors->first('param') }}</p> @endif
                        <span class="input-group-btn">
                            <button  type="submit" style="display:none;"></button>
                        </span>
                    </div>
                </form>
                <!--<form action="{{ url('/gigs/search') }}" method="GET">
                    <div class="right-inner-addon searchBox">
                        <i class="fa fa-search fa-2x"></i>
                        <input type="text" class="form-control" placeholder="Ketik kata kunci" name="keyword" />
                        <button  type="submit" style="display:none;"></button>
                    </div>
                </form>-->
            </div>
        </div>
    </div>
</section>
@else
<nav class="navbar navbar-default navbar-static-top">
    <div class="container">
        <div class="navbar-header">
            <!-- Collapsed Hamburger -->
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
            <span class="sr-only">Toggle Navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            </button>
            <!-- Branding Image -->
            <a class="navbar-brand" href="{{ url('/') }}"></a>
        </div>
        <div class="collapse navbar-collapse" id="app-navbar-collapse">
            <!-- Left Side Of Navbar -->
            <ul class="nav navbar-nav">
                <li><a href="{{ url('/home') }}">cari gigs</a></li>
                <li><a href="{{ url('/home') }}">cari musisi / band</a></li>
            </ul>
            <!-- Right Side Of Navbar -->
            <ul class="nav navbar-nav navbar-right">
                <!-- Authentication Links -->
                @if (Auth::guest())
                <li><a href="{{ url('/login') }}">Login</a></li>
                <li><a href="{{ url('/register') }}">Register</a></li>
                @else
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                        {{ Auth::user()->name }} <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Logout</a></li>
                    </ul>
                </li>
                @endif
            </ul>
        </div>
    </div>
</nav>
@endif