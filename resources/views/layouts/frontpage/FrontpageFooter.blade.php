<div id="footer">
	<div class="container">
		<div class="col-md-6 col-xs-6">
			<p class="text-muted"><span class="pixfuelFooter">© 2016 <strong>Adagigs.com</strong></span></p>
		</div>
		<div class="col-md-6 col-xs-6">
			<div class="text-right">
				<ul class="footerNav">
					<li><a href="/privacy">PRIVASI</a></li>
					<li><a href="/termsofservice">KETENTUAN</a></li>
					<li><a href="">FAQ</a></li>
				</ul>
			</div>
		</div>
		<!--<span class="infoFooter">
			<a href="https://www.facebook.com/PixfuelMedia/"><i class="fa fa-facebook-square fa-lg"></i></a>
			<a href="https://twitter.com/pixfuel_"><i class="fa fa-twitter-square fa-lg"></i></a>
		</span>-->
	</div>
</div>