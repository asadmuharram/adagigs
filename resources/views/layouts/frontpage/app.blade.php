<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>@yield('title') - Adagigs | Temukan gig disini.</title>
        <meta name="robots" content="noindex,nofollow">
        <!-- Fonts -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
        <link rel="shortcut icon" href="{{ URL::asset('assets/images/favicon_32.png') }}">
        <!-- Styles -->
        <link rel="stylesheet" href="{{ URL::asset('assets/css/bootstrap.min.css') }}" rel="stylesheet"/>
        <link rel="stylesheet" href="{{ URL::asset('assets/css/bootstrap-social.css') }}" rel="stylesheet"/>
        <link rel="stylesheet" href="{{ URL::asset('assets/css/animate.css') }}" rel="stylesheet"/>
        <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
        <link rel="stylesheet" href="{{ URL::asset('assets/css/style.css') }}" />
    </head>
    <body>
        <div id="wrapper">
            <div id="content">
                @include('layouts.frontpage.FrontpagenavMenu')
                @yield('content')
            </div>
            @include('layouts.frontpage.FrontpageFooter')
        </div>
        <!-- JavaScripts -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
        {{-- <script src="{{ elixir('js/app.js') }}"></script> --}}
        <script type="text/javascript" charset="utf-8">
        $(document).ready(function(e){
        $('.search-panel .dropdown-menu').find('a').click(function(e) {
        e.preventDefault();
        var param = $(this).attr("href").replace("#","");
        var concept = $(this).text();
        $('.search-panel span#search_concept').text(concept);
        $('.input-group #search_param').val(param);
        });
        });
        </script>
    </body>
</html>