<div class="col-xs-12 col-md-3 col-sm-height">
    <div class="row-height">
        <div class="inside inside-full-height">
            <div class="panelSidebar bgCard">
                <div class="panelTitle">
                    <h4 class="mainTitle">
                    <a id="toggle-collapse" data-toggle="collapse" href="#panel-collapse-gig" aria-expanded="false" aria-controls="collapseExample">
                        CARI BAND
                    </a></h4>
                </div>
                <div class="panelSidemenu">
                    <ul class="sideMenu-search collapse in" id="panel-collapse-gig">
                        <li>
                            <form action="{{ url('/bands/search') }}" method="GET">
                                <label>Nama Band</label>
                                <input type="text" class="form-control" name="q" placeholder="Ketik nama musisi / band" value="{{ old('q') }}"/>
                                <button  type="submit" style="display:none;"></button>
                            </form>
                        </li>
                        <li>
                            <form id="searchLocation" action="{{ url('/bands/search') }}" method="GET">
                                <label>Lokasi</label>
                                <input id="city_id_search" type="text" class="form-control" name="cityname" placeholder="Ketik lokasi kota" value="{{ Request::get('cityname') }}"/>
                                <input id="city_search" type="hidden" class="form-control" name="city" placeholder="Ketik lokasi kota"/>
                                <button  type="submit" style="display:none;"></button>
                            </form>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="row-height">
        <div class="inside inside-full-height">
            <div class="panelSidebar bgCard">
                <div class="panelTitle">
                    <h4 class="mainTitle">
                    <a id="toggle-collapse" data-toggle="collapse" href="#panel-collapse-genre" aria-expanded="false" aria-controls="collapseExample">
                        GENRE BAND
                    </a>
                    </h4>
                </div>
                <div class="panelSidemenu">
                    <ul class="sideMenu-search-overflow collapse in" id="panel-collapse-genre">
                        <form action="{{ url('/bands/search') }}" method="GET">
                            @foreach($GenreLists as $GenreList)
                            <li>
                                <label>
                                    @if($GenreList->id == Request::get('genre') )
                                        <input type="radio" name="genre" value="{{$GenreList->id}}" onChange="this.form.submit()" checked=""> {{$GenreList->genre_name}}
                                    @else
                                        <input type="radio" name="genre" value="{{$GenreList->id}}" onChange="this.form.submit()"> {{$GenreList->genre_name}}
                                    @endif
                                </label>
                            </li>
                            @endforeach
                        </form>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>