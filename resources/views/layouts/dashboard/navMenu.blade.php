<nav class="navbar navbar-default navbar-static-top">
    <div class="container">
        <div class="navbar-header">
            <!-- Collapsed Hamburger -->
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
            <span class="sr-only">Toggle Navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            </button>
            <!-- Branding Image -->
            <a class="navbar-brand" href="{{ url('/') }}"></a>
        </div>
        <div class="collapse navbar-collapse" id="app-navbar-collapse">
            <!-- Left Side Of Navbar -->
            <ul class="nav navbar-nav">
                <li><a href="{{ url('/gigs') }}">cari gigs</a></li>
                <li><a href="{{ url('/bands') }}">cari musisi / band</a></li>
            </ul>
            <!-- Right Side Of Navbar -->
            <ul class="nav navbar-nav navbar-right">
                <!-- Authentication Links -->
                @if (Auth::guest())
                <li><a href="{{ url('/login') }}">Masuk</a></li>
                <li><a href="{{ url('/register') }}">Daftar</a></li>
                @else
                <li>
                <a href="/dashboard"><i class="fa fa-bell-o" aria-hidden="true"></i>
                @if(Auth::check())
                @if($notificationCount > 0)
                    <span class="notification-counter">{{$notificationCount}}</span>
                @endif
                @endif
                </a>
                </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            {{ Auth::user()->first_name }} <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="{{ url('/dashboard') }}">Dashboard</a></li>
                            <li><a href="{{ url('/dashboard/settings') }}">Pengaturan</a></li>
                            <li><a href="{{ url('/logout') }}">Keluar</a></li>
                        </ul>
                    </li>
                    @endif
                </ul>
            </div>
        </div>
    </nav>