<div class="col-md-3">
    <div class="row">
        <div class="col-md-12">
            <div class="panelSidebar bgCard">
                <div class="panelTitle">
                    <h4 class="mainTitle">AKUN</h4>
                </div>
                <div class="panelSidemenu">
                    <ul class="sideMenu">
                        @if($User->type == 'unknown')

                        @elseif(($User->type == 'organization'))
                        <a href="{{ URL::to('/dashboard/organization/') }}"><li>
                            Organisasi
                        </li></a>
                        <a href="{{ URL::to('/dashboard/gig/') }}"><li>
                            Gig
                        </li></a>
                        @elseif(($User->type == 'band'))
                        <a href="{{ URL::to('/dashboard/band/') }}"><li>
                            Musisi / Band
                        </li></a>
                        <a href="{{ URL::to('/dashboard/gig/') }}"><li>
                            Gig
                        </li></a>
                        @else
                        
                        @endif
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>