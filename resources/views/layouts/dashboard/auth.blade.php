<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="_token" content="{{ csrf_token() }}"/>
        <title>Adagigs - @yield('title') | Temukan gig disini.</title>
        <meta name="robots" content="noindex,nofollow">
        <!-- Fonts -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/css/bootstrap-select.min.css">
        <link rel="shortcut icon" href="{{ URL::asset('assets/images/favicon_32.png') }}">
        <!-- Styles -->
        <link rel="stylesheet" href="{{ URL::asset('assets/css/bootstrap.min.css') }}" rel="stylesheet"/>
        <link rel="stylesheet" href="{{ URL::asset('assets/css/bootstrap-social.css') }}" rel="stylesheet"/>
        <link rel="stylesheet" href="{{ URL::asset('assets/css/auth.css') }}" rel="stylesheet"/>
        <link rel="stylesheet" href="{{ URL::asset('assets/css/animate.css') }}" rel="stylesheet"/>
        <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    </head>
    <body>
        <div id="wrapper">
            <div id="content">
                @include('layouts.dashboard.navMenu')
                @yield('content')
            </div>
            @include('layouts.dashboard.footer')
        </div>
        <!-- JavaScripts -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
        <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
        <script src="{{ URL::asset('assets/js/bootstrap.min.js') }}"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/js/bootstrap-select.min.js"></script>
        <script src="{{ URL::asset('assets/js/tinymce/jquery.tinymce.min.js') }}"></script>
        <script src="{{ URL::asset('assets/js/tinymce/tinymce.min.js') }}"></script>
        <script>
        $(document).ready (function ()
        {   
        $("[data-toggle='tooltip']").tooltip();
        $('#city_id_search').autocomplete(
        {
        minLength: 2,
        select: function(event,ui) {
        $('#city_id_search').val(ui.item.value);
        $('#city_search').val(ui.item.id);
        $('#searchLocation').submit();
        return false; // Prevent the widget from inserting the value.
        },
        source: function (request, response)
        {
        $.ajax(
        {
        url: '{{ url("/getcities") }}',
        data: {term: request.term},
        dataType: "json",
        success: function (jsonDataReceivedFromServer)
        {
        //alert (JSON.stringify (jsonDataReceivedFromServer));
        // console.log (jsonDataReceivedFromServer);
        response ($.map(jsonDataReceivedFromServer, function (item)
        {
        console.log (item.city_name);
        // NOTE: BRACKET START IN THE SAME LINE AS RETURN IN
        //       THE FOLLOWING LINE
        return {
        id: item.city_id, value: item.city_name };
        }));
        }
        });
        },
        });

        $('#city_id').autocomplete(
        {
        minLength: 2,
        select: function(event,ui) {
        $('#city_id').val(ui.item.value);
        $('#city').val(ui.item.id);
        return false; // Prevent the widget from inserting the value.
        },
        source: function (request, response)
        {
        $.ajax(
        {
        url: '{{ url("getcities") }}',
        data: {term: request.term},
        dataType: "json",
        success: function (jsonDataReceivedFromServer)
        {
        //alert (JSON.stringify (jsonDataReceivedFromServer));
        // console.log (jsonDataReceivedFromServer);
        response ($.map(jsonDataReceivedFromServer, function (item)
        {
        console.log (item.city_name);
        // NOTE: BRACKET START IN THE SAME LINE AS RETURN IN
        //       THE FOLLOWING LINE
        return {
        id: item.city_id, value: item.city_name };
        }));
        }
        });
        },
        });

        $('#band_id').autocomplete(
        {
        minLength: 2,
        select: function(event,ui) {
        $('#band_id').val(ui.item.value);
        $('#band').val(ui.item.id);
        return false; // Prevent the widget from inserting the value.
        },
        source: function (request, response)
        {
        $.ajax(
        {
        url: '{{ url("dashboard/getbands") }}',
        data: {term: request.term},
        dataType: "json",
        success: function (jsonDataReceivedFromServer)
        {
        //alert (JSON.stringify (jsonDataReceivedFromServer));
        // console.log (jsonDataReceivedFromServer);
        response ($.map(jsonDataReceivedFromServer, function (item)
        {
        console.log (item.band_name);
        // NOTE: BRACKET START IN THE SAME LINE AS RETURN IN
        //       THE FOLLOWING LINE
        return {
        id: item.band_id, value: item.band_name };
        }));
        }
        });
        },
        });
        $('#genre_id').autocomplete(
        {
        minLength: 2,
        select: function(event,ui) {
        $('#genre_id').val(ui.item.value);
        $('#genre').val(ui.item.id);
        return false; // Prevent the widget from inserting the value.
        },
        source: function (request, response)
        {
        $.ajax(
        {
        url: '{{ url("dashboard/getgenres") }}',
        data: {term: request.term},
        dataType: "json",
        success: function (jsonDataReceivedFromServer)
        {
        //alert (JSON.stringify (jsonDataReceivedFromServer));
        // console.log (jsonDataReceivedFromServer);
        response ($.map(jsonDataReceivedFromServer, function (item)
        {
        console.log (item.genre_name);
        // NOTE: BRACKET START IN THE SAME LINE AS RETURN IN
        //       THE FOLLOWING LINE
        return {
        id: item.genre_id, value: item.genre_name };
        }));
        }
        });
        },
        });
        });
        $('.selectpicker').selectpicker({
        size: 4
        });
        
        var daysToAdd = 1;
        $("#openDate").datepicker({
        onSelect: function (selected) {
        var dtMax = new Date(selected);
        dtMax.setDate(dtMax.getDate() + daysToAdd);
        var dd = dtMax.getDate();
        var mm = dtMax.getMonth() + 1;
        var y = dtMax.getFullYear();
        var dtFormatted = mm + '/'+ dd + '/'+ y;
        $("#closeDate").datepicker("option", "minDate", dtFormatted);
        }
        });
        
        $("#closeDate").datepicker({
        onSelect: function (selected) {
        var dtMax = new Date(selected);
        dtMax.setDate(dtMax.getDate() + daysToAdd);
        var dd = dtMax.getDate();
        var mm = dtMax.getMonth() + 1;
        var y = dtMax.getFullYear();
        var dtFormatted = mm + '/'+ dd + '/'+ y;
        $('#searchGig').submit();
        $("#gigDate").datepicker("option", "minDate", dtFormatted)
        }
        });
        $("#gigDate").datepicker({
        onSelect: function (selected) {
        var dtMax = new Date(selected);
        dtMax.setDate(dtMax.getDate() + daysToAdd);
        var dd = dtMax.getDate();
        var mm = dtMax.getMonth() + 1;
        var y = dtMax.getFullYear();
        var dtFormatted = mm + '/'+ dd + '/'+ y;
        $("#gigDate").datepicker("option", "minDate", dtFormatted);
        }
        });
        (function($) {
        $('.dropdown-submit-input .dropdown-menu a').click(function (e) {
        e.preventDefault();
        $(this).closest('.dropdown-submit-input').find('input').val($(this).data('value'));
        $(this).closest('form').submit();
        });
        })(jQuery);
        $(document).ready(function () {
        window.setTimeout(function() {
        $(".alert").fadeTo(1000, 0).slideUp(400, function(){
        $(this).remove();
        });
        }, 3000);
        });
        $('[data-toggle="tooltip"]').tooltip();
        </script>
        {{-- <script src="{{ elixir('js/app.js') }}"></script> --}}
        <script>
        tinymce.init({
        selector: '.textDesc',
        toolbar: [
        'undo | redo | cut | copy | paste | bold | italic | underline | bullist | numlist | outdent | indent | removeformat '
        ],
        menu: {
        
        },
        statusbar: false,
        theme: "modern",
        skin: 'light',
        plugins : "paste",
        paste_auto_cleanup_on_paste : true,
        });
        </script>
    </body>
</html>